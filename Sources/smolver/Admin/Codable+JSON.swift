//
//  Codable+JSON.swift
//  smolver
//
//  Created by Justin Marshall on 7/3/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

private enum JSONificationError: LocalizedError {
    case message(String)

    var errorDescription: String? {
        switch self {
        case .message(let message):
            return message
        }
    }
}

extension Encodable {
    func jsonify() throws -> String {
        do {
            let data = try JSONEncoder().encode(self)

            guard let dictionary = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                throw JSONificationError.message("Unable to convert encoded data structure into JSON dictionary.")
            }
            let prettyPrintedData = try JSONSerialization.data(withJSONObject: dictionary, options: [.sortedKeys, .prettyPrinted, .withoutEscapingSlashes])

            guard let prettyPrintedString = String(data: prettyPrintedData, encoding: .utf8) else {
                throw JSONificationError.message("Unable to convert encoded data structure into pretty printed JSON string.")
            }

            return prettyPrintedString
        } catch {
            throw JSONificationError.message("Unable to encode data structure. Reason: \"\(error.localizedDescription)\" This should not be possible! Please notify smolver developer.")
        }
    }
}
