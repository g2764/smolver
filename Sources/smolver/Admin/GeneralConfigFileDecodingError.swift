//
//  GeneralConfigFileDecodingError.swift
//  smolver
//
//  Created by Justin Marshall on 2/11/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

enum GeneralConfigFileDecodingError: LocalizedError {
    case absolutePath(key: String, path: String)
    case illegalLoggingConfiguration(reason: String)
    case illegalPathConfiguration(key: String, path: String)
    case noSuchFile(key: String, path: String)
    case noSuchPath(key: String, path: String)
    case unstandardizedPath(key: String, path: String)

    var errorDescription: String? {
        switch self {
        case .absolutePath(let key, let path):
            return """
            absolute paths not supported. Offending element: "\(key)": "\(path)"
            """
        case .illegalLoggingConfiguration(let reason):
            return reason
        case .illegalPathConfiguration(let key, let path):
            return """
            illegal path configuration. Subdirectories of subdirectories are not supported. Offending element: "\(key)": "\(path)"
            """
        case .noSuchFile(let key, let path):
            return """
            nonexistent file specified. Offending element: "\(key)": "\(path)"
            """
        case .noSuchPath(let key, let path):
            return """
            nonexistent relative path detected. All paths must be relative to ~\(GlobalConfiguration.smolverSubdirectory). Offending element: "\(key)": "\(path)"
            """
        case .unstandardizedPath(let key, let path):
            return """
            unstandardized path: relative path detected. All paths must be relative to ~\(GlobalConfiguration.smolverSubdirectory) and cannot contain `.` or `..`. Offending element: "\(key)": "\(path)"
            """
        }
    }
}
