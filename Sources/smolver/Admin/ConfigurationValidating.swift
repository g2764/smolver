//
//  ConfigurationValidating.swift
//  smolver
//
//  Created by Justin Marshall on 2/11/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol ConfigurationValidating {
    static func validate(fileName: String, forKey key: String) throws -> URL
    static func validate(filePath: String, forKey key: String) throws -> URL
    static func validate(directoryPath: String, forKey key: String, maxSubdirectoriesPerPathProperty: Int?) throws -> URL
}

extension ConfigurationValidating {
    static func validate(fileName: String, forKey key: String) throws -> URL {
        // If all paths are guaranteed to be relative to the server's configured
        // subirectory, it is one less thing to have to think about in the rest
        // of the codebase. More secure that way, too.

        guard !fileName.hasPrefix("/") else {
            throw GeneralConfigFileDecodingError.absolutePath(key: key, path: fileName)
        }

        guard let url = URL(string: fileName) else {
            throw GeneralConfigFileDecodingError.illegalLoggingConfiguration(reason: "illegal redirect configuration. \(fileName) is not a valid file name")
        }

        var urlToCheck = url
        urlToCheck.deletePathExtension()
        guard !urlToCheck.absoluteString.contains(".") && !urlToCheck.absoluteString.contains("..") else {
            throw GeneralConfigFileDecodingError.unstandardizedPath(key: key, path: fileName)
        }

        return url
    }

    static func validate(filePath: String, forKey key: String) throws -> URL {
        // If all paths are guaranteed to be relative to the server's configured
        // subirectory, it is one less thing to have to think about in the rest
        // of the codebase. More secure that way, too.

        guard !filePath.hasPrefix("/") else {
            throw GeneralConfigFileDecodingError.absolutePath(key: key, path: filePath)
        }

        guard FileManager.default.fileExists(atPath: GlobalConfiguration.configDirectory + filePath),
            let url = URL(string: filePath) else {
                throw GeneralConfigFileDecodingError.noSuchFile(key: key, path: filePath)
        }

        var urlToCheck = url
        urlToCheck.deletePathExtension()
        guard !urlToCheck.absoluteString.contains(".") && !urlToCheck.absoluteString.contains("..") else {
            throw GeneralConfigFileDecodingError.unstandardizedPath(key: key, path: filePath)
        }

        return url
    }

    static func validate(directoryPath: String, forKey key: String, maxSubdirectoriesPerPathProperty: Int? = 1) throws -> URL {
        // If all paths are guaranteed to be relative to the server's configured
        // subirectory, it is one less thing to have to think about in the rest
        // of the codebase. More secure that way, too.

        guard !directoryPath.hasPrefix("/") else {
            throw GeneralConfigFileDecodingError.absolutePath(key: key, path: directoryPath)
        }

        guard !directoryPath.contains(".") && !directoryPath.contains("..") else {
            throw GeneralConfigFileDecodingError.unstandardizedPath(key: key, path: directoryPath)
        }

        if let maxSubdirectoriesPerPathProperty = maxSubdirectoriesPerPathProperty {
            guard directoryPath.split(separator: "/").count <= maxSubdirectoriesPerPathProperty else {
                throw GeneralConfigFileDecodingError.illegalPathConfiguration(key: key, path: directoryPath)
            }
        }

        var isDirectory: ObjCBool = false
        guard FileManager.default.fileExists(atPath: GlobalConfiguration.configDirectory + directoryPath, isDirectory: &isDirectory),
            isDirectory.boolValue,
            let url = URL(string: directoryPath) else {
                throw GeneralConfigFileDecodingError.noSuchPath(key: key, path: directoryPath)
        }

        return url
    }
}
