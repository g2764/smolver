//
//  LocalConfiguration.swift
//  smolver
//
//  Created by Justin Marshall on 2/11/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

struct LocalConfiguration: Codable {
    static let fileName = ".smol.json"

    struct Authentication: Codable {
        struct File: Codable, ConfigurationValidating {
            private enum CodingKeys: String, CodingKey {
                // I think the JSON strings are more admin-friendly, while the
                // property names in this file are more dev-friendly and context-aware
                case name
                case whitelistedPath = "whitelistedCertificatePath"
            }

            let name: String
            let whitelistedPath: URL?

            init(name: String, whitelistedPath: String?) {
                self.name = name
                if let whitelistedPath = whitelistedPath {
                    self.whitelistedPath = URL(string: whitelistedPath)
                } else {
                    self.whitelistedPath = nil
                }
            }

            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                let name = try container.decode(String.self, forKey: .name)
                self.name = try Self.validate(fileName: name, forKey: CodingKeys.name.rawValue).absoluteString

                if let whitelistedPath = try container.decodeIfPresent(String.self, forKey: .whitelistedPath) {
                    self.whitelistedPath = try Self.validate(directoryPath: whitelistedPath, forKey: CodingKeys.whitelistedPath.rawValue, maxSubdirectoriesPerPathProperty: nil)
                } else {
                    self.whitelistedPath = nil
                }
            }
        }

        struct Directory: Codable, ConfigurationValidating {
            private enum CodingKeys: String, CodingKey {
                // I think the JSON strings are more admin-friendly, while the
                // property names in this file are more dev-friendly and context-aware
                case excludedFiles
                case whitelistedPath = "whitelistedCertificatePath"
            }

            let excludedFiles: [String]?
            let whitelistedPath: URL?

            init(excludedFiles: [String]?, whitelistedPath: String?) {
                self.excludedFiles = excludedFiles

                if let whitelistedPath = whitelistedPath {
                    self.whitelistedPath = URL(string: whitelistedPath)
                } else {
                    self.whitelistedPath = nil
                }
            }

            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                let excludedFiles = try container.decodeIfPresent([String].self, forKey: .excludedFiles)
                self.excludedFiles = try excludedFiles?.map { try Self.validate(fileName: $0, forKey: CodingKeys.excludedFiles.rawValue).absoluteString }

                if let whitelistedPath = try container.decodeIfPresent(String.self, forKey: .whitelistedPath) {
                    self.whitelistedPath = try Self.validate(directoryPath: whitelistedPath, forKey: CodingKeys.whitelistedPath.rawValue, maxSubdirectoriesPerPathProperty: nil)
                } else {
                    self.whitelistedPath = nil
                }
            }
        }

        let files: [File]?
        let directory: Directory?
    }

    struct Redirects: Codable {
        struct File: Codable, ConfigurationValidating {
            private enum CodingKeys: String, CodingKey {
                // I think the JSON strings are more admin-friendly, while the
                // property names in this file are more dev-friendly and context-aware
                case oldName = "oldFileName"
                case newName = "newFileName"
                case newPath = "newFilePath"
                case isTemporary
            }

            let oldName: String
            let newName: String
            let newPath: URL
            let isTemporary: Bool

            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                let oldName = try container.decode(String.self, forKey: .oldName)
                self.oldName = try Self.validate(fileName: oldName, forKey: CodingKeys.oldName.rawValue).absoluteString

                let newName = try container.decode(String.self, forKey: .newName)
                self.newName = try Self.validate(fileName: newName, forKey: CodingKeys.newName.rawValue).absoluteString

                let newPath = try container.decode(String.self, forKey: .newPath)
                self.newPath = try Self.validate(directoryPath: newPath, forKey: CodingKeys.newPath.rawValue, maxSubdirectoriesPerPathProperty: nil)

                let isTemporary = try container.decodeIfPresent(Bool.self, forKey: .isTemporary)
                self.isTemporary = isTemporary ?? false
            }
        }

        let files: [File]

        init(from decoder: Decoder) throws {
            self.files = try decoder.singleValueContainer().decode([File].self)
        }
    }

    struct Deletions: Codable {
        struct File: Codable, ConfigurationValidating {
            private enum CodingKeys: String, CodingKey {
                case fileName
            }

            let fileName: String

            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                let fileName = try container.decode(String.self, forKey: .fileName)
                self.fileName = try Self.validate(fileName: fileName,
                                                  forKey: CodingKeys.fileName.rawValue).absoluteString
            }
        }

        let files: [File]

        init(from decoder: Decoder) throws {
            self.files = try decoder.singleValueContainer().decode([File].self)
        }
    }

    struct Languages: Codable {
        struct File: Codable, ConfigurationValidating {
            private enum CodingKeys: String, CodingKey {
                case fileName
                case language
            }

            let fileName: String
            let language: String

            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                let fileName = try container.decode(String.self, forKey: .fileName)
                self.fileName = try Self.validate(fileName: fileName,
                                                  forKey: CodingKeys.fileName.rawValue).absoluteString

                self.language = try container.decode(String.self, forKey: .language)
            }
        }

        struct Directory: Codable {
            let language: String
        }

        let files: [File]?
        let directory: Directory?
    }

    struct MimeTypes: Codable {
        struct File: Codable, ConfigurationValidating {
            private enum CodingKeys: String, CodingKey {
                case fileName
                case mimeType
            }

            let fileName: String
            let mimeType: String

            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                let fileName = try container.decode(String.self, forKey: .fileName)
                self.fileName = try Self.validate(fileName: fileName,
                                                  forKey: CodingKeys.fileName.rawValue).absoluteString

                self.mimeType = try container.decode(String.self, forKey: .mimeType)
            }
        }

        struct Directory: Codable {
            let mimeType: String
        }

        let files: [File]?
        let directory: Directory?
    }

    let authentication: Authentication?
    let redirects: Redirects?
    let deletions: Deletions?
    let languages: Languages?
    let mimeTypes: MimeTypes?
}
