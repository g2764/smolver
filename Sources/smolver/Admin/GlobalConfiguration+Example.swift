//
//  GlobalConfiguration+Example.swift
//  smolver
//
//  Created by Justin Marshall on 7/3/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension GlobalConfiguration {
    static var example: Self {
        let staticContent = GlobalConfiguration.StaticContent(allowedFileDownloadPaths: [
            "gemlog/",
            "tags/",
            "anySubdirectory"
        ])
        let cgi = GlobalConfiguration.CGI(whitelistedScriptFiles: [
            "scriptDir1/myscriptFile",
            "script2/test.sh",
            "script3",
            "script4.sh"
        ])
        let encryption = GlobalConfiguration.Encryption(certificateAuthorityPath: "location/of/certificateAuthority.pem",
                                                        certificateFile: "location/of/certificate.pem",
                                                        privateKeyFile: "location/of/private.pem")
        let logs = GlobalConfiguration.Logs(ipAddresses: false,
                                            level: .info,
                                            logPath: "logs/")
        let globalConfig = GlobalConfiguration(maintenanceMessage: "Define maintenance reason here.",
                                               host: "your.dns.host.name",
                                               ip: "127.0.0.1",
                                               requestInterval: 3,
                                               language: "en",
                                               shouldServeRobotsFile: true,
                                               staticContent: staticContent,
                                               cgi: cgi,
                                               encryption: encryption,
                                               logs: logs)
        return globalConfig
    }
}

private extension GlobalConfiguration {
    init(
        maintenanceMessage: String?,
        host: String,
        ip: String?,
        requestInterval: UInt?,
        language: String,
        shouldServeRobotsFile: Bool,
        staticContent: StaticContent,
        cgi: CGI?,
        encryption: Encryption,
        logs: Logs
    ) {
        self.maintenanceMessage = maintenanceMessage
        self.host = host
        self.ip = ip
        self.requestInterval = requestInterval
        self.language = language
        self.shouldServeRobotsFile = shouldServeRobotsFile
        self.staticContent = staticContent
        self.cgi = cgi
        self.encryption = encryption
        self.logs = logs
    }
}

private extension GlobalConfiguration.StaticContent {
    init(allowedFileDownloadPaths: [String]) {
        self.allowedFileDownloadPaths = allowedFileDownloadPaths.compactMap(URL.init(string:))
        precondition(self.allowedFileDownloadPaths.count == allowedFileDownloadPaths.count)
     }
}

private extension GlobalConfiguration.CGI {
    init(whitelistedScriptFiles: [String]) {
        self.whitelistedScriptFiles = whitelistedScriptFiles.compactMap(URL.init(string:))
        precondition(self.whitelistedScriptFiles.count == whitelistedScriptFiles.count)
    }
}

private extension GlobalConfiguration.Encryption {
    init(
        certificateAuthorityPath: String?,
        certificateFile: String,
        privateKeyFile: String
    ) {
        if let certificateAuthorityPath {
            self.certificateAuthorityPath = URL(string: certificateAuthorityPath)
        } else {
            self.certificateAuthorityPath = nil
        }

        self.certificateFile = URL(string: certificateFile) ?? URL(fileURLWithPath: "")
        self.privateKeyFile = URL(string: privateKeyFile) ?? URL(fileURLWithPath: "")
    }
}

private extension GlobalConfiguration.Logs {
    init(ipAddresses: Bool?, level: Level, logPath: String?) {
        self.ipAddresses = ipAddresses
        self.level = level

        if let logPath {
            self.logPath = URL(string: logPath)
        } else {
            self.logPath = nil
        }
    }
}
