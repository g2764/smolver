//
//  Command.swift
//  smolver
//
//  Created by Justin Marshall on 4/21/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol Command {
    var name: String { get }
    var description: String { get }
    var version: String { get }

    func run()
}

extension Command {
    var name: String {
        "\(type(of: self))"
            .replacingOccurrences(of: "Command", with: "")
            .replacingOccurrences(of: ".Type", with: "")
            .lowercased()
    }
}

struct SmolverCommand: Command {
    var description: String {
    """
    Usage:
        \(name) run
            Attempt to start a Gemini server running locally on port 1965. A ~/.smolver directory must
            exist, and it must contain a config.json file.

        \(name) generateGlobalConfig
            Output to stdout the json data required for a new global configuration. This is intended
            to be piped into a file. Data generated will cover all supported configuration parameters,
            not all of which are required. You will need to modify the output for your needs.
            For detailed information on each parameter, see the man page or the README.
            Example:    \(name) generateGlobalConfig > ~/.smolver/config.json

        \(name) generateLocalConfig
            Output to stdout the json data required for a new local configuration. This is intended
            to be piped into a file. Data generated will cover all supported configuration parameters,
            not all of which are needed in most cases. You will need to modify the output for your needs.
            For detailed information on each parameter, see the man page or the README.
            Example:    \(name) generateLocalConfig > ~/.smolver/gemlog/.smol.json

        \(name) validate
            Find and validate all configuration files in ~/.smolver, recursively. Includes both your
            config.json, and .smol.json files, if present.

        \(name) --version
        \(name) -v
            Output version information and exit.

        \(name) --help
        \(name) -h
            Display this help and exit.
    """
    }

    let version = "v1.5.4"

    func run() {
        // this whole function is sloppy, especially the `switch` statement
        // good enough for now, though

        let arguments = CommandLine.arguments
        guard arguments.count == 2 else {
            print("Try '" + name + " --help' or '" + name + " -h' for more information.")
            exit(EXIT_FAILURE)
        }

        switch arguments[1] {
        case "--version",
             "-v":
            print(name + " " + version)
            exit(EXIT_SUCCESS)
        case "--help",
             "-h":
            print(description)
            exit(EXIT_SUCCESS)
        case "generateGlobalConfig":
            printOutput(of: GlobalConfiguration.example.jsonify)
        case "generateLocalConfig":
            printOutput(of: LocalConfiguration.example.jsonify)
        case "validate":
            validate()
            exit(EXIT_SUCCESS)
        case "run":
            let app = App()
            app.run()
        default:
            print(name + ": invalid option -- '" + arguments[1]  + "'")
            print("Try '" + name + " --help' or '" + name + " -h' for more information.")
            exit(EXIT_FAILURE)
        }
    }

    private struct StderrTextOutputStream: TextOutputStream {
        func write(_ string: String) {
            let stderr = FileHandle.standardError
            stderr.write(Data(string.utf8))
        }
    }

    private func printOutput<T>(of otherFunc: () throws -> T) {
        do {
            let json = try otherFunc()
            print(json)
            exit(EXIT_SUCCESS)
        } catch {
            var standardError = StderrTextOutputStream()
            print(error.localizedDescription, to: &standardError)
            exit(EXIT_FAILURE)
        }
    }

    private func validate() {
        validateGlobalConfig()
        print()
        validateLocalConfigs()
    }

    private func validateGlobalConfig() {
        print("Validating global config file...")

        guard let configFileData = FileManager.default.contents(atPath: GlobalConfiguration.configFilePath) else {
            print("    No global config file found at \(GlobalConfiguration.configFilePath)")
            return
        }

        do {
            // this is simply to detect errors in config file
            // we do not need to do anything with the return value
            // hence the `_ =`
            _ = try JSONDecoder().decode(GlobalConfiguration.self, from: configFileData)
            print("    \(GlobalConfiguration.configFilePath): correct. \(name) should startup successfully.")
        } catch {
            print("    \(GlobalConfiguration.configFilePath): incorrect. Reason: \(error.localizedDescription)")
        }
    }

    private func validateLocalConfigs() {
        print("Validating local config files...")

        let enumerator = FileManager.default.enumerator(atPath: GlobalConfiguration.configDirectory)
        var localConfigFilePaths: [String] = []
        while let element = enumerator?.nextObject() as? String {
            // including this condition within the while statement directly,
            // like you would in an if or guard, mysteriously fails...
            guard element.hasSuffix(LocalConfiguration.fileName) else { continue }
            localConfigFilePaths.append(GlobalConfiguration.configDirectory.appendingPathComponent(element))
        }

        guard !localConfigFilePaths.isEmpty else {
            print("    None detected.")
            return
        }

        localConfigFilePaths
            .map { localConfigFilePath in
                (localConfigFilePath, FileManager.default.contents(atPath: localConfigFilePath))
            }
            .forEach { (localConfigFilePath, data) in
                guard let data else {
                    print("     \(localConfigFilePath) unable to be opened.")
                    return
                }

                do {
                    _ = try JSONDecoder().decode(LocalConfiguration.self, from: data)
                    print("     \(localConfigFilePath): correct.")
                } catch {
                    print("    \(localConfigFilePath): invalid. Reason: \(error.localizedDescription)")
                }
            }
    }
}
