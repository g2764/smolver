//
//  Log.swift
//  smolver
//
//  Created by Justin Marshall on 1/14/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

enum Source {
    case system
    case remote(hostname: String)

    fileprivate var logPrefix: String {
        switch self {
        case .system:
            return "SYSTEM"
        case .remote(let hostname):
            return hostname
        }
    }

    fileprivate static var maxLength: Int {
        "xxx.xxx.xxx.xxx".count
    }
}

protocol Logging {
    static func config(_ message: String)
    static func error(_ message: String, from source: Source)
    static func warn(_ message: String, from source: Source)
    static func info(_ message: String, from source: Source)
    static func debug(_ message: String, from source: Source)
}

enum Log: Logging {
    static var config: GlobalConfiguration.Logs?

    private static let currentDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd" // 2022-01-30
        return formatter
    }()

    private static let fileWritingLockQueue = DispatchQueue(label: "com.smolver.logFileWriting")

    static func config(_ message: String) {
        // As this type of log is only intended to run on app launch, and
        // only for sysadmin misconfigurations, having conditions around
        // logging this would be detrimental to admins. Hence the direct
        // call to `log(_, from:)` without the `threshold` parameter, and
        // the lack of a `source` parameter in `config(_:)`.
        log("MISCONFIGURATION: " + message, from: .system)
    }

    static func error(_ message: String, from source: Source) {
        log(message, from: source, threshold: .error)
    }

    static func warn(_ message: String, from source: Source) {
        log(message, from: source, threshold: .warn)
    }

    static func info(_ message: String, from source: Source) {
        log(message, from: source, threshold: .info)
    }

    static func debug(_ message: String, from source: Source) {
        log(message, from: source, threshold: .verbose)
    }

    private static func log(_ message: String, from source: Source, threshold: GlobalConfiguration.Logs.Level, shouldLog: Bool = true) {
        guard let config = config,
            config.level >= threshold else {
            return
        }

        let paddedThresholdPrefix = (threshold.logPrefix + ":").padding(toLength: GlobalConfiguration.Logs.Level.maxLength + 1, // +1 for the ":"
                                                                        withPad: " ",
                                                                        startingAt: 0)
        log(paddedThresholdPrefix + " " + message, from: source, includeIP: config.ipAddresses ?? false, shouldLog: shouldLog)
    }

    private static func log(_ message: String, from source: Source, includeIP: Bool = true, shouldLog: Bool = true) {
        let timestamp = Formatters.time.string(from: Date())
        let paddedIP = (source.logPrefix + ":").padding(toLength: Source.maxLength + 1, // +1 for the ":"
                                                        withPad: " ",
                                                        startingAt: 0)
        let paddedSource = includeIP ? paddedIP + " " : ""
        let fullMessage = (timestamp + ": " + paddedSource + message).replacingOccurrences(of: "\r\n", with: "")
        print(fullMessage)

        if shouldLog {
            // Do not block the current thread for this. Allows responses to be
            // returned to client while file system access runs on another thread.
            DispatchQueue.global(qos: .background).async {
                writeToDisk(fullMessage)
            }
        }
    }

    private static func writeToDisk(_ message: String) {
        // at this point, we are in a background thread and this one *should* block so that the log file doesn't get out of whack
        fileWritingLockQueue.sync {
            guard let logPath = config?.logPath else {
                return
            }

            // I vacillated between a single log file for everything and separating them by arbitrary time intervals. I went with
            // starting a new file for every day; that'll make it easier for admins to purge old logs as well as give a clear
            // demarcation between days, which will let admins be able to determine relative traffic levels based solely on the daily
            // log file sizes (assuming that the configured log level isn't changing).
            let currentDateString = currentDateFormatter.string(from: Date())
            let logFile = GlobalConfiguration.configDirectory + logPath.appendingPathComponent("\(currentDateString).txt").absoluteString
            var message = message

            if let existingLogFileData = FileManager.default.contents(atPath: logFile),
                let existingLogFileContents = String(data: existingLogFileData, encoding: .utf8) {
                    message = existingLogFileContents + "\n" + message
            }

            do {
                try message.write(toFile: logFile, atomically: true, encoding: .utf8)
            } catch {
                // don't go through `Self.error`, because that calls back into this function, causing infinite recursion
                log(error.localizedDescription, from: .system, threshold: .error, shouldLog: false)
            }
        }
    }
}

private extension GlobalConfiguration.Logs.Level {
    var logPrefix: String {
        switch self {
        case .none:
            return ""
        case .error:
            return "ERROR"
        case .warn:
            return "WARNING"
        case .info:
            return "INFO"
        case .verbose:
            return "DEBUG"
        }
    }

    static var maxLength: Int {
        allCases.max(by: { $0.logPrefix.length < $1.logPrefix.length })?.logPrefix.length ?? 0
    }
}
