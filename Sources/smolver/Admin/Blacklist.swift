//
//  Blacklist.swift
//  smolver
//
//  Created by Justin Marshall on 5/22/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

/// This struct encapsulates the rules for which files are blacklisted from being served.
/// The only URIs that are checked are those that are known at compile time to be both
/// sensitive and by design contained in a directory that also contains servable content.
///
/// Log and TLS files were also considered for blacklisting, but, in the interest of simplicity,
/// instead that is left up to you as the server admin; if you don't want those served, don't
/// put them under the ``staticContent.allowedFileDownloadPaths`` object in ``config.json``.
///
/// - Attention: Blacklisted files include:
///
/// * Configuration files
///   * ``.smol.json``
///   * ``config.json``
/// * Anything related to anonymous authentication or authorization
///   * ``.anonymous-authentications/`` [used internally]
enum Blacklist {
    static func shouldBlock(_ request: RequestProtocol, from clientHostname: String, logger: Logging.Type = Log.self) -> Bool {
        logger.debug("Checking blacklist status", from: .remote(hostname: clientHostname))

        let isLocalConfigFile = request.url.lastPathComponent == LocalConfiguration.fileName
        guard !isLocalConfigFile else {
            logger.warn("Client attempting to access \(LocalConfiguration.fileName) file", from: .remote(hostname: clientHostname))
            return true
        }

        let isGlobalConfigFile = request.url.path == "/" + GlobalConfiguration.configFileName
        guard !isGlobalConfigFile else {
            logger.warn("Client attempting to access \(GlobalConfiguration.configFileName) file", from: .remote(hostname: clientHostname))
            return true
        }

        let isWithinAnonymousAuthPath = request.url.pathComponents.contains(String(ClientCertificateValidator.anonymousAuthenticationsDirectory.dropLast()))
        guard !isWithinAnonymousAuthPath else {
            logger.warn("Client attempting to access \(ClientCertificateValidator.anonymousAuthenticationsDirectory) directory", from: .remote(hostname: clientHostname))
            return true
        }

        logger.debug("Requested file not blacklisted", from: .remote(hostname: clientHostname))
        return false
    }
}
