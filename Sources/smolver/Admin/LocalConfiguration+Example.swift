//
//  LocalConfiguration+Example.swift
//  smolver
//
//  Created by Justin Marshall on 7/5/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension LocalConfiguration {
    private static var authentication: Authentication {
        let file1 = LocalConfiguration.Authentication.File(name: "authenticated-file1.gmi",
                                                          whitelistedPath: "whitelisted/path/relative/to/root/dir/containing/pem/certificate/files/")
        let file2 = LocalConfiguration.Authentication.File(name: "authenticated-file2.gmi",
                                                          whitelistedPath: "whitelisted/path/relative/to/root/dir/containing/pem/certificate/files/")
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: [
                                                                            "unauthenticatedFile1InAuthenticatedPath1.ext",
                                                                            "unauthenticatedFile2InAuthenticatedPath2.ext"
                                                                    ],
                                                                    whitelistedPath: "whitelisted/path/relative/to/root/dir/containing/pem/certificate/files/")
        let authentication = LocalConfiguration.Authentication(files: [file1, file2],
                                                               directory: directory)
        return authentication
    }

    private static var redirects: Redirects {
        let file1 = LocalConfiguration.Redirects.File(oldName: "name-of-an-old-file-in-this-directory.gmi",
                                                      newName: "new-file-name-1.gmi",
                                                      newPath: "new/path/relative/to/config",
                                                      isTemporary: true)
        let file2 = LocalConfiguration.Redirects.File(oldName: "name-of-another-old-file-in-this-directory.gmi",
                                                      newName: "new-file-name-2.gmi",
                                                      newPath: "new/path/relative/to/config",
                                                      isTemporary: false)
        let redirects = LocalConfiguration.Redirects(files: [file1, file2])
        return redirects
    }

    private static var deletions: Deletions {
        let file1 = LocalConfiguration.Deletions.File(fileName: "deleted-file-name-1.gmi")
        let file2 = LocalConfiguration.Deletions.File(fileName: "deleted-file-name-2.gmi")
        let deletions = LocalConfiguration.Deletions(files: [file1, file2])
        return deletions
    }

    private static var languages: Languages {
        let file1 = LocalConfiguration.Languages.File(fileName: "de-que-es-this-capsule.gmi",
                                                      language: "en-US,es")
        let file2 = LocalConfiguration.Languages.File(fileName: "hola-amigos.gmi",
                                                      language: "es-MX")
        let file3 = LocalConfiguration.Languages.File(fileName: "au-revoir",
                                                      language: "fr-FR")
        let directory = LocalConfiguration.Languages.Directory(language: "de-DE")
        let languages = LocalConfiguration.Languages(files: [file1, file2, file3],
                                                     directory: directory)
        return languages
    }

    private static var mimeTypes: MimeTypes {
         let file1 = LocalConfiguration.MimeTypes.File(fileName: "index.pdf",
                                                       mimeType: "application/octet-stream")
         let file2 = LocalConfiguration.MimeTypes.File(fileName: "index.md",
                                                       mimeType: "text/plain")
         let file3 = LocalConfiguration.MimeTypes.File(fileName: "au-revoir.gmi",
                                                       mimeType: "application/json")
         let directory = LocalConfiguration.MimeTypes.Directory(mimeType: "text/markdown")
         let mimeTypes = LocalConfiguration.MimeTypes(files: [file1, file2, file3],
                                                      directory: directory)
         return mimeTypes
     }

     static var example: Self {
        let localConfig = LocalConfiguration(authentication: authentication,
                                             redirects: redirects,
                                             deletions: deletions,
                                             languages: languages,
                                             mimeTypes: mimeTypes)
        return localConfig
    }
}

private extension LocalConfiguration.Redirects {
    init(
        files: [File]
    ) {
        self.files = files
    }
}

private extension LocalConfiguration.Redirects.File {
    init(
        oldName: String,
        newName: String,
        newPath: String,
        isTemporary: Bool
    ) {
        self.oldName = oldName
        self.newName = newName
        self.newPath = URL(string: newPath) ?? URL(fileURLWithPath: newPath)
        self.isTemporary = isTemporary
    }
}

private extension LocalConfiguration.Deletions {
    init(
        files: [File]
    ) {
        self.files =  files
    }
}

private extension LocalConfiguration.Deletions.File {
    init(
        fileName: String
    ) {
        self.fileName = fileName
    }
}

private extension LocalConfiguration.Languages.File {
    init(
        fileName: String,
        language: String
    ) {
        self.fileName = fileName
        self.language = language
    }
}

private extension LocalConfiguration.MimeTypes.File {
    init(
        fileName: String,
        mimeType: String
    ) {
        self.fileName = fileName
        self.mimeType = mimeType
    }
}
