//
//  GlobalConfiguration.swift
//  smolver
//
//  Created by Justin Marshall on 1/25/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

struct GlobalConfiguration: Codable {
    enum DecodingError: LocalizedError {
        case noSuchLogLevel(key: String, path: String)

        var errorDescription: String? {
            switch self {
            case .noSuchLogLevel(let key, let level):
                return """
                invalid log level specified. Offending element: "\(key)": "\(level)". Available options include: \(Logs.Level.allCases.map { String($0.rawValue) }))
                """
            }
        }
    }

    static let smolverSubdirectory = "/.smolver/"
    static let configDirectory = NSHomeDirectory() + smolverSubdirectory

    static let configFileName = "config.json"
    static var configFilePath: String { configDirectory + configFileName }

    struct StaticContent: Codable, ConfigurationValidating {
        private enum CodingKeys: String, CodingKey {
            case allowedFileDownloadPaths
        }

        let allowedFileDownloadPaths: [URL]

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            self.allowedFileDownloadPaths = try container
                                                  .decode([String].self, forKey: .allowedFileDownloadPaths)
                                                  .map { try Self.validate(directoryPath: $0, forKey: CodingKeys.allowedFileDownloadPaths.rawValue) }
        }
    }

    struct CGI: Codable, ConfigurationValidating {
        private enum CodingKeys: String, CodingKey {
            case whitelistedScriptFiles
        }

        let whitelistedScriptFiles: [URL]

        #if DEBUG
        /// For testing purposes only
        init(whitelistedScriptFiles: [URL]) {
            self.whitelistedScriptFiles = whitelistedScriptFiles
        }
        #endif

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            self.whitelistedScriptFiles = try container
                                          .decode([String].self, forKey: .whitelistedScriptFiles)
                                          .map { try Self.validate(filePath: $0, forKey: CodingKeys.whitelistedScriptFiles.rawValue) }
        }
    }

    struct Encryption: Codable, ConfigurationValidating {
        private enum CodingKeys: String, CodingKey {
            case certificateAuthorityPath
            case certificateFile
            case privateKeyFile
        }

        let certificateAuthorityPath: URL?
        let certificateFile: URL
        let privateKeyFile: URL

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            if let certificateAuthorityPath = try container.decodeIfPresent(String.self, forKey: .certificateAuthorityPath) {
                self.certificateAuthorityPath = try Self.validate(directoryPath: certificateAuthorityPath, forKey: CodingKeys.certificateAuthorityPath.rawValue)
            } else {
                self.certificateAuthorityPath = nil
            }

            let certificateFile = try container.decode(String.self, forKey: .certificateFile)
            self.certificateFile = try Self.validate(filePath: certificateFile, forKey: CodingKeys.certificateFile.rawValue)

            let privateKeyFile = try container.decode(String.self, forKey: .privateKeyFile)
            self.privateKeyFile = try Self.validate(filePath: privateKeyFile, forKey: CodingKeys.privateKeyFile.rawValue)
        }
    }

    struct Logs: Codable, ConfigurationValidating {
        private enum CodingKeys: String, CodingKey {
            case ipAddresses
            case level
            case logPath
        }

        enum Level: String, Codable, CaseIterable, Comparable {
            case none
            case error
            case warn
            case info
            case verbose

            private var order: UInt {
                switch self {
                case .none:
                    return 0
                case .error:
                    return 1
               case .warn:
                    return 2
                case .info:
                    return 3
                case .verbose:
                    return 4
                }
            }

            static func <(lhs: Level, rhs: Level) -> Bool {
                lhs.order < rhs.order
            }

            init(from decoder: Decoder) throws {
                let container = try decoder.singleValueContainer()
                let rawLevel = try container.decode(String.self)
                guard let level = Level(rawValue: rawLevel) else {
                    throw DecodingError.noSuchLogLevel(key: CodingKeys.level.rawValue, path: rawLevel)
                }

                self = level
            }
        }

        let ipAddresses: Bool?
        let level: Level
        let logPath: URL?

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            self.ipAddresses = try container.decodeIfPresent(Bool.self, forKey: .ipAddresses)
            self.level = try container.decode(Level.self, forKey: .level)

            if let logPath = try container.decodeIfPresent(String.self, forKey: .logPath) {
                self.logPath = try Self.validate(directoryPath: logPath, forKey: CodingKeys.logPath.rawValue)
            } else {
                self.logPath = nil
            }

            // `logPath` is only optional only if log level is `none`; required in all other cases
            if logPath == nil {
                guard level == .none else {
                    throw GeneralConfigFileDecodingError.illegalLoggingConfiguration(reason: "illegal logging configuration. `logPath` is optional only if log level is `none`; required in all other cases; if still provided it is ignored")
                }
            }

            // `ipAddresses` is only optional only if log level is `none`; required in all other cases
            if ipAddresses == nil {
                guard level == .none else {
                    throw GeneralConfigFileDecodingError.illegalLoggingConfiguration(reason: "illegal logging configuration. `ipAddresses` is optional only if log level is `none`; required in all other cases; if still provided it is ignored")
                }
            }
        }
    }

    let maintenanceMessage: String?
    let host: String
    let ip: String?
    let requestInterval: UInt?
    let language: String
    let shouldServeRobotsFile: Bool
    let staticContent: StaticContent
    let cgi: CGI?
    let encryption: Encryption
    let logs: Logs

    private static var defaultLanguage: String { "en" }

    private enum CodingKeys: String, CodingKey {
        case maintenanceMessage
        case host
        case ip
        case requestInterval
        case language
        case shouldServeRobotsFile
        case staticContent
        case cgi
        case encryption
        case logs
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.maintenanceMessage = try container.decodeIfPresent(String.self, forKey: .maintenanceMessage)
        self.host = try container.decode(String.self, forKey: .host)
        self.ip = try container.decodeIfPresent(String.self, forKey: .ip)
        self.requestInterval = try container.decodeIfPresent(UInt.self, forKey: .requestInterval)
        self.language = try container.decodeIfPresent(String.self, forKey: .language) ?? Self.defaultLanguage
        self.shouldServeRobotsFile = try container.decode(Bool.self, forKey: .shouldServeRobotsFile)
        self.staticContent = try container.decode(StaticContent.self, forKey: .staticContent)
        self.cgi = try container.decodeIfPresent(CGI.self, forKey: .cgi)
        self.encryption = try container.decode(Encryption.self, forKey: .encryption)
        self.logs = try container.decode(Logs.self, forKey: .logs)
    }
}
