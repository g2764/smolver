//
//  Request.swift
//  smolver
//
//  Created by Justin Marshall on 1/11/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol RequestProtocol {
    var url: URL { get }
    var path: String { get }
    var query: String? { get }
}

struct Request: RequestProtocol {
    enum Error: Swift.Error {
        typealias PermanentFailure = Response.Status.Failure.Permanent

        case reason(PermanentFailure)
    }

    let url: URL
    let path: String
    let query: String?

    /// Hacky workaround that accomplishes:
    /// If requesting directory that will happen to contain an index.[gmi|gemini],
    /// then this will force a server-side redirect so that the client and server
    /// URLs stay in sync. That way relative links when *not* at capsule root can
    /// be resolved and returned. Else has no effect.
    private(set) var forceRedirectIfIndexShortcut = false

    init(_ rawRequest: String, from hostname: String, expectations: (host: String, ip: String?)) throws {
        guard !rawRequest.hasPrefix("\u{FEFF}") else {
            Log.debug("Invalid request prefix", from: .remote(hostname: hostname))
            throw Error.reason(.badRequest(userInfo: nil))
        }

        guard let marker = rawRequest.range(of: "\r\n") else {
            Log.debug("Missing \\r\\n", from: .remote(hostname: hostname))
            throw Error.reason(.badRequest(userInfo: nil))
        }

        Log.debug("Removing everything after <CR><LF>", from: .remote(hostname: hostname))
        let urlString = String(rawRequest[rawRequest.startIndex..<marker.lowerBound])

        Log.debug("Checking URL string of \(urlString)", from: .remote(hostname: hostname))
        guard let url = URL(string: urlString) else {
            Log.debug("Invalid URL string", from: .remote(hostname: hostname))
            throw Error.reason(.badRequest(userInfo: nil))
        }

        Log.debug("Standardizing", from: .remote(hostname: hostname))
        let standardizedURL = url.standardized

        guard standardizedURL == url else {
            Log.warn("Client attempting to escape capsule contents' root of \(GlobalConfiguration.configDirectory)", from: .remote(hostname: hostname))
            throw Error.reason(.permanent(userInfo: nil))
        }

        Log.debug("Validating request size", from: .remote(hostname: hostname))
        guard let urlStringData = standardizedURL.absoluteString.data(using: .utf8),
            urlStringData.count <= 1024 else {
                Log.debug("Request too large", from: .remote(hostname: hostname))
                throw Error.reason(.badRequest(userInfo: nil))
        }

        Log.debug("Performing basic URL validation", from: .remote(hostname: hostname))
        guard !standardizedURL.isRelative else {
            Log.info("Illegal relative URL", from: .remote(hostname: hostname))
            throw Error.reason(.badRequest(userInfo: nil))
        }

        guard standardizedURL.scheme != nil else {
            Log.info("Missing scheme", from: .remote(hostname: hostname))
            throw Error.reason(.badRequest(userInfo: nil))
        }

        guard standardizedURL.scheme == "gemini" else {
            Log.info("Invalid scheme: \(standardizedURL.scheme ?? "NIL SCHEME")", from: .remote(hostname: hostname))
            throw Error.reason(.proxyRequestRefused(userInfo: "Only the gemini:// protocol is supported over this port."))
        }

        guard standardizedURL.host == expectations.host || standardizedURL.host == expectations.ip else {
            Log.info("Invalid host: \(standardizedURL.host ?? "NIL HOST")", from: .remote(hostname: hostname))
            throw Error.reason(.proxyRequestRefused(userInfo: "Incorrect host requested"))
        }

        guard standardizedURL.user == nil,
              standardizedURL.host != nil,
              !standardizedURL.path.contains("+") else {
                  Log.debug("Invalid URL contents", from: .remote(hostname: hostname))
                  throw Error.reason(.badRequest(userInfo: nil))
        }

        Log.debug("Checking port", from: .remote(hostname: hostname))
        if let port = standardizedURL.port,
            port != 1965 {
                Log.info("Invalid port: \(port)", from: .remote(hostname: hostname))
                throw Error.reason(.proxyRequestRefused(userInfo: "Gemini runs on port 1965, not \(port)."))
        }

        Log.debug("Checking query string", from: .remote(hostname: hostname))

        // I'm going with the assumption that if this Swift API returns a query, then it's a valid query
        if let query = standardizedURL.query {
            Log.debug("Found valid query string: \(query)", from: .remote(hostname: hostname))
            self.query = query
        } else {
            Log.debug("No query string", from: .remote(hostname: hostname))
            self.query = nil
        }

        if standardizedURL.path.isEmpty {
            Log.debug("Empty path, defaulting to root (/)", from: .remote(hostname: hostname))
            self.path = "/"
        } else {
            Log.debug("Non-empty path of \(standardizedURL.path)", from: .remote(hostname: hostname))

            let pathToUse = standardizedURL.path
            let isRequestingDirectoryWithoutTrailingSlash = query == nil && !pathToUse.contains(".") && !standardizedURL.absoluteString.hasSuffix("/")
            // if there is no query and it's not a file url, then assume it's a directory
            if isRequestingDirectoryWithoutTrailingSlash {
                self.forceRedirectIfIndexShortcut = true
            }

            self.path = pathToUse
        }


        self.url = standardizedURL
    }
}

private extension URL {
    var isRelative: Bool {
        (host == nil && scheme == nil) || (host == nil)
    }
}
