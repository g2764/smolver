//
//  App.swift
//  smolver
//
//  Created by Justin Marshall on 2/24/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import SocketServer

final class App {
    private var rateLimiter: RateLimiter?

    func run() {
        guard let configFileData = FileManager.default.contents(atPath: GlobalConfiguration.configFilePath) else {
            Log.config("No config file found at \(GlobalConfiguration.configFilePath)")
            exit(EXIT_FAILURE)
        }

        do {
            let configuration = try JSONDecoder().decode(GlobalConfiguration.self, from: configFileData)
            Log.config = configuration.logs

            if let requestInterval = configuration.requestInterval {
                rateLimiter = .init(rateLimitingLockQueue: DispatchQueue(label: "com.smolver.rateLimitingLockQueue"), requestInterval: requestInterval)
            }

            let server = SocketServer(
                            port: 1965,
                            sslConfiguration: configuration.sslConfiguration,
                            responseOnConnect: { [weak self] rawRequest, connection, certificate in
                                if let rateLimiter = self?.rateLimiter,
                                    rateLimiter.shouldLimit(hostname: connection.hostname) {
                                        Log.info("request: \(rawRequest ?? "")", from: .remote(hostname: connection.hostname))

                                        let interval = rateLimiter.requestInterval
                                        let message = "Slow down! Requests must be at least \(interval) second\(interval == 1 ? "" : "s") apart."
                                        let response = Response(status: .failure(.temporary(.slowDown(userInfo: message))))

                                        self?.rateLimiter?.responded(to: connection.hostname, with: response.status)
                                        Log.info("response: \(response.header) [header]", from: .remote(hostname: connection.hostname))
                                        return (response.header, response.body)
                                } else {
                                    if let maintenanceMessage = configuration.maintenanceMessage {
                                        Log.info("request: \(rawRequest ?? "")", from: .remote(hostname: connection.hostname))
                                        let response = Response(status: .failure(.temporary(.serverUnavailable(userInfo: maintenanceMessage))))

                                        self?.rateLimiter?.responded(to: connection.hostname, with: response.status)
                                        Log.info("response: \(response.header) [header]", from: .remote(hostname: connection.hostname))
                                        return (response.header, response.body)
                                    } else {
                                        let router = Router(configuration: configuration,
                                                            rawRequest: rawRequest,
                                                            certificate: certificate,
                                                            hostname: connection.hostname)
                                        let response = router.route()

                                        self?.rateLimiter?.responded(to: connection.hostname, with: response.status)
                                        Log.info("response: \(response.header) [header]", from: .remote(hostname: connection.hostname))
                                        return (response.header, response.body)
                                    }
                                }
                            }
                         )
            server.onListen {
                Log.info("smolver listening", from: .system)
            }
            .onAccept { sourceHost in
                Log.debug("Accepted connected from \(sourceHost)", from: .system)
            }
            .onError { error in
                Log.error("\(error)", from: .system)
            }
            .onUnrecoverableError { [weak server] error in
                Log.error("Aborting due to unrecoverable error: \(error)", from: .system)
                // for all practical purposes, capturing a weak reference of
                // server makes no difference because we shutdown the app here,
                // but it's a good practice
                server?.shutdown()
            }
            .onUserInput { [weak server] sourceHost, userInput in
                Log.error("Aborting due to unexpected user input of \"\(userInput)\" from \(sourceHost)", from: .system)
                // for all practical purposes, capturing a weak reference of
                // server makes no difference because we shutdown the app here,
                // but it's a good practice
                server?.shutdown()
            }
            .onClose { sourceHost in
                Log.debug("Closed connection from \(sourceHost)", from: .system)
            }

            server.boot()
        } catch {
            Log.config("Invalid config file at \(GlobalConfiguration.configFilePath). Reason: \(error.localizedDescription)")
            exit(EXIT_FAILURE)
        }
    }
}

private extension GlobalConfiguration {
    var sslConfiguration: SSLConfiguration {
        if let caCertificateDirectory = encryption.certificateAuthorityPath {
            return SSLConfiguration(
                caCertificateDirectory: Self.configDirectory + caCertificateDirectory.absoluteString,
                certificateFilePath: Self.configDirectory + encryption.certificateFile.absoluteString,
                keyFilePath: Self.configDirectory + encryption.privateKeyFile.absoluteString
            )
        } else {
            return SSLConfiguration(
                caCertificateDirectory: nil,
                certificateFilePath: Self.configDirectory + encryption.certificateFile.absoluteString,
                keyFilePath: Self.configDirectory + encryption.privateKeyFile.absoluteString
            )
        }
    }
}
