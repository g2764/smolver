//
//  EnvironmentVariables.swift
//  smolver
//
//  Created by Justin Marshall on 2/21/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

struct EnvironmentVariables: CustomDebugStringConvertible {
    enum Key: Hashable, CustomDebugStringConvertible {
        enum Authenticated: Hashable {
            enum Certificate: String {
                case fingerprint = "X_CERTIFICATE_FINGERPRINT"
                case notAfter    = "X_CERTIFICATE_NOT_AFTER"
                case notBefore   = "X_CERTIFICATE_NOT_BEFORE"
                case username    = "X_CERTIFICATE_USERNAME"
            }

            case certificate(Certificate)
            case type

            var stringValue: String {
                switch self {
                case .type:
                    return "AUTH_TYPE"
                case .certificate(let certificate):
                    return certificate.rawValue
                }
            }
        }

        enum Network: String {
            case gatewayInterface = "GATEWAY_INTERFACE"
            case url              = "GEMINI_URL"
            case pathInfo         = "PATH_INFO"
            case queryString      = "QUERY_STRING"
            case remoteHost       = "REMOTE_HOST"

            var stringValue: String { rawValue }
        }

        enum Script: String {
            case name = "SCRIPT_NAME"

            var stringValue: String { rawValue }
        }

        enum Server: String {
            case  name       = "SERVER_NAME"
            case  port       = "SERVER_PORT"
            case `protocol`  = "SERVER_PROTOCOL"
            case  software   = "SERVER_SOFTWARE"

            var stringValue: String { rawValue }
        }

        var stringValue: String {
            switch self {
            case .auth(let auth):
                return auth.stringValue
            case .network(let network):
                return network.stringValue
            case .script(let script):
                return script.stringValue
            case .server(let server):
                return server.stringValue
            }
        }

        case auth(Authenticated)
        case network(Network)
        case script(Script)
        case server(Server)

        var debugDescription: String { stringValue }
    }

    var debugDescription: String {
        "\n" +
        dictionary.map { key, value in
            "\"\(key)\": \"\(value)\""
        }
        .joined(separator: ",\n")
    }

    private var dictionary: [Key: String]
    var count: Int { dictionary.count }

    #if DEBUG
    subscript(key: Key) -> String? {
        get {

            dictionary[key]
        }
        // The setter is for testing purposes only.
        set {
            dictionary[key] = newValue
        }
    }
    #else
    subscript(key: Key) -> String? {
        dictionary[key]
    }
    #endif

    /// This allows consumers to access an instance via `myInstance[]`.
    /// Not sure how I feel about that, but we'll give it a shot for now...
    subscript() -> [String: String]  {
        Dictionary(uniqueKeysWithValues: dictionary.map { ($0.key.stringValue, $0.value) })
    }

    init(request: RequestProtocol, executableFileName: String, certificate: ClientCertificateMetadata?, hostname: String) {
        Log.debug("Instantiating EnvironmentVariables", from: .remote(hostname: hostname))
        let requestPathComponents = request.path.split(separator: "/")
        let executablePathComponents: any Sequence<String.SubSequence>
        if requestPathComponents.isEmpty {
            Log.debug("Request path components empty", from: .remote(hostname: hostname))
            executablePathComponents = []
        } else {
            Log.debug("Request path components not empty", from: .remote(hostname: hostname))
            let index = requestPathComponents.firstIndex(of: Substring(executableFileName)) ?? 0
            executablePathComponents = requestPathComponents[(index + 1)...]
        }

        let executablePath = "/" + executablePathComponents.joined(separator: "/")

        var dictionary: [Key: String] = [
            .network(.gatewayInterface): "CGI/1.1",
            .network(.url): request.url.absoluteString,
            .network(.pathInfo): executablePath,
            .network(.queryString): request.query ?? "",
            .network(.remoteHost): hostname,

            .script(.name): executableFileName == "/" ? executableFileName : ("/" + executableFileName),

            .server(.name): request.url.host ?? "",
            .server(.port): "1965",
            .server(.protocol): "GEMINI",
            .server(.software): "smolver/" + SmolverCommand().version.replacingOccurrences(of: "v", with: "")
        ]

        if let certificate = certificate {
            Log.debug("Authentication detected, expanding environment variable list", from: .remote(hostname: hostname))
            dictionary[.auth(.certificate(.fingerprint))] = "\(String(describing: certificate.encryptionAlgorithm).uppercased):\(certificate.fingerprint)"
            dictionary[.auth(.certificate(.notBefore))] = Formatters.time.string(from: certificate.notValidBeforeDate)
            dictionary[.auth(.certificate(.notAfter))] = Formatters.time.string(from: certificate.notValidAfterDate)
            dictionary[.auth(.certificate(.username))] = certificate.commonName
            dictionary[.auth(.type)] = "CERTIFICATE"
        }

        self.dictionary = dictionary
        Log.debug("Instantiated EnvironmentVariables", from: .remote(hostname: hostname))
    }
}
