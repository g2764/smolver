//
//  CGIExecutor.swift
//  smolver
//
//  Created by Justin Marshall on 2/21/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

struct CGIExecutor {
    let executable: Executable
    let hostname: String
    var logger: Logging.Type = Log.self

    enum Error: Swift.Error, LocalizedError {
        case undecodableOutput
        case failure

        var errorDescription: String? {
            switch self {
            case .undecodableOutput:
                return "CGI script output was undecodable. Make sure output is valid Gemini response string encoded as UTF-8."
            case .failure:
                return "A server-side script powering this page has failed."
            }
        }
    }

    func execute() -> Response {
        do {
            let output = try executable.execute()

            logger.debug("Attempting to parse script output: \(output)", from: .remote(hostname: hostname))
            guard let response = Response(rawData: output) else {
                logger.error("Script output not valid Gemini response", from: .remote(hostname: hostname))
                throw Error.undecodableOutput
            }

            logger.info("Script output parsed successfully", from: .remote(hostname: hostname))
            logger.debug("Parsed script output: \(response)", from: .remote(hostname: hostname))
            return response
        } catch {
            logger.error(error.localizedDescription, from: .remote(hostname: hostname))
            logger.error("Script execution failed", from: .remote(hostname: hostname))
            return .init(status: .failure(.temporary(.cgiError(userInfo: Error.failure.localizedDescription))))
        }
    }
}
