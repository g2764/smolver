//
//  CGIExecutable.swift
//  smolver
//
//  Created by Justin Marshall on 2/21/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol Executable {
    typealias Config = GlobalConfiguration.CGI

    var environmentVariables: EnvironmentVariables { get }

    func execute() throws -> Data
}

struct CGIExecutable: Executable {
    let environmentVariables: EnvironmentVariables
    let fullScriptPath: String
    private let hostname: String

    private enum Error: Swift.Error, LocalizedError {
        case stderr(message: String)

        var errorDescription: String? {
            switch self {
            case .stderr(let message):
                return message
            }
        }
    }

    init?(
        request: RequestProtocol,
        config: Config,
        certificate: ClientCertificateMetadata?,
        hostname: String,
        scriptBasePath: String = GlobalConfiguration.configDirectory)
    {
        Log.debug("Evaluating if request should be directed to script", from: .remote(hostname: hostname))
        Log.debug("Configured CGI whitelisted script files: \(config.whitelistedScriptFiles)", from: .remote(hostname: hostname))
        let whitelistedScriptFiles = config.whitelistedScriptFiles.map { whitelistedScriptFile in
            var path = whitelistedScriptFile.absoluteString
            while path.last == "/" {
                path = String(path.dropLast())
            }
            return path
        }

        Log.debug("Normalized CGI whitelisted script files: \(whitelistedScriptFiles)", from: .remote(hostname: hostname))

        // a request path looking like:
        // /hello/world/bob
        //
        // should have the following file paths checked as being whitelisted, stopping at the first match:
        // hello
        // hello/world
        // hello/world/bob

        let requestSubpaths = request.path.split(separator: "/")
        let combinedRequestSubpaths = requestSubpaths.enumerated().map { index, subpath in
            requestSubpaths[0...index].joined(separator: "/")
        }

        Log.debug("Determining request's CGI eligibility for path: \(scriptBasePath) and subpaths: \(combinedRequestSubpaths)", from: .remote(hostname: hostname))

        for whitelistedScriptFile in whitelistedScriptFiles {
            let match = combinedRequestSubpaths.first(where:{ subpath in
                                                                guard subpath == whitelistedScriptFile else {
                                                                    Log.debug("Subpath \"\(subpath)\" not matched", from: .remote(hostname: hostname))
                                                                    return false
                                                                }

                                                                Log.debug("Subpath \"\(subpath)\" matched", from: .remote(hostname: hostname))
                                                                return true
                                                          })

            if let match = match,
                let fileName = match.split(separator: "/").last {
                    let fileName = String(fileName)
                    Log.debug("Matched file \"\(fileName)\"", from: .remote(hostname: hostname))
                    self.environmentVariables = .init(request: request,
                                                      executableFileName: fileName,
                                                      certificate: certificate,
                                                      hostname: hostname)
                    Log.debug("Will use environment variables: \(self.environmentVariables)", from: .remote(hostname: hostname))
                    self.fullScriptPath = scriptBasePath + whitelistedScriptFile
                    Log.debug("Will attempt execution of script at: \(self.fullScriptPath)", from: .remote(hostname: hostname))
                    self.hostname = hostname
                    Log.debug("Request able to be directed to script", from: .remote(hostname: hostname))
                    return
            }
        }

        Log.debug("Request unable to be directed to script", from: .remote(hostname: hostname))
        return nil
    }


    func execute() throws -> Data {
        Log.debug("About to execute \(self.fullScriptPath)", from: .remote(hostname: hostname))
        let scriptURL = URL(fileURLWithPath: fullScriptPath, isDirectory: false)

        let process = Process()
        process.executableURL = scriptURL

        process.environment = environmentVariables[]

        let stdoutPipe = Pipe()
        process.standardOutput = stdoutPipe

        let stderrPipe = Pipe()
        process.standardError = stderrPipe

        Log.info("Executing \(self.fullScriptPath)", from: .remote(hostname: hostname))
        try process.run()
        process.waitUntilExit()

        let stderrData = stderrPipe.fileHandleForReading.readDataToEndOfFile()
        if let stderr = String(data: stderrData, encoding: .utf8),
            !stderr.isEmpty {
                Log.error("Script output error(s)", from: .remote(hostname: hostname))
                throw Error.stderr(message: stderr)
        }

        let stdoutData = stdoutPipe.fileHandleForReading.readDataToEndOfFile()
        Log.info("Script execution successful", from: .remote(hostname: hostname))

        return stdoutData
    }
}
