//
//  URL+Gemini.swift
//  smolver
//
//  Created by Justin Marshall on 1/19/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension URL {
    struct MimeType: Equatable {
        let type: String
        let extensions: [String]
        private static let systemFilePath = "/etc/mime.types"

        var isText: Bool {
            type.hasPrefix(Self.textPrefix)
        }

        init(type: String, extensions: [String]) {
            self.type = type
            self.extensions = extensions
        }

        init?(extension: String) {
            guard let match = Self.all.first(where: { $0.extensions.contains(`extension`) }) else {
                Log.debug("No known mime type \"\(`extension`)\"", from: .system)
                return nil
            }

            Log.debug("Mime type found for \"\(`extension`)\": \(match)", from: .system)
            self = match
        }

        init?(type: String) {
            guard let match = Self.all.first(where: { $0.type == type }) else {
                Log.debug("No known mime type \"\(type)\"", from: .system)
                return nil
            }

            Log.debug("Mime type found for \"\(type)\": \(match)", from: .system)
            self = match
        }

        private static let systemMimeTypes: [Self] = {
            guard let systemMimeTypesData = FileManager.default.contents(atPath: systemFilePath),
                let systemMimeTypesString = String(data: systemMimeTypesData, encoding: .utf8) else {
                    Log.warn("Unable to parse or read \"\(systemFilePath)\"", from: .system)
                    return []
            }

            Log.debug("\"\(systemFilePath)\" parsed successfully", from: .system)
            let systemMimeTypesRaw = systemMimeTypesString
                                        .split(separator: "\n")
                                        .filter { !$0.hasPrefix("#") }
                                        .map(String.init)
            Log.debug("Detected \(systemMimeTypesRaw.count) mime types known to OS", from: .system)

            let systemMimeTypesRawWithExtensions = systemMimeTypesRaw.compactMap { (rawMimeTypeLine: String) -> (String, [String])? in
                let lineParts = rawMimeTypeLine.split(separator: "\t")
                let threshholdIndicatingExtensionIsKnown = 2
                guard lineParts.count == threshholdIndicatingExtensionIsKnown else {
                    return nil
                }
                let type = String(lineParts[0])
                let extensions = lineParts[1].split(separator: " ").map(String.init)
                return (type, extensions)
            }

            Log.debug("Detected \(systemMimeTypesRawWithExtensions.count) mime types with file extensions known to OS", from: .system)
            let systemMimeTypes = systemMimeTypesRawWithExtensions.map { Self(type: $0, extensions: $1) }
            return systemMimeTypes
        }()

        private static let all = [textGemini] + systemMimeTypes
        private static let textPrefix = "text/"

        static let textGemini = Self(type: textPrefix + "gemini", extensions: ["gmi", "gemini"])
        static let applicationOctetStream = Self(type: "application/octet-stream", extensions: [])
    }

    var mimeType: MimeType {
        guard let mimeType = MimeType(extension: pathExtension) else {
            Log.warn("No known mime type for file with extension \"\(pathExtension)\", defaulting to \(MimeType.applicationOctetStream.type)", from: .system)
            return MimeType.applicationOctetStream
        }

        return mimeType
    }
}
