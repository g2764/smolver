//
//  ClientCertificateValidator.swift
//  smolver
//
//  Created by Justin Marshall on 9/19/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import SocketServer

protocol ClientCertificateMetadata {
    var fingerprint: String { get }
    var encryptionAlgorithm: X509Certificate.EncryptionAlgorithm { get }
    var commonName: String { get }
    var notValidBeforeDate: Date { get }
    var notValidAfterDate: Date { get }
}

protocol ClientCertificateProtocol: ClientCertificateMetadata {
    func write(to url: URL) throws
    static func create(from url: URL) -> Self?
}

extension X509Certificate: ClientCertificateProtocol {
    static func create(from url: URL) -> X509Certificate? { .init(from: url) }
}

struct ClientCertificateValidator {
    static let anonymousAuthenticationsDirectory = ".anonymous-authentications/"

    let request: Request
    let x509Certificate: ClientCertificateProtocol?
    let authentication: LocalConfiguration.Authentication?
    let clientHostname: String

    private let fileManager: FileManager
    private let logger: Logging.Type

    init(request: Request,
         x509Certificate: ClientCertificateProtocol?,
         authentication: LocalConfiguration.Authentication?,
         clientHostname: String,
         fileManager: FileManager = .default,
         logger: Logging.Type = Log.self)
    {
        self.request = request
        self.x509Certificate = x509Certificate
        self.authentication = authentication
        self.clientHostname = clientHostname
        self.fileManager = fileManager
        self.logger = logger
    }

    enum Error: Swift.Error {
        case clientCertificate(Response.Status.ClientCertificate)
    }

    func validate() throws {
        logger.debug("Commencing certificate validation",
                     from: .remote(hostname: clientHostname))

        defer {
            logger.debug("Completed certificate validation",
                         from: .remote(hostname: clientHostname))
        }

        guard let authentication = authentication else {
            return logger.debug("No authentication config",
                                from: .remote(hostname: clientHostname))
        }

        let requestedFileName = request.url.pathComponents.last
        let directoryOfRequestedFile = request.url.pathComponents.dropLast().dropFirst().joined(separator: "/")
        let directoryServingRequest = GlobalConfiguration.configDirectory + directoryOfRequestedFile

        if authentication.directory != nil && authentication.files != nil {
            logger.warn("\(directoryServingRequest)/\(LocalConfiguration.fileName) is configured both for authentication of individual files and entire directories. The server will now pick one arbitrarily. Do not rely on which is picked, it is subject to change with no notice. Please pick one or the other.",
                        from: .system)
        } else if authentication.directory == nil && authentication.files == nil {
            return logger.warn("Empty authentication config in \(directoryServingRequest)",
                               from: .remote(hostname: clientHostname))
        }

        if let files = authentication.files,
            let match = files.first(where: { $0.name == requestedFileName }) {
                logger.debug("Verifying certificate for \(match.name)",
                             from: .remote(hostname: clientHostname))
                try verify(x509Certificate, for: directoryServingRequest, with: match.whitelistedPath)
        } else if let directory = authentication.directory {
            guard let requestedFileName = requestedFileName,
                let excludedFiles = directory.excludedFiles,
                excludedFiles.contains(requestedFileName) else {
                    logger.debug("Verifying certificate for \(requestedFileName ?? "nil")",
                                 from: .remote(hostname: clientHostname))
                    return try verify(x509Certificate, for: directoryServingRequest, with: directory.whitelistedPath)
            }
        }
    }

    private func verify(_ x509Certificate: ClientCertificateProtocol?, for directoryServingRequest: String, with whitelistedPath: URL?) throws {
        guard let x509Certificate = x509Certificate else {
            logger.debug("No client certificate, disallowing access",
                         from: .remote(hostname: clientHostname))
            throw Error.clientCertificate(.required(reason: "A client certificate is required to view this resource."))
        }

        logger.debug("Client certificate attached",
                     from: .remote(hostname: clientHostname))
        let now = Date()

        if now < x509Certificate.notValidBeforeDate {
            logger.debug("Client certificate not yet valid, disallowing access",
                         from: .remote(hostname: clientHostname))
            throw Error.clientCertificate(.notValid(reason: "Certificate is not valid until future time."))
        } else if now > x509Certificate.notValidAfterDate {
            logger.debug("Client certificate expired, disallowing access",
                         from: .remote(hostname: clientHostname))
            throw Error.clientCertificate(.notValid(reason: "Certificate is expired."))
        } else if let whitelistedPath = whitelistedPath {
            logger.debug("Client certificate metadata accepted, checking whitelisted authorization",
                         from: .remote(hostname: clientHostname))
            try checkAuthorization(of: x509Certificate, using: whitelistedPath)
        } else {
            logger.debug("Client certificate metadata accepted, checking anonymous authorization",
                         from: .remote(hostname: clientHostname))
            authorizeAnonymously(x509Certificate, for: directoryServingRequest)
        }
    }

    private func checkAuthorization(of x509Certificate: ClientCertificateProtocol, using whitelistedPath: URL) throws {
        let contents = (try? fileManager
                                       .contentsOfDirectory(atPath: GlobalConfiguration.configDirectory + whitelistedPath.path)
                                       .map(URL.init(fileURLWithPath:)))
                                       ?? []

        let hasMatch = contents.contains {
            type(of: x509Certificate).create(from: $0)?.fingerprint == x509Certificate.fingerprint
        }

        guard hasMatch else {
            // A common reason this could fail is if a server admin puts a corrupted .pem file in place.
            //
            // It's necessary because, with whitelists, the intention is for the server admin to add the
            // certificate (as stated explicitly in the documentation), which obviously has the possibility
            // of human error. This check will detect that and not let the whitelisted user in, which
            // should prompt the admin to investigate.
            logger.debug("Client certificate not whitelisted for this resource, disallowing access",
                         from: .remote(hostname: clientHostname))
            throw Error.clientCertificate(.notAuthorized(reason: "This client certificate is not authorized for this resource."))
        }

        logger.debug("Client certificate whitelisted for this resource, allowing access",
                     from: .remote(hostname: clientHostname))
    }

    private func authorizeAnonymously(_ x509Certificate: ClientCertificateProtocol, for directoryServingRequest: String) {
        // This function is intentionally not checking for a corrupted .pem file (via the `create(from:)` API, see the
        // logic and comment in the whitelist checking function), because this anonymous access file is intended to be
        // created solely via this server software, not by manual admin action (as stated explicitly in the
        // documentation). Therefore, there is no possibility for human error and the only way this file should be able
        // to be corrupted is if either a bad actor has server access, in which case all bets are off anyway, or the
        // server admin has manually changed something, which is an admin error. It is simpler just not to code for that.

        let certPath = directoryServingRequest + "/\(Self.anonymousAuthenticationsDirectory)" + x509Certificate.fingerprint + ".pem"
        // example certPath: ~/.smolver/aoeu/.anonymous-authentications/99570B8B-4AA4-4A9B-970F-6C8E9AFAC1DD.pem

        guard !fileManager.fileExists(atPath: certPath) else {
            logger.debug("Anonymous client certificate already stored at \(certPath), allowing access",
                         from: .remote(hostname: clientHostname))
            return
        }

        logger.debug("Anonymous client certificate not found locally at \(certPath), checking parent directories",
                     from: .remote(hostname: clientHostname))

        // certs authorized for a parent directory are authorized for child directories as well, per the spec:
        //
        // Client certificates are supposed to have their scope bound to the same hostname
        // as the request URL and to all paths below the path of the request URL path
        let directoryServingRequestURL = URL(fileURLWithPath: directoryServingRequest, isDirectory: true)
        var certURLParent = directoryServingRequestURL.deletingLastPathComponent()
        certURLParent.appendPathComponent("\(Self.anonymousAuthenticationsDirectory)", isDirectory: true)
        certURLParent.appendPathComponent(x509Certificate.fingerprint + ".pem", isDirectory: false)
        // example certURLParent: ~/.smolver/.anonymous-authentications/99570B8B-4AA4-4A9B-970F-6C8E9AFAC1DD.pem

        // does the given cert exist in a parent directory already?
        var topMostCertURLParent = certURLParent
        var previousTopMostCertURLParent = topMostCertURLParent
        //
        // NOTE: Sometimes this causes a parent directory to be skipped. Not sure what's going on, but not
        //       high priority until that exact scenario happens with production content and configuration.
        //       If it does, it should be easy enough to add test(s) and/or assertion(s) into the existing tests.
        //
        while topMostCertURLParent.path.contains(GlobalConfiguration.smolverSubdirectory) && !fileManager.fileExists(atPath: topMostCertURLParent.path) {
            previousTopMostCertURLParent = topMostCertURLParent
            topMostCertURLParent.deleteLastPathComponent() // "fingerprint.pem"
            topMostCertURLParent.deleteLastPathComponent() // ".anonymous-authentications/"
            topMostCertURLParent.deleteLastPathComponent() // parent dir
            topMostCertURLParent.appendPathComponent("\(Self.anonymousAuthenticationsDirectory)", isDirectory: true)
            topMostCertURLParent.appendPathComponent(x509Certificate.fingerprint + ".pem", isDirectory: false)
            // example topMostCertURLParent: ~/.anonymous-authentications/99570B8B-4AA4-4A9B-970F-6C8E9AFAC1DD.pem

            logger.debug("Anonymous client certificate not found locally at \(topMostCertURLParent), checking parent directory",
                         from: .remote(hostname: clientHostname))
        }

        if !topMostCertURLParent.path.contains(GlobalConfiguration.smolverSubdirectory) {
            topMostCertURLParent = previousTopMostCertURLParent
        }
        guard !fileManager.fileExists(atPath: topMostCertURLParent.path) else {
            logger.debug("Anonymous client certificate already stored at \(topMostCertURLParent), allowing access",
                         from: .remote(hostname: clientHostname))
            return
        }

        logger.debug("Anonymous client certificate not found locally anywhere, storing locally and allowing access",
                     from: .remote(hostname: clientHostname))
        // given cert does not exist in the directory serving the request, nor in a parent
        let certURL = URL(fileURLWithPath: certPath, isDirectory: false)
        do {
            try x509Certificate.write(to: certURL)
        } catch {
            logger.error(error.localizedDescription, from: .remote(hostname: clientHostname))
        }
    }
}
