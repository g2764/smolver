//
//  Response+ClientCertificate.swift
//  smolver
//
//  Created by Justin Marshall on 5/10/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

extension Response.Status {
    enum ClientCertificate: StatusProtocol, Equatable {
        case required(reason: String?)
        case notAuthorized(reason: String?)
        case notValid(reason: String?)

        init?(status: Int, meta: String?) {
            switch status {
            case Self.required(reason: nil).code:
                self = .required(reason: meta)
            case Self.notAuthorized(reason: nil).code:
                self = .notAuthorized(reason: meta)
            case Self.notValid(reason: nil).code:
                self = .notValid(reason: meta)
            default:
                return nil
            }
        }

        var meta: Meta? {
            switch self {
            case .required(let reason),
                 .notAuthorized(let reason),
                 .notValid(let reason):
                return reason ?? ""
            }
        }

        var code: Int {
            switch self {
            case .required:
                return 60
            case .notAuthorized:
                return 61
            case .notValid:
                return 62
            }
        }
    }
}
