//
//  Response+Input.swift
//  smolver
//
//  Created by Justin Marshall on 5/10/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

extension Response.Status {
    enum Input: StatusProtocol, Equatable {
        case text(userPrompt: String)
        case sensitiveText(userPrompt: String)

        init?(status: Int, meta: String) {
            switch status {
            case Self.text(userPrompt: "").code:
                self = .text(userPrompt: meta)
            case Self.sensitiveText(userPrompt: "").code:
                self = .sensitiveText(userPrompt: meta)
            default:
                return nil
            }
        }

        var meta: Meta? {
            switch self {
            case .text(let userPrompt),
                 .sensitiveText(let userPrompt):
                 return userPrompt
            }
        }

        var code: Int {
            switch self {
            case .text:
                return 10
            case .sensitiveText:
                return 11
            }
        }
    }
}
