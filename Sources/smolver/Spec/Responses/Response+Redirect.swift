//
//  Response+Redirect.swift
//  smolver
//
//  Created by Justin Marshall on 5/10/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension Response.Status {
    enum Redirect: StatusProtocol, Equatable {
        case temporary(newURL: URL)
        case permanent(newURL: URL)

        init?(status: Int, url: URL) {
            switch status {
            case Self.temporary(newURL: URL(fileURLWithPath: "")).code:
                self = .temporary(newURL: url)
            case Self.permanent(newURL: URL(fileURLWithPath: "")).code:
                self = .permanent(newURL: url)
            default:
                return nil
            }
        }

        var meta: Meta? {
            switch self {
            case .temporary(let newURL),
                 .permanent(let newURL):
                 return newURL.absoluteString
            }
        }

        var code: Int {
            switch self {
            case .temporary:
                return 30
            case .permanent:
                return 31
            }
        }
    }
}
