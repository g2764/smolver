//
//  Response.swift
//  smolver
//
//  Created by Justin Marshall on 1/11/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol StatusProtocol {
    typealias Meta = String

    var meta: Meta? { get }
    var code: Int { get }
}

struct Response: Equatable {
    enum Status: StatusProtocol, Equatable {
        case input(Input)
        case success(Success)
        case redirect(Redirect)
        case failure(Failure)
        case clientCertificate(ClientCertificate)

        init?(status: Int, meta: String?, body: Data?) {
            if let body = body {
                if let success = Success(status: status, meta: meta, body: body) {
                    self = .success(success)
                } else {
                    return nil
                }
            } else if let failure = Failure(status: status, meta: meta) {
                self = .failure(failure)
            } else if let clientCertificate = ClientCertificate(status: status, meta: meta) {
                self = .clientCertificate(clientCertificate)
            } else if let meta = meta {
                if let input = Input(status: status, meta: meta) {
                    self = .input(input)
                } else if let url = URL(string: meta),
                    let redirect = Redirect(status: status, url: url) {
                        self = .redirect(redirect)
                } else {
                    return nil
                }
            } else {
                return nil
            }
        }

        var code: Int {
            switch self {
            case .input(let input):
                return input.code
            case .success(let success):
                return success.code
            case .redirect(let redirect):
                return redirect.code
            case .failure(let failure):
                return failure.code
            case .clientCertificate(let clientCertificate):
                return clientCertificate.code
            }
        }

        var meta: Meta? {
            switch self {
            case .input(let input):
                return input.meta
            case .success(let success):
                return success.meta
            case .redirect(let redirect):
                return redirect.meta
            case .failure(let failure):
                return failure.meta
            case .clientCertificate(let clientCertificate):
                return clientCertificate.meta
            }
        }

        fileprivate var header: String {
            // <STATUS><SPACE><META><CR><LF>
            "\(code) \(meta ?? "")\r\n"
        }

        fileprivate var body: Data? {
            switch self {
            case .success(let success):
                switch success {
                case .success(let mimeType):
                    return mimeType.data
                }
            case .input,
                 .redirect,
                 .failure,
                 .clientCertificate:
                 return nil
            }
        }
    }

    let status: Status

    var header: String { status.header }
    var body: Data? { status.body }

    static let maximumMetaByteSize: UInt = 1024 // per the Gemini spec

    init(status: Status) {
        self.status = status
    }

    init?(rawData: Data) {
        guard let separatorData = "\r\n".data(using: .utf8),
            let range = rawData.range(of: separatorData) else {
                return nil
        }

        let headerData = rawData[..<range.endIndex]
        guard let header = String(data: headerData, encoding: .utf8) else {
            return nil
        }

        let body: Data?
        if range.endIndex < rawData.count {
            body = rawData[range.endIndex...]
        } else {
            body = nil
        }

        let cleanedHeader = header.replacingOccurrences(of: "\r\n", with: "")
        guard let header = Self.separateHeader(cleanedHeader),
            let parsedStatus = Status(status: header.status, meta: header.meta, body: body) else {
                return nil
        }

        self.init(status: parsedStatus)
    }

    private static func separateHeader(_ header: String) -> (status: Int, meta: String?)? {
        // <STATUS><SPACE><META><CR><LF>

        // this is nasty, but that's Swift's String range APIs for ya...
        let statusEndOffset = 1
        let endIndexOfStatus = header.index(header.startIndex, offsetBy: statusEndOffset)
        let statusRange = header.startIndex...endIndexOfStatus
        guard statusRange.upperBound < header.endIndex else {
            return nil
        }

        guard let status = Int(header[statusRange]) else {
            return nil
        }

        guard header[header.index(after: statusRange.upperBound)...].first == " " else {
            return nil
        }

        let metaEndOffset = 2
        let possibleMeta = String(header[header.index(endIndexOfStatus, offsetBy: metaEndOffset)...])
        //

        let meta = possibleMeta.isEmpty ? nil : possibleMeta
        guard let metaSize = possibleMeta.data(using: .utf8)?.count,
            metaSize <= Self.maximumMetaByteSize else {
                return nil
        }

        return (status, meta)
    }
}
