//
//  Response+MimeType.swift
//  smolver
//
//  Created by Justin Marshall on 5/10/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension Response.Status {
    enum MimeType: CustomStringConvertible, CustomDebugStringConvertible, Equatable {
        enum Text: Equatable {
            // text/gemini, text/* (non-gemini) have specific extra data required
            // everything else does not

            case gemini(encoding: String.Encoding = .utf8, language: String?)
            case nonGemini(URL.MimeType, encoding: String.Encoding = .utf8)

            fileprivate var mimeType: URL.MimeType {
                switch self {
                case .gemini:
                    return .textGemini
                case .nonGemini(let mimeType, _):
                     return mimeType
                }
            }
        }

        case text(Text, Data)
        case other(URL.MimeType, Data)

        var data: Data {
            switch self {
            case .text(_, let data),
                 .other(_, let data):
                 return data
            }
        }

        var debugDescription: String {
            switch self {
            case .text(let text, let data):
                switch text {
                case .gemini(let encoding, let language):
                    return "\(text.mimeType); encoding: \(encoding.description); language: \(language ?? "nil"); body: \"\(String(data: data, encoding: encoding) ?? "UNDECODABLE DATA"))\""
                case .nonGemini(let mimeType, let encoding):
                    return "\(mimeType); encoding: \(encoding.description); language: nil; body: \"\(String(data: data, encoding: encoding) ?? "UNDECODABLE DATA"))\""
                }
            case .other:
                return description
            }
        }

        var description: String {
            switch self {
            case .text(let text, _):
                switch text {
                case .gemini(let encoding, let language):
                    guard encoding == .utf8 else {
                        return text.mimeType.type
                    }

                    // String.Encoding.utf8.description returns a human-readable string
                    // i.e., "Unicode (UTF-8)", so just handle this manually.
                    let charset = "; charset=utf-8"
                    var theLanguage: String?
                    if let language {
                        theLanguage = "; lang=\(language)"
                    }

                    let meta = text.mimeType.type + charset + (theLanguage ?? "")
                    return meta
                case .nonGemini:
                    return text.mimeType.type + "; charset=utf-8"
                }
            case .other(let mimeType, _):
                return mimeType.type
            }
        }
    }
}
