//
//  Response+Failure.swift
//  smolver
//
//  Created by Justin Marshall on 5/10/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

extension Response.Status {
    enum Failure: StatusProtocol, Equatable {
        case temporary(Temporary)
        case permanent(Permanent)

        init?(status: Int, meta: String?) {
            if let temporary = Temporary(status: status, meta: meta) {
                self = .temporary(temporary)
            } else if let permanent = Permanent(status: status, meta: meta) {
                self = .permanent(permanent)
            } else {
                return nil
            }
        }

        enum Temporary: StatusProtocol, Equatable {
            case temporary(userInfo: String?)
            case serverUnavailable(userInfo: String?)
            case cgiError(userInfo: String?)
            case proxyError(userInfo: String?)
            case slowDown(userInfo: String)

            init?(status: Int, meta: String?) {
                switch status {
                case Self.temporary(userInfo: nil).code:
                    self = .temporary(userInfo: meta)
                case Self.serverUnavailable(userInfo: nil).code:
                    self = .serverUnavailable(userInfo: meta)
                case Self.cgiError(userInfo: nil).code:
                    self = .cgiError(userInfo: meta)
                case Self.proxyError(userInfo: nil).code:
                    self = .proxyError(userInfo: meta)
                case Self.slowDown(userInfo: "").code:
                    guard let meta = meta else {
                        return nil
                    }

                    self = .slowDown(userInfo: meta)
                default:
                    return nil
                }
            }

            var meta: Meta? {
                switch self {
                case .temporary(let userInfo),
                     .serverUnavailable(let userInfo),
                     .cgiError(let userInfo),
                     .proxyError(let userInfo):
                     return userInfo ?? ""
                case .slowDown(let userInfo):
                    return userInfo
                }
            }

            var code: Int {
                switch self {
                case .temporary:
                    return 40
                case .serverUnavailable:
                    return 41
                case .cgiError:
                    return 42
                case .proxyError:
                    return 43
                case .slowDown:
                    return 44
                }
            }
        }

        enum Permanent: StatusProtocol, Equatable {
            case permanent(userInfo: String?)
            case notFound(userInfo: String?)
            case gone(userInfo: String?)
            case proxyRequestRefused(userInfo: String?)
            case badRequest(userInfo: String?)

            init?(status: Int, meta: String?) {
                switch status {
                case Self.permanent(userInfo: nil).code:
                    self = .permanent(userInfo: meta)
                case Self.notFound(userInfo: nil).code:
                    self = .notFound(userInfo: meta)
                case Self.gone(userInfo: nil).code:
                    self = .gone(userInfo: meta)
                case Self.proxyRequestRefused(userInfo: nil).code:
                    self = .proxyRequestRefused(userInfo: meta)
                case Self.badRequest(userInfo: nil).code:
                    self = .badRequest(userInfo: meta)
                default:
                    return nil
                }
            }

            var meta: Meta? {
                switch self {
                case .permanent(let userInfo),
                     .notFound(let userInfo),
                     .gone(let userInfo),
                     .proxyRequestRefused(let userInfo),
                     .badRequest(let userInfo):
                     return userInfo ?? ""
                }
            }

            var code: Int {
                switch self {
                case .permanent:
                    return 50
                case .notFound:
                    return 51
                case .gone:
                    return 52
                case .proxyRequestRefused:
                    return 53
                case .badRequest:
                    return 59
                }
            }
        }

        var meta: Meta? {
            switch self {
            case .temporary(let temporary):
                return temporary.meta
            case .permanent(let permanent):
                return permanent.meta
            }
        }

        var code: Int {
            switch self {
            case .temporary(let temporary):
                return temporary.code
            case .permanent(let permanent):
                return permanent.code
            }
        }
    }
}
