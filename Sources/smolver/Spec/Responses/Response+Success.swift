//
//  Response+Success.swift
//  smolver
//
//  Created by Justin Marshall on 5/10/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension Response.Status {
    enum Success: StatusProtocol, Equatable {
        case success(mimeType: MimeType)

        private static func parse(_ optionalMetaParts: [String.SubSequence], for mimeType: URL.MimeType) -> (charset: String.Encoding, language: String?)? {
            var language: String?
            let charsetIndex = 0
            let charsetParameter = "charset=utf-8"
            let langParameterKey = "lang="

            switch optionalMetaParts.count {
            case 0:
                break
            case 1:
                let charsetOrLanguage = optionalMetaParts[charsetIndex]

                if charsetOrLanguage == charsetParameter && mimeType.isText {
                    break
                } else if charsetOrLanguage.hasPrefix(langParameterKey) && mimeType == .textGemini {
                    language = charsetOrLanguage.replacingOccurrences(of: langParameterKey, with: "")
                } else {
                    return nil
                }
            case 2:
                let charset = optionalMetaParts[charsetIndex]

                guard charset == charsetParameter else {
                    return nil
                }

                let languageIndex = 1
                let languageParameter = optionalMetaParts[languageIndex]
                guard languageParameter.hasPrefix(langParameterKey),
                    mimeType == .textGemini else {
                        return nil
                }

                language = languageParameter.replacingOccurrences(of: langParameterKey, with: "")
            default:
                return nil
            }

            // The spec says `charset` is optional but defaults to UTF-8 and, in the interest
            // of simplicity, that is all smolver supports.
            return (.utf8, language)
        }

        private static func parse(_ meta: String?) -> (mimeType: URL.MimeType, charset: String.Encoding, language: String?)? {
            // From spec: If <META> is an empty string, the MIME type MUST default to "text/gemini; charset=utf-8".
            guard let meta = meta,
                meta != " " else {
                    return (.textGemini, .utf8, nil)
                }

            let metaParts = meta.split(separator: "; ")
            let mimeIndex = 0
            let mimeTypeString = String(metaParts[mimeIndex])

            let mimeType: URL.MimeType = .init(type: mimeTypeString) ??
                                         .init(type: mimeTypeString, extensions: [])

            // When not manually wrapping this back into an Array(...),
            // the indices of `optionalMetaParts` are still relative to
            // `metaParts.count`. Who knew? I'd rather reference elements
            // relative to `metaParts.count - 1`. Hence the `Array(...)` wrapper.
            let optionalMetaParts = Array(metaParts.dropFirst())

            guard let parsedOptionalMetaParts = parse(optionalMetaParts, for: mimeType) else {
                return nil
            }

            return (mimeType, parsedOptionalMetaParts.charset, parsedOptionalMetaParts.language)
        }

        init?(status: Int, meta: String?, body: Data) {
            guard status == Self.success(mimeType: .text(.gemini(language: nil), Data())).code else {
                return nil
            }

            guard let parsedMeta = Self.parse(meta) else {
                return nil
            }

            let mimeType = parsedMeta.mimeType
            let charset = parsedMeta.charset

            switch (mimeType.isText, mimeType == .textGemini) {
            case (true, true):
                let language = parsedMeta.language
                self = .success(mimeType: .text(.gemini(encoding: charset, language: language), body))
            case (true, false):
                self = .success(mimeType: .text(.nonGemini(mimeType, encoding: charset), body))
            case (false, _):
                self = .success(mimeType: .other(mimeType, body))
            }
        }

        var meta: Meta? {
            switch self {
            case .success(let mimeType):
                return mimeType.description
            }
        }

        var code: Int {
            switch self {
            case .success:
                return 20
            }
        }
    }
}
