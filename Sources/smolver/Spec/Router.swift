//
//  Router.swift
//  smolver
//
//  Created by Justin Marshall on 1/15/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import SocketServer

class Router {
    let configuration: GlobalConfiguration
    let rawRequest: String?
    let certificate: X509Certificate?
    let hostname: String

    init(configuration: GlobalConfiguration,
         rawRequest: String?,
         certificate: X509Certificate?,
         hostname: String)
    {
        self.configuration = configuration
        self.rawRequest = rawRequest
        self.certificate = certificate
        self.hostname = hostname
    }

    /// A cached copy of the local configuration for a given file name. This is not cached across
    /// requests, but only internally to this instance (which should be initialized anew with each
    /// request) due to it being checked multiple times when `route` is called.
    private var cachedLocalConfiguration: (config: LocalConfiguration, requestedFileName: String)?

    //
    // !!!IMPORTANT!!!
    //
    // Some of the newer code in this file, starting with CGI integration, does not log
    // from this file because I'd rather start offloading work out of here and into
    // subcomponents. Continue this pattern when changing this file.
    //

    func route() -> Response {
        Log.info("request: \(rawRequest ?? "")", from: .remote(hostname: hostname))
        guard let rawRequest = rawRequest else {
            return Response(status: .failure(.permanent(.badRequest(userInfo: nil))))
        }

        do {
            let request = try Request(rawRequest,
                                      from: hostname,
                                      expectations: (configuration.host, configuration.ip))
            let response = determineResponse(for: request)
            return response
        } catch let error as Request.Error {
            switch error {
            case .reason(let reason):
                return Response(status: .failure(.permanent(reason)))
            }
        } catch {
            let message = "An unexpected error has occurred."
            Log.warn(message, from: .remote(hostname: hostname))
            return Response(status: .failure(.permanent(.permanent(userInfo: message))))
        }
    }

    private func determineResponse(for request: Request) -> Response {
        Log.debug("Looking for \(request.path)", from: .remote(hostname: hostname))
        guard request.path != "/" else {
            Log.info("Requested capsule root, checking for index.[gmi|gemini] to serve instead.", from: .remote(hostname: hostname))
            return request.rerouteToIndex(reusing: self)
        }

        guard configuration.allows(request) else {
            Log.warn("Client attempting to access unconfigured subdirectory of \(GlobalConfiguration.configDirectory)", from: .remote(hostname: hostname))
            return Response(status: .failure(.permanent(.notFound(userInfo: nil))))
        }

        var isDirectory: ObjCBool = false
        guard FileManager.default.fileExists(atPath: GlobalConfiguration.configDirectory + request.path, isDirectory: &isDirectory) else {
            guard let configuredRedirect = configuredRedirect(for: request, from: hostname) else {
                guard let configuredDeletion = configuredDeletion(for: request, from: hostname) else {
                    Log.debug("File not found", from: .remote(hostname: hostname))
                    return Response(status: .failure(.permanent(.notFound(userInfo: nil))))
                }

                Log.debug("Deletion configuration correct. Returning gone status code instead of not found", from: .remote(hostname: hostname))
                return configuredDeletion
            }

            Log.debug("Redirection configuration correct. Redirecting to new location", from: .remote(hostname: hostname))
            return configuredRedirect
        }

        guard !isDirectory.boolValue else {
            Log.info("Requested existent directory, checking for index.[gmi|gemini] to serve instead.", from: .remote(hostname: hostname))
            return request.rerouteToIndex(reusing: self)
        }

        if let errorResponse = createClientCertificateErrorResponse(for: request, from: hostname) {
            Log.info("Client certificate invalid",
                     from: .remote(hostname: hostname))
            return errorResponse
        } else if Blacklist.shouldBlock(request, from: hostname) {
            return Response(status: .failure(.permanent(.notFound(userInfo: nil))))
        } else {
            Log.debug("Client certificate either accepted or not necessary",
                      from: .remote(hostname: hostname))
            if let cgiResponse = makeCGIResponse(for: request, from: hostname) {
                return cgiResponse
            } else {
                return createSuccessResponse(for: request, from: hostname)
            }
        }
    }

    private func createClientCertificateErrorResponse(for request: Request, from hostname: String) -> Response? {
        do {
            let localConfiguration = localConfiguration(matching: request, from: hostname)
            let authentication = localConfiguration?.config.authentication

            let clientCertificateValidator = ClientCertificateValidator(request: request,
                                                                        x509Certificate: certificate,
                                                                        authentication: authentication,
                                                                        clientHostname: hostname)
            try clientCertificateValidator.validate()
            return nil
        } catch let error as ClientCertificateValidator.Error {
            switch error {
            case .clientCertificate(let certificateError):
                return Response(status: .clientCertificate(certificateError))
            }
        } catch {
            Log.error("Unexpected error while validating client certificate: \(error.localizedDescription)", from: .remote(hostname: hostname))
            return Response(status: .failure(.temporary(.temporary(userInfo: "Unexpected error while validating client certificate."))))
        }
    }

    private func createSuccessResponse(for request: Request, from hostname: String) -> Response {
        guard let fileData = FileManager.default.contents(atPath: GlobalConfiguration.configDirectory + request.path) else {
            Log.debug("File not found", from: .remote(hostname: hostname))
            return Response(status: .failure(.permanent(.notFound(userInfo: nil))))
        }

        let requestedMimeType = mimeType(for: request, from: hostname) ?? request.url.mimeType
        let isFileUTF8 = String(data: fileData, encoding: .utf8) != nil

        switch (requestedMimeType.isText, requestedMimeType == .textGemini) {
        case (true, true) where isFileUTF8:
            let language = language(for: request, from: hostname)
            return Response(status: .success(.success(mimeType: .text(.gemini(encoding: .utf8, language: language), fileData))))
        case (true, false) where isFileUTF8:
            return Response(status: .success(.success(mimeType: .text(.nonGemini(requestedMimeType, encoding: .utf8), fileData))))
        case (true, true),
             (true, false):
             return Response(status: .success(.success(mimeType: .other(.applicationOctetStream, fileData))))
        case (false, _):
            return Response(status: .success(.success(mimeType: .other(requestedMimeType, fileData))))
        }
    }

    private func makeCGIResponse(for request: Request, from hostname: String) -> Response? {
        guard let cgiConfig = configuration.cgi,
            let executable = CGIExecutable(request: request, config: cgiConfig, certificate: certificate, hostname: hostname) else {
                return nil
        }

        let executor = CGIExecutor(executable: executable, hostname: hostname)
        let response = executor.execute()
        return response
    }

    private func localConfiguration(matching request: Request, from hostname: String) -> (config: LocalConfiguration, requestedFileName: String)? {
        if let cachedLocalConfiguration {
            Log.debug("Reusing cached localConfiguration", from: .remote(hostname: hostname))
            return cachedLocalConfiguration
        }

        guard let fileName = request.url.pathComponents.last else {
            Log.debug("No file name detected in request", from: .remote(hostname: hostname))
            return nil
        }

        let localLocationForRequest = GlobalConfiguration.configDirectory + request.path
        let localConfigPath = localLocationForRequest.replacingOccurrences(of: fileName, with: "") + LocalConfiguration.fileName
        Log.debug("Checking for local configuration at " + localConfigPath, from: .remote(hostname: hostname))

        guard FileManager.default.fileExists(atPath: localConfigPath),
            let localConfigData = FileManager.default.contents(atPath: localConfigPath) else {
                Log.debug("No local directory configuration (\"\(LocalConfiguration.fileName)\") detected", from: .remote(hostname: hostname))
                return nil
        }

        do {
            let localConfig = try JSONDecoder().decode(LocalConfiguration.self, from: localConfigData)
            cachedLocalConfiguration = (localConfig, fileName)
            return (localConfig, fileName)
        } catch {
            Log.config("Invalid local config file at \(localConfigPath). Reason: \(error.localizedDescription)")
            return nil
        }
    }

    private func mimeType(for request: Request, from hostname: String) -> URL.MimeType? {
        guard let localConfig = localConfiguration(matching: request, from: hostname) else {
            // intentionally omitting log because the `localConfiguration` function handles that
            return nil
        }

        let config = localConfig.config
        let fileName = localConfig.requestedFileName

        guard let mimeTypes = config.mimeTypes else {
            Log.debug("No mime type override configuration detected", from: .remote(hostname: hostname))
            return nil
        }

        guard let matchingOverridableFile = mimeTypes.files?.first(where: { $0.fileName == fileName }) else {
            Log.debug("No mime type override for \"\(fileName)\"", from: .remote(hostname: hostname))

            guard let matchingOverridableDirectory = mimeTypes.directory else {
                Log.debug("No mime type override for this directory", from: .remote(hostname: hostname))
                return nil
            }

            let rawMimeType = matchingOverridableDirectory.mimeType

            // Per smolver's documentation:
            //
            // `>smolver` will make no attempt to validate your mime type string(s);
            // >it'll be returned verbatim in the response header.
            //
            // Hence the nil coalesce.
            let mimeType = URL.MimeType(type: rawMimeType) ?? URL.MimeType(type: rawMimeType, extensions: [])

            Log.debug("Overriding mime type for directory to \"\(mimeType)\"", from: .remote(hostname: hostname))
            return mimeType
        }

        let rawMimeType = matchingOverridableFile.mimeType

        // Per smolver's documentation:
        //
        // `>smolver` will make no attempt to validate your mime type string(s);
        // >it'll be returned verbatim in the response header.
        //
        // Hence the nil coalesce.
        let mimeType = URL.MimeType(type: rawMimeType) ?? URL.MimeType(type: rawMimeType, extensions: [])

        Log.debug("Overriding mime type for \"\(fileName)\" to \"\(mimeType)\"", from: .remote(hostname: hostname))
        return mimeType
    }

    private func language(for request: Request, from hostname: String) -> String {
        guard let localConfig = localConfiguration(matching: request, from: hostname) else {
            // intentionally omitting log because the `localConfiguration` function handles that
            return configuration.language
        }

        let config = localConfig.config
        let fileName = localConfig.requestedFileName

        guard let languages = config.languages else {
            Log.debug("No language override configuration detected", from: .remote(hostname: hostname))
            return configuration.language
        }

        guard let matchingOverridableFile = languages.files?.first(where: { $0.fileName == fileName }) else {
            Log.debug("No language override for \"\(fileName)\"", from: .remote(hostname: hostname))

            guard let matchingOverridableDirectory = languages.directory else {
                Log.debug("No language override for this directory", from: .remote(hostname: hostname))
                return configuration.language
            }

            let language = matchingOverridableDirectory.language
            Log.debug("Overriding language for directory to \"\(language)\"", from: .remote(hostname: hostname))
            return language
        }

        let language = matchingOverridableFile.language
        Log.debug("Overriding language for \"\(fileName)\" to \"\(language)\"", from: .remote(hostname: hostname))
        return language
    }

    private func configuredRedirect(for request: Request, from hostname: String) -> Response? {
        guard let localConfig = localConfiguration(matching: request, from: hostname) else {
            // intentionally omitting log because the `localConfiguration` function handles that
            return nil
        }

        let config = localConfig.config
        let fileName = localConfig.requestedFileName

        guard let redirects = config.redirects else {
            Log.debug("No redirection configuration detected", from: .remote(hostname: hostname))
            return nil
        }

        guard let matchingRedirectableFile = redirects.files.first(where: { $0.oldName == fileName }) else {
            Log.debug("No matches for the configured redirects for this directory", from: .remote(hostname: hostname))
            return nil
        }

        guard let host = request.url.host,
            let partialNewURL = URL(string: "gemini://" + host + "/") else {
                Log.error("Unable to create new URL for configured redirect. This should not be possible! Please notify smolver developer.", from: .remote(hostname: hostname))
                return nil
        }

        let fullNewURL = partialNewURL.appendingPathComponent(matchingRedirectableFile.newPath.absoluteString).appendingPathComponent(matchingRedirectableFile.newName)

        let redirectType: Response.Status.Redirect
        if matchingRedirectableFile.isTemporary {
            Log.debug("Temporary redirect configured", from: .remote(hostname: hostname))
            redirectType = .temporary(newURL: fullNewURL)
        } else {
            Log.debug("Permanent redirect configured", from: .remote(hostname: hostname))
            redirectType = .permanent(newURL: fullNewURL)
        }

        return Response(status: .redirect(redirectType))
    }

    private func configuredDeletion(for request: Request, from hostname: String) -> Response? {
        guard let localConfig = localConfiguration(matching: request, from: hostname) else {
            // intentionally omitting log because the `localConfiguration` function handles that
            return nil
        }

        let config = localConfig.config
        let fileName = localConfig.requestedFileName

        guard let deletedFiles = config.deletions?.files else {
            Log.debug("No deletion configuration detected", from: .remote(hostname: hostname))
            return nil
        }

        guard deletedFiles.contains(where: { $0.fileName == fileName }) else {
            Log.debug("No matches for the configured deletions for this directory", from: .remote(hostname: hostname))
            return nil
        }

        return Response(status: .failure(.permanent(.gone(userInfo: nil))))
    }
}

private extension Request {
    func rerouteToIndex(reusing router: Router) -> Response {
        if forceRedirectIfIndexShortcut {
            Log.debug("Requested existent directory without client-side trailing slash, redirecting to add slash", from: .remote(hostname: router.hostname))
            return Response(status: .redirect(.permanent(newURL: url.appendingPathComponent("/"))))
        }

        let newRouter = Router(configuration: router.configuration,
                               rawRequest: url.appendingPathComponent("index.gmi").absoluteString + "\r\n",
                               certificate: router.certificate,
                               hostname: router.hostname)
        let indexGmiResponse = newRouter.route()

        switch indexGmiResponse.status {
        case .input,
             .success,
             .redirect,
             .clientCertificate:
             return indexGmiResponse
        case .failure:
             let newRouter = Router(configuration: router.configuration,
                                    rawRequest: url.appendingPathComponent("index.gemini").absoluteString + "\r\n",
                                    certificate: router.certificate,
                                    hostname: router.hostname)
             let indexGeminiResponse = newRouter.route()
             return indexGeminiResponse
        }
    }
}

private extension GlobalConfiguration {
    func allows(_ request: Request) -> Bool {
        let thresholdIndicatingRequestIsWithinSubdir = 2
        let subdirectoryIndex = 1
        let pathComponents = request.url.pathComponents

        if request.path == "/robots.txt" {
            return shouldServeRobotsFile
        } else if pathComponents.count >= thresholdIndicatingRequestIsWithinSubdir,
            pathComponents.first == "/",
            // the only exception to only being able to access files outside of pre-configured directories is the top-level index
            request.path != "/index.gmi" && request.path != "/index.gemini",
            !staticContent.allowedFileDownloadPaths.map({ $0.absoluteString.replacingOccurrences(of: "/", with: "") }).contains(pathComponents[subdirectoryIndex].replacingOccurrences(of: "/", with: "")) {
                return false
        } else {
            return true
        }
    }
}
