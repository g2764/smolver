//
//  RateLimiter.swift
//  smolver
//
//  Created by Justin Marshall on 1/29/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

final class RateLimiter {
    struct Host {
        let hostname: String
        let dateOfLastRequest: Date
        let isPreviousRequestRedirect: Bool
    }

    let rateLimitingLockQueue: DispatchQueue
    let requestInterval: UInt

    private var _hosts: [Host] = []
    private(set) var hosts: [Host] {
        // I suspect that this won't scale, as it'll block the current thread,
        // but with the small size of Gemini to date, it shouldn't be a problem.
        set {
            rateLimitingLockQueue.sync { _hosts = newValue }
        }
        get {
            rateLimitingLockQueue.sync { _hosts }
        }
    }

    init(rateLimitingLockQueue: DispatchQueue, requestInterval: UInt) {
        self.rateLimitingLockQueue = rateLimitingLockQueue
        self.requestInterval = requestInterval
    }

    func shouldLimit(hostname: String) -> Bool {
        if let matchIndex = hosts.firstIndex(where: { $0.hostname == hostname }) {
            let match = hosts[matchIndex]
            guard !match.isPreviousRequestRedirect else {
                hosts[matchIndex] = .init(hostname: hostname,
                                          dateOfLastRequest: match.dateOfLastRequest,
                                          isPreviousRequestRedirect: false)
                return false
            }

            let nextAllowedTimestamp = match.dateOfLastRequest.addingTimeInterval(TimeInterval(requestInterval))
            let now = Date()
            hosts[matchIndex] = .init(hostname: hostname, dateOfLastRequest: now, isPreviousRequestRedirect: false)

            if now >= nextAllowedTimestamp {
                return false
            } else {
                return true
            }
        } else {
            hosts.append(.init(hostname: hostname, dateOfLastRequest: Date(), isPreviousRequestRedirect: false))
            return false
        }
    }

    func responded(to hostname: String, with status: Response.Status) {
        guard let matchIndex = hosts.firstIndex(where: { $0.hostname == hostname }) else {
            return
        }

        let match = hosts[matchIndex]
        if case .redirect = status {
            hosts[matchIndex] = .init(hostname: hostname,
                                      dateOfLastRequest: match.dateOfLastRequest,
                                      isPreviousRequestRedirect: true)
        }
    }
}
