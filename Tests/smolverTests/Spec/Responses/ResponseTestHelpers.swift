//
//  ResponseTestHelpers.swift
//  smolver
//
//  Created by Justin Marshall on 4/26/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import XCTest

class ResponseTestHelpers: XCTestCase {
    // MARK: - Helper functions

    func createResponse(for status: String, ofSize size: UInt) -> Response? {
        let meta = String(bytes: (0..<size).map { _ in 0 }, encoding: .utf8)!
        let header = status + " " + meta + "\r\n"
        let response = Response(rawResponse: header)
        return response
    }
}
