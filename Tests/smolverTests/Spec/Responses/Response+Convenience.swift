//
//  Response+Convenience.swift
//  smolver
//
//  Created by Justin Marshall on 06/28/24.
//  Copyright © 2024 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.

@testable import smolver

extension Response {
    //
    // A more complicated verion of this used to exist in smolver itself.
    // Once CGI support was refactored to accept any mime type (vs. its
    // original "text/*" only support), this init was able to be reduced
    // down to simply this.
    //
    // As unnecessary as a one-line wrapper seems at first, this init is
    // used so many times in this file that refactoring those call sites
    // to use the `init(rawData:)` function would be more trouble than
    // it's worth.
    //
    init?(rawResponse: String) {
        self.init(rawData: rawResponse.data(using: .utf8)!)
    }
}
