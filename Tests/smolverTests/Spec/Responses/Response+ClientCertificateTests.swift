//
//  Response+ClientCertificateTests.swift
//  smolver
//
//  Created by Justin Marshall on 06/27/24.
//  Copyright © 2024 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.

@testable import smolver
import XCTest

class ResponseClientCertificateTests: ResponseTestHelpers {
    /*

    <STATUS><SPACE><META><CR><LF>

    ### 60 (CLIENT CERTIFICATE REQUIRED)
    #### META may provide info for user, optional
    ### 61 (CERTIFICATE NOT AUTHORIZED)
    #### META may provide info for user, optional
    ### 62 (CERTIFICATE NOT VALID)
    #### META may provide info for user, optional

    */

    // MARK: - Happy path

    func testCertificateRequiredWithMetaNoBody() {
        let meta = "cert needed for this resource"
        let header = "60 " + meta + "\r\n"
        let expectedResponse = Response(status: .clientCertificate(.required(reason: meta)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testCertificateRequiredNoMetaNoBody() {
        let meta = nil as String?
        let header = "60 " + "\r\n"
        let expectedResponse = Response(status: .clientCertificate(.required(reason: meta)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testCertificateNotAuthorizedWithMetaNoBody() {
        let meta = "you're not authorized"
        let header = "61 " + meta + "\r\n"
        let expectedResponse = Response(status: .clientCertificate(.notAuthorized(reason: meta)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testCertificateNotAuthorizedNoMetaNoBody() {
        let meta = nil as String?
        let header = "61 " + "\r\n"
        let expectedResponse = Response(status: .clientCertificate(.notAuthorized(reason: meta)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testCertificateNotValidWithMetaNoBody() {
        let meta = "cert not valid"
        let header = "62 " + meta + "\r\n"
        let expectedResponse = Response(status: .clientCertificate(.notValid(reason: meta)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testCertificateNotValidNoMetaNoBody() {
        let meta = nil as String?
        let header = "62 " + "\r\n"
        let expectedResponse = Response(status: .clientCertificate(.notValid(reason: meta)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    // MARK: - Happy path - meta at max length

    // These simply assert not nil because they are bounds testing the max meta byte size.
    // All the other business rules are accounted for in other tests.

    func testCertificateRequiredMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "60", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testCertificateNotAuthorizedMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "61", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testCertificateNotValidMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "62", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    // MARK: - Sad path - meta too big

    func testCertificateRequiredMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "60", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testCertificateNotAuthorizedMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "61", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testCertificateNotValidMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "62", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    // MARK: - Sad path - malformed response

    func testCertificateRequiredHeaderNoCarriageReturnNoLineFeed() {
        let header = "60 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testCertificateNotAuthorizedHeaderNoCarriageReturnNoLineFeed() {
        let header = "61 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testCertificateNotValidHeaderNoCarriageReturnNoLineFeed() {
        let header = "62 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    // MARK: - Sad path - unexpected body

    func testCertificateRequiredWithMetaWithBody() {
        let header = "60 you need cert\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testCertificateRequiredNoMetaWithBody() {
        let header = "60 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testCertificateNotAuthorizedWithMetaWithBody() {
        let header = "61 forbidden\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testCertificateNotAuthorizedNoMetaWithBody() {
        let header = "61 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testCertificateNotValidWithMetaWithBody() {
        let header = "62 expiry\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testCertificateNotValidNoMetaWithBody() {
        let header = "62 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }
}
