//
//  ResponseTests.swift
//  smolver
//
//  Created by Justin Marshall on 4/26/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import XCTest

class ResponseTests: XCTestCase {
    /*

    <STATUS><SPACE><META><CR><LF>

    */

    // MARK: - Sad path

    // MARK: - Sad path - no status

    func testNoStatusWithMetaWithBody() {
        let meta = "hello world"
        let header = " \(meta)\r\n"
        let body = "test"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)

        XCTAssertNil(actualResponse)
    }

    func testNoStatusNoMetaWithBody() {
        let header = " \r\n"
        let body = "test"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)

        XCTAssertNil(actualResponse)
    }

    func testNoStatusWithMetaNoBody() {
        let meta = "any meta string["
        let header = " \(meta)\r\n"
        let rawResponse = header
        let actualResponse = Response(rawResponse: rawResponse)

        XCTAssertNil(actualResponse)
    }

    func testNoStatusNoMetaNoBody() {
        let header = " \r\n"
        let actualResponse = Response(rawResponse: header)

        XCTAssertNil(actualResponse)
    }

    // MARK: - Sad path - nonexistent status

    func testStatusTwoDigitNumberNotInSpec() {
        let meta = "lorem ipsum"
        let header = "99 " + meta + "\r\n"
        let actualResponse = Response(rawResponse: header)

        XCTAssertNil(actualResponse)
    }

    func testStatusOneDigitNumber() {
        let meta = "cert"
        let header = "6 " + meta + "\r\n"
        let actualResponse = Response(rawResponse: header)

        XCTAssertNil(actualResponse)
    }

    func testStatusThreeDigitNumber() {
        let meta = "hola.mundo"
        let header = "100 " + meta + "\r\n"
        let actualResponse = Response(rawResponse: header)

        XCTAssertNil(actualResponse)
    }

    func testStatusFourDigitNumber() {
        let meta = "meta string goes here"
        let header = "2345 " + meta + "\r\n"
        let actualResponse = Response(rawResponse: header)

        XCTAssertNil(actualResponse)
    }

    func testStatusString1() {
        let meta = "string for meta"
        let header = "\"20\" " + meta + "\r\n"
        let actualResponse = Response(rawResponse: header)

        XCTAssertNil(actualResponse)
    }

    func testStatusString2() {
        let meta = "string for meta"
        let header = "HI " + meta + "\r\n"
        let actualResponse = Response(rawResponse: header)

        XCTAssertNil(actualResponse)
    }

    // MARK: - Sad path - malformed response

    func testNoBodyNoHeader() {
        let actualResponse = Response(rawResponse: "")
        XCTAssertNil(actualResponse)
    }

    func testBodyOnlyNoHeader() {
        let body = """
        # Lorem Ipsum

        >Ipsum Lorem

        ```Swift code
        print("Hello world")
        ```
        """
        let actualResponse = Response(rawResponse: body)

        XCTAssertNil(actualResponse)
    }
}
