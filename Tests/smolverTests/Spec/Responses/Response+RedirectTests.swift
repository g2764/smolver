//
//  Response+RedirectTests.swift
//  smolver
//
//  Created by Justin Marshall on 06/27/24.
//  Copyright © 2024 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.

@testable import smolver
import XCTest

class ResponseRedirectTests: XCTestCase {
    /*

    <STATUS><SPACE><META><CR><LF>

    ### 30 (REDIRECT - TEMPORARY)
    #### META is new url to which to redirect, absolute or relative, required
    ### 31 (REDIRECT - PERMANENT)
    #### META is new url to which to redirect, absolute or relative, required

    */

    // MARK: - Helper functions

    private func createRedirectResponse(for status: String, urlOfSize size: UInt) -> Response? {
        let scheme = "gemini://"
        let schemeByteSize = UInt(scheme.data(using: .utf8)!.count)
        let longString = (0..<(size - schemeByteSize)).map { _ in "0" }.joined()

        let meta = scheme + URL(string: longString)!.absoluteString
        let header = status + " " + meta + "\r\n"
        let response = Response(rawResponse: header)
        return response
    }

    // MARK: -

    // MARK: - Happy path

    func testTemporaryRedirectWithMetaNoBody() {
        let meta = "gemini://local.host/new.gmi"
        let header = "30 " + meta + "\r\n"
        let expectedResponse = Response(status: .redirect(.temporary(newURL: URL(string: meta)!)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testPermanentRedirectWithMetaNoBody() {
        let meta = "gemini://example.com/sub/post.md"
        let header = "31 " + meta + "\r\n"
        let expectedResponse = Response(status: .redirect(.permanent(newURL: URL(string: meta)!)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    // MARK: - Happy path - meta at max length

    // These simply assert not nil because they are bounds testing the max meta byte size.
    // All the other business rules are accounted for in other tests.

    func testTemporaryRedirectMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createRedirectResponse(for: "30", urlOfSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testPermanentRedirectMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createRedirectResponse(for: "31", urlOfSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    // MARK: - Sad path - meta too big

    func testTemporaryRedirectMetaOverThousandTwentyFourBytesNoBody() {
        let response = createRedirectResponse(for: "30", urlOfSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testPermanentRedirectMetaOverThousandTwentyFourBytesNoBody() {
        let response = createRedirectResponse(for: "31", urlOfSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    // MARK: - Sad path - malformed response

    func testTemporaryRedirectMetaNotURL() {
        // Gemini URLs can be relative, so anything other than a space (" ")
        // is technically a valid meta URL
        let header = "30  \r\n"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testPermanentRedirectMetaNotURL() {
        // Gemini URLs can be relative, so anything other than a space (" ")
        // is technically a valid meta URL
        let header = "31  \r\n"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testTemporaryRedirectHeaderNoCarriageReturnNoLineFeed() {
        let header = "30 gemini://example.com"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testPermanentRedirectHeaderNoCarriageReturnNoLineFeed() {
        let header = "31 gemini://example.org"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    // MARK: - Sad path - unexpected body

    func testTemporaryRedirectWithMetaWithBody() {
        let header = "30 relativeURL\r\n"
        let body = "unexpected body!"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testPermanentRedirectWithMetaWithBody() {
        let header = "31 gemini://absolute.url\r\n"
        let body = "body is unexpected"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }
}
