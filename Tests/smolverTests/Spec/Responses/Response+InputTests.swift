//
//  Response+InputTests.swift
//  smolver
//
//  Created by Justin Marshall on 06/27/24.
//  Copyright © 2024 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.

@testable import smolver
import XCTest

class ResponseInputTests: ResponseTestHelpers {
    /*

    <STATUS><SPACE><META><CR><LF>

    ### 10 (INPUT)
    #### META is prompt displayed to user, required
    ### 11 (SENSITIVE INPUT)
    #### META is prompt displayed to user, required

    */

    // MARK: - Happy path

    func testInputWithMetaNoBody() {
        let userPrompt = "Enter search query"
        let header = "10 " + userPrompt + "\r\n"
        let expectedResponse = Response(status: .input(.text(userPrompt: userPrompt)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSensitiveInputWithMetaNoBody() {
        let userPrompt = "super secret key please"
        let header = "11 " + userPrompt + "\r\n"
        let expectedResponse = Response(status: .input(.sensitiveText(userPrompt: userPrompt)))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    // MARK: - Happy path - meta at max length

    // These simply assert not nil because they are bounds testing the max meta byte size.
    // All the other business rules are accounted for in other tests.

    func testInputMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "10", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testSensitiveInputMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "11", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    // MARK: - Sad path - meta too big

    func testInputMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "10", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testSensitiveInputMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "11", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    // MARK: - Sad path - malformed response

    func testInputHeaderNoCarriageReturnNoLineFeed() {
        let header = "10 enter input"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testSensitiveInputHeaderNoCarriageReturnNoLineFeed() {
        let header = "11 enter password"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    // MARK: - Sad path - unexpected body

    func testInputWithMetaWithBody() {
        let header = "10 entry\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testSensitiveInputWithMetaWithBody() {
        let header = "11 security\r\n"
        let body = "ipsum loreM"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }
}
