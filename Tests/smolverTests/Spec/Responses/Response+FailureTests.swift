//
//  Response+FailureTests.swift
//  smolver
//
//  Created by Justin Marshall on 06/27/24.
//  Copyright © 2024 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.

@testable import smolver
import XCTest

class ResponseFailureTests: ResponseTestHelpers {
    /*

    <STATUS><SPACE><META><CR><LF>

    ### 40 (TEMPORARY FAILURE)
    #### META may provide info for user, optional
    ### 41 (SERVER UNAVAILABLE)
    #### META may provide info for user, optional
    ### 42 (CGI ERROR)
    #### META may provide info for user, optional
    ### 43 (PROXY ERROR)
    #### META may provide info for user, optional
    ### 44 (SLOW DOWN)
    #### META must provide info for user, is human-readable failure reason, required

    ### 50 (PERMANENT FAILURE)
    #### META may provide info for user, optional
    ### 51 (NOT FOUND)
    #### META may provide info for user, optional
    ### 52 (GONE)
    #### META may provide info for user, optional
    ### 53 (PROXY REQUEST REFUSED)
    #### META may provide info for user, optional
    ### 59 (BAD REQUEST)
    #### META may provide info for user, optional

    */

    // MARK: - Helper functions

    private func createSlowDownResponse(intervalSize size: UInt) -> Response? {
        let meta = (0..<size).map { _ in "1" }.joined()
        let header = "44 " + meta + "\r\n"
        let response = Response(rawResponse: header)
        return response
    }

    // MARK: -

    // MARK: - Temporary

    // MARK: - Temporary - Happy path

    func testTemporaryFailureWithMetaNoBody() {
        let meta = "generic error message"
        let header = "40 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.temporary(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testTemporaryFailureNoMetaNoBody() {
        let meta = nil as String?
        let header = "40 " + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.temporary(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testServerUnavailableWithMetaNoBody() {
        let meta = "down for maintenance"
        let header = "41 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.serverUnavailable(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testServerUnavailableNoMetaNoBody() {
        let meta = nil as String?
        let header = "41 " + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.serverUnavailable(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testCGIErrorWithMetaNoBody() {
        let meta = "cgi script bugged out"
        let header = "42 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.cgiError(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testCGIErrorNoMetaNoBody() {
        let meta: String? = nil
        let header = "42 " + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.cgiError(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testProxyErrorWithMetaNoBody() {
        let meta = "request to downstream server timed out"
        let header = "43 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.proxyError(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testProxyErrorNoMetaNoBody() {
        let meta = nil as String?
        let header = "43 " + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.proxyError(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSlowDownWithMetaNoBody() {
        let userInfo = "not so fast there"
        let meta = userInfo
        let header = "44 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.temporary(.slowDown(userInfo: userInfo))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    // MARK: - Temporary - Happy path - meta at max length

    // These simply assert not nil because they are bounds testing the max meta byte size.
    // All the other business rules are accounted for in other tests.

    func testTemporaryFailureMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "40", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testServerUnavailableMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "41", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testCGIErrorMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "42", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testProxyErrorMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "43", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testSlowDownMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createSlowDownResponse(intervalSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    // MARK: - Temporary - Sad path - meta too big

    func testTemporaryFailureMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "40", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testServerUnavailableMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "41", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testCGIErrorMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "42", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testProxyErrorMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "43", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testSlowDownMetaOverThousandTwentyFourBytesNoBody() {
        let response = createSlowDownResponse(intervalSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    // MARK: - Temporary - Sad path - malformed response

    func testSlowDownNoMetaNoBody() {
        let header = "44 \r\n"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testTemporaryFailureHeaderNoCarriageReturnNoLineFeed() {
        let header = "40 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testServerUnavailableHeaderNoCarriageReturnNoLineFeed() {
        let header = "41 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testCGIErrorHeaderNoCarriageReturnNoLineFeed() {
        let header = "42 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testProxyErrorHeaderNoCarriageReturnNoLineFeed() {
        let header = "43 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testSlowDownHeaderNoCarriageReturnNoLineFeed() {
        let header = "44 1"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    // MARK: - Temporary - Sad path - unexpected body

    func testTemporaryFailureWithMetaWithBody() {
        let header = "40 server buggy\r\n"
        let body = "abcdefghijklmnop"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testTemporaryFailureNoMetaBody() {
        let header = "40 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testServerUnavailableWithMetaWithBody() {
        let header = "41 entry\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testServerUnavailableNoMetaWithBody() {
        let header = "41 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testCGIErrorWithMetaWithBody() {
        let header = "42 entry\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testCGIErrorNoMetaWithBody() {
        let header = "42 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testProxyErrorWithMetaWithBody() {
        let header = "43 entry\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testProxyErrorNoMetaWithbody() {
        let header = "43 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testSlowDownWithMetaBody() {
        let header = "44 22\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testSlowDownNoMetaWithBody() {
        let header = "44 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    // MARK: -

    // MARK: - Permanent

    // MARK: - Permanent - Happy path

    func testPermanentFailureWithMetaNoBody() {
        let meta = "server is broken"
        let header = "50 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.permanent(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testPermanentFailureNoMetaNoBody() {
        let meta = nil as String?
        let header = "50 " + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.permanent(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testNotFoundWithMetaNoBody() {
        let meta = "whatever it is you're looking for, 'tis not here"
        let header = "51 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.notFound(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testNotFoundNoMetaNoBody() {
        let meta = nil as String?
        let header = "51 " + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.notFound(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testGoneWithMetaNoBody() {
        let meta = "used to be here, no more"
        let header = "52 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.gone(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testGoneNoMetaNoBody() {
        let meta = nil as String?
        let header = "52 " + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.gone(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testProxyRequestRefusedWithMetaNoBody() {
        let meta = "can't proxy like that"
        let header = "53 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.proxyRequestRefused(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testProxyRequestRefusedNoMetaNoBody() {
        let meta = nil as String?
        let header = "53 " + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.proxyRequestRefused(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testBadRequestWithMetaNoBody() {
        let meta = "that doesn't look like a gemini request!"
        let header = "59 " + meta + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.badRequest(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testBadRequestNoMetaNoBody() {
        let meta = nil as String?
        let header = "59 " + "\r\n"
        let expectedResponse = Response(status: .failure(.permanent(.badRequest(userInfo: meta))))
        let actualResponse = Response(rawResponse: header)

        XCTAssertEqual(expectedResponse, actualResponse)
    }

    // MARK: - Permanent - Happy path - meta at max length

    // These simply assert not nil because they are bounds testing the max meta byte size.
    // All the other business rules are accounted for in other tests.

    func testPermanentFailureMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "50", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testNotFoundMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "51", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testGoneMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "52", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testProxyRequestRefusedMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "53", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    func testBadRequestMetaExactlyThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "59", ofSize: Response.maximumMetaByteSize)
        XCTAssertNotNil(response)
    }

    // MARK: - Permanent - Sad path - meta too big

    func testPermanentFailureMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "50", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testNotFoundMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "51", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testGoneMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "52", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testProxyRequestRefusedMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "53", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    func testBadRequestMetaOverThousandTwentyFourBytesNoBody() {
        let response = createResponse(for: "59", ofSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    // MARK: - Permanent - Sad path - malformed response

    func testPermanentFailureHeaderNoCarriageReturnNoLineFeed() {
        let header = "50 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testNotFoundHeaderNoCarriageReturnNoLineFeed() {
        let header = "51 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testGoneHeaderNoCarriageReturnNoLineFeed() {
        let header = "52 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testProxyRequestRefusedHeaderNoCarriageReturnNoLineFeed() {
        let header = "53 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testBadRequestHeaderNoCarriageReturnNoLineFeed() {
        let header = "59 "
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    // MARK: - Permanent - Sad path - unexpected body

    func testPermanentFailureWithMetaWithBody() {
        let header = "50 server crash\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testPermanentFailureNoMetaWithBody() {
        let header = "50 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testNotFoundWithMetaWithBody() {
        let header = "51 no such resource\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testNotFoundNoMetaWithBody() {
        let header = "51 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testGoneWithMetaWithBody() {
        let header = "52 deleted\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testGoneNoMetaWithBody() {
        let header = "52 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testProxyRequestRefusedWithMetaWithBody() {
        let header = "53 proxy no\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testProxyRequestRefusedNoMetaWithBody() {
        let header = "53 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testBadRequestWithMetaWithBody() {
        let header = "59 malformed request\r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testBadRequestNoMetaWithBody() {
        let header = "59 \r\n"
        let body = "lorem ipsum"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }
}
