//
//  Response+SuccessTests.swift
//  smolver
//
//  Created by Justin Marshall on 06/27/24.
//  Copyright © 2024 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.

@testable import smolver
import XCTest

class ResponseSuccessTests: XCTestCase {
    /*

    <STATUS><SPACE><META><CR><LF>

    ### 20 (SUCCESS)
    #### Only one with response body
    #### META is mime type
        #### charset optional if mime is text/.*, assumed be be utf8
        #### If meta empty, assume "text/gemini; charset=utf8"

    */

    // MARK: - Helper functions

    private func createTextSuccessResponses(header: String,
                                            body: String,
                                            mimeType urlMimeType: URL.MimeType,
                                            language: String?)
    -> (expectedResponse: Response, actualResponse: Response?)
    {
        let encoding = String.Encoding.utf8

        // This is a helper function intended only for tests needing text/* CGI responses.
        // So, to keep it simple, just check for gemini mime type here and assume
        // everything else is text/$NON_GEMINI
        let mimeType: Response.Status.MimeType
        if urlMimeType == .textGemini {
            mimeType = .text(.gemini(encoding: encoding, language: language), body.data(using: encoding)!)
        } else {
            mimeType = .text(.nonGemini(urlMimeType, encoding: encoding), body.data(using: encoding)!)
        }

        let expectedResponse = Response(status: .success(.success(mimeType: mimeType)))

        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)

        return (expectedResponse, actualResponse)
    }

    private func createSuccessResponse(mimeSize size: UInt) -> Response? {
        let mimeType = "text/gemini"
        let separator = "; "
        let charset = "utf-8"
        let combined = mimeType + separator + charset
        let mimeTypeByteSize = UInt(combined.data(using: .utf8)!.count)
        let longString = (0..<(size - mimeTypeByteSize)).map { _ in "0" }.joined()

        let meta = combined + longString
        let header = "20" + " " + meta + "\r\n"
        let body = "hi"
        let rawResponse = header + body
        let response = Response(rawResponse: rawResponse)
        return response
    }

    // MARK: -

    // these should be enough coverage for smolver's CGI needs, a Gemini
    // client library would need to parse response bodies, too

    // MARK: - Happy path

    func testSuccessGeminiWithMetaMime() {
        let responses = createTextSuccessResponses(header: "20 text/gemini\r\n",
                                                   body: "# Header!",
                                                   mimeType: .textGemini,
                                                   language: nil)
        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/gemini; charset=utf-8\r\n",
                                                   body: "=> example.com Example link",
                                                   mimeType: .textGemini,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeNoCharsetOneLang() {
        let responses = createTextSuccessResponses(header: "20 text/gemini; lang=en-US\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textGemini,
                                                   language: "en-US")

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeCharsetOneLang() {
        let responses = createTextSuccessResponses(header: "20 text/gemini; charset=utf-8; lang=en-US\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textGemini,
                                                   language: "en-US")

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeCharsetTwoLangs() {
        let responses = createTextSuccessResponses(header: "20 text/gemini; charset=utf-8; lang=fr,es\r\n",
                                                   body: "Some text",
                                                   mimeType: .textGemini,
                                                   language: "fr,es")

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessMarkdownWithMetaMime() {
        let responses = createTextSuccessResponses(header: "20 text/markdown\r\n",
                                                   body: "- [ ] Markdown here",
                                                   mimeType: .textMarkdown,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessMarkdownWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/markdown; charset=utf-8\r\n",
                                                   body: "- [ ] Markdown here",
                                                   mimeType: .textMarkdown,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessPlaintextWithMetaMime() {
        let responses = createTextSuccessResponses(header: "20 text/plain\r\n",
                                                   body: "Plain text body",
                                                   mimeType: .textPlain,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessPlaintextWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/plain; charset=utf-8\r\n",
                                                   body: "Plain text body",
                                                   mimeType: .textPlain,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessPythonWithMetaMime() {
        let responses = createTextSuccessResponses(header: "20 text/x-python\r\n",
                                                   body: "print('Hello from python!')",
                                                   mimeType: .textPython,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessPythonWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/x-python; charset=utf-8\r\n",
                                                   body: "print('Hello from python!')",
                                                   mimeType: .textPython,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessHTMLWithMetaMime() {
        let responses = createTextSuccessResponses(header: "20 text/html\r\n",
                                                   body: "<html><p>hi</p></html>",
                                                   mimeType: .textHTML,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessHTMLWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/html; charset=utf-8\r\n",
                                                   body: "<html><p>hi</p></html>",
                                                   mimeType: .textHTML,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessCSVWithMetaMime() {
        let responses = createTextSuccessResponses(header: "20 text/csv\r\n",
                                                   body: "language,name,swift,smolver",
                                                   mimeType: .textCSV,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessCSVWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/csv; charset=utf-8\r\n",
                                                   body: "language,name,swift,smolver",
                                                   mimeType: .textCSV,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessCPlusPlusWithMetaMime() {
        let responses = createTextSuccessResponses(header: "20 text/x-c++src\r\n",
                                                   body: "cout >>> This is likely invalid C++ syntax",
                                                   mimeType: .textCPlusPlus,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessCPlusPlusWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/x-c++src; charset=utf-8\r\n",
                                                   body: "cout >>> This is likely invalid C++ syntax",
                                                   mimeType: .textCPlusPlus,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessCacheManifestWithMetaMimeCharset() {
        let responses = createTextSuccessResponses(header: "20 text/cache-manifest; charset=utf-8\r\n",
                                                   body: "test",
                                                   mimeType: .textCacheManifest,
                                                   language: nil)

        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessPDF() {
        let header = "20 application/pdf\r\n".data(using: .utf8)!
        let body = """
        From the spec:
        >Internet media types are registered with a canonical form.  Content transferred
        >via Gemini MUST be represented in the appropriate canonical form prior to its
        >transmission except for "text" types, as defined in the next paragraph.

        It is beyond the scope of smolver to validate that this data is actually
        what its mime type claims. That protocol requirement, for smolver's implementation,
        is pushed to you as the server administrator.
        """
            .data(using: .utf8)!

        let expectedResponse = Response(status: .success(.success(mimeType: .other(.applicationPDF, body))))
        let actualResponse = Response(rawData: header + body)
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessPNG() {
        let header = "20 image/png\r\n".data(using: .utf8)!
        let body = """
        From the spec:
        >Internet media types are registered with a canonical form.  Content transferred
        >via Gemini MUST be represented in the appropriate canonical form prior to its
        >transmission except for "text" types, as defined in the next paragraph.

        It is beyond the scope of smolver to validate that this data is actually
        what its mime type claims. That protocol requirement, for smolver's implementation,
        is pushed to you as the server administrator.
        """
            .data(using: .utf8)!

        let expectedResponse = Response(status: .success(.success(mimeType: .other(.imagePNG, body))))
        let actualResponse = Response(rawData: header + body)
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessMP4() {
        let header = "20 video/mp4\r\n".data(using: .utf8)!
        let body = """
        From the spec:
        >Internet media types are registered with a canonical form.  Content transferred
        >via Gemini MUST be represented in the appropriate canonical form prior to its
        >transmission except for "text" types, as defined in the next paragraph.

        It is beyond the scope of smolver to validate that this data is actually
        what its mime type claims. That protocol requirement, for smolver's implementation,
        is pushed to you as the server administrator.
        """
            .data(using: .utf8)!

        let expectedResponse = Response(status: .success(.success(mimeType: .other(.videoMP4, body))))
        let actualResponse = Response(rawData: header + body)
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessXML() {
        let headerData = "20 application/xml\r\n"
            .data(using: .utf8)!

        let bodyData = """
        <?xml version="1.0" encoding="UTF-8"?>
        <server>
            <name>smolver</name>
            <protocol>Gemini</protocol>
            <language>Swift</language>
        </server>
        """
            .data(using: .utf8)!

        let expectedResponse = Response(status: .success(.success(mimeType: .other(.applicationXML, bodyData))))
        let actualResponse = Response(rawData: headerData + bodyData)
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessUnknownMimeType() {
        let headerData = "20 gibberish/made-up-data-file-smolver\r\n"
            .data(using: .utf8)!

        let bodyData = """
        - some data
        { }
        []
        - content is irrelevant
        as long as unknown mime type doesn't cause failure
        """
            .data(using: .utf8)!

        let expectedMimeType = URL.MimeType(
            type: "gibberish/made-up-data-file-smolver",
            extensions: []
        )
        let expectedResponseMimeType = Response.Status.MimeType.other(expectedMimeType, bodyData)
        let expectedResponse = Response(
            status: .success(.success(mimeType: expectedResponseMimeType))
        )

        let actualResponse = Response(rawData: headerData + bodyData)
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    // MARK: - Happy path - headers - text charsets

    func testSuccessTextGeminiMimeTypeHeaderIncludesCharsetStaticInit() {
        let response = Response(status: .success(.success(mimeType: .text(.gemini(encoding: .utf8, language: nil), Data()))))
        let expectedHeader = "20 text/gemini; charset=utf-8\r\n"
        let actualHeader = response.header
        XCTAssertEqual(expectedHeader, actualHeader)
    }

    func testSuccessTextGeminiMimeTypeHeaderIncludesCharsetStringInit() {
        let response = Response(rawResponse: "20 text/gemini; charset=utf-8\r\n# Hi")
        let expectedHeader = "20 text/gemini; charset=utf-8\r\n"
        let actualHeader = response?.header
        XCTAssertEqual(expectedHeader, actualHeader)
    }

    func testSuccessTextNonGeminiMimeTypeHeaderIncludesCharsetStaticInit() {
        let response = Response(status: .success(.success(mimeType: .text(.nonGemini(.textPlain, encoding: .utf8), Data()))))
        let expectedHeader = "20 text/plain; charset=utf-8\r\n"
        let actualHeader = response.header
        XCTAssertEqual(expectedHeader, actualHeader)
    }

    func testSuccessTextNonGeminiMimeTypeHeaderIncludesCharsetStringInit() {
        let response = Response(rawResponse: "20 text/markdown; charset=utf-8\r\n### content")
        let expectedHeader = "20 text/markdown; charset=utf-8\r\n"
        let actualHeader = response?.header
        XCTAssertEqual(expectedHeader, actualHeader)
    }

    // MARK: - Happy path - meta at max length

    func testSuccessMetaExactlyThousandTwentyFourBytes() {
        let response = createSuccessResponse(mimeSize: Response.maximumMetaByteSize)

        // This nil assertion (compared to all the others in similar sections in other files
        // asserting on not nil) is intentional because the only valid success response meta
        // parameters are charset, language, both, or neither. A 1024-byte size meta can only
        // theoretically fulfill those requirements. In practice, any meta that long would not
        // match any realistic combination of language and charset. Hence the nil assertion.
        XCTAssertNil(response)
    }

    // MARK: - Sad path - invalid response parameters

    func testSuccessGeminiWithMetaMimeInvalidParameters1() {
        // first (and only) parameter invalid
        let responses = createTextSuccessResponses(header: "20 text/gemini; something=else\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textGemini,
                                                   language: "en-US")

        // Per the spec, only charset and lang are valid parameters for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeInvalidParameters2() {
        // second parameter invalid
        let responses = createTextSuccessResponses(header: "20 text/gemini; charset=utf-8; hello=world\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textGemini,
                                                   language: "en-US")

        // Per the spec, only charset and lang are valid parameters for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeInvalidParameters3() {
        // first (of two) parameter invalid
        let responses = createTextSuccessResponses(header: "20 text/gemini; test=ing; lang=en-US\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textGemini,
                                                   language: "en-US")

        // Per the spec, only charset and lang are valid parameters for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeInvalidParameters4() {
        // both parameters invalid
        let responses = createTextSuccessResponses(header: "20 text/gemini; fake=parameter; another=fakeparam\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textGemini,
                                                   language: "en-US")

        // Per the spec, only charset and lang are valid parameters for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessGeminiWithMetaMimeInvalidParameters5() {
        // parameters flipped
        let responses = createTextSuccessResponses(header: "20 text/gemini; lang=es; charset=utf-8\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textGemini,
                                                   language: "en-US")

        // Per the spec, only charset and lang are valid parameters for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessMarkdownWithMetaMimeCharsetOneLang() {
        let responses = createTextSuccessResponses(header: "20 text/markdown; charset=utf-8; lang=en-US\r\n",
                                                   body: "=> url",
                                                   mimeType: .textMarkdown,
                                                   language: "en-US")

        // Per the spec, the lang parameter is only valid for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessTextPlainWithMetaMimeNoCharsetOneLang() {
        let responses = createTextSuccessResponses(header: "20 text/plain; lang=en-US\r\n",
                                                   body: ">Quote",
                                                   mimeType: .textPlain,
                                                   language: "en-US")

        // Per the spec, the lang parameter is only valid for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessHTMLWithMetaMimeNoCharsetOneLang() {
        let responses = createTextSuccessResponses(header: "20 text/html; lang=en-US\r\n",
                                                   body: "<p>Hi</p>",
                                                   mimeType: .textHTML,
                                                   language: "en-US")

        // Per the spec, the lang parameter is only valid for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessMP4WithMetaMimeNoCharsetOneLang() {
        let header = "20 video/mp4; lang=en-US\r\n"
        let body = "Doesn't matter, failure should happen before even attempting to parse body"
        let rawResponse = header + body
        let response = Response(rawResponse: rawResponse)

        // Per the spec, the lang parameter is only valid for the text/gemini mime type
        XCTAssertNil(response)
    }

    func testSuccessPNGWithMetaMimeCharsetOneLang() {
        let header = "20 image/png; charset=utf-8; lang=en-US\r\n"
        let body = "Doesn't matter, failure should happen before even attempting to parse body"
        let rawResponse = header + body
        let response = Response(rawResponse: rawResponse)

        // Per the spec, charset and lang are only valid parameters for the text/gemini mime type
        XCTAssertNil(response)
    }

    func testSuccessEnglishWithMetaMimeCharsetOneLang() {
        let responses = createTextSuccessResponses(header: "20 text/english; charset=utf-8; lang=en-US\r\n",
                                                   body: "Doesn't matter, failure should happen before even attempting to parse body",
                                                   mimeType: .imagePNG,
                                                   language: "en-US")

        // Per the spec, charset and lang are only valid parameters for the text/gemini mime type
        XCTAssertNil(responses.actualResponse)
    }

    func testSuccessJPEGWithMetaMimeCharset() {
        let header = "20 image/jpeg; charset=utf-8\r\n"
        let body = "Doesn't matter, failure should happen before even attempting to parse body"
        let rawResponse = header + body
        let response = Response(rawResponse: rawResponse)

        // Per the spec, the charset parameter is only valid for the text/gemini mime type
        XCTAssertNil(response)
    }

    // MARK: - Sad path - meta too big

    func testSuccessMetaOverThousandTwentyFourBytes() {
        let response = createSuccessResponse(mimeSize: Response.maximumMetaByteSize + 1)
        XCTAssertNil(response)
    }

    // MARK: - Sad path - malformed response

    func testSuccessNoBody() {
        let header = "20 text/gemini\r\n"
        let actualResponse = Response(rawResponse: header)
        XCTAssertNil(actualResponse)
    }

    func testSuccessGeminiWithMetaMimeNonUTF8Charset() {
        let header = "20 text/gemini; charset=utf-16\r\n"
        let body = "abcdef"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)

        // only UTF-8 char sets are supported
        XCTAssertNil(actualResponse)
    }

    func testSuccessGeminiNoMeta() {
        let responses = createTextSuccessResponses(header: "20 \r\n",
                                                   body: "asdf",
                                                   mimeType: .textGemini,
                                                   language: nil)
        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessGeminiMetaEmptyString() {
        let responses = createTextSuccessResponses(header: "20  \r\n",
                                                   body: "asdf",
                                                   mimeType: .textGemini,
                                                   language: nil)
        XCTAssertEqual(responses.expectedResponse, responses.actualResponse)
    }

    func testSuccessMetaTooManyParameters() {
        let header = "20 text/gemini; charset=utf-8; lang=es; extra=param\r\n"
        let body = "anything"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }

    func testSuccessHeaderNoCarriageReturnNoLineFeed() {
        let header = "20 text/gemini"
        let body = "uiop"
        let rawResponse = header + body
        let actualResponse = Response(rawResponse: rawResponse)
        XCTAssertNil(actualResponse)
    }
}
