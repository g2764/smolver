//
//  ClientCertificateValidatorTests.swift
//  smolver
//
//  Created by Justin Marshall on 9/21/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import SocketServer
import XCTest

// TODO: Next, add test for correct filename (fingerprint.pem), but file actually has a different fingerprint. Should fail. If this assumption correct, add it to backlog as a fixed defect. If this assumption incorrect, try it on the latest production code. It may have been inadvertently fixed

private final class TestingClientCertificate: ClientCertificateProtocol {
    let fingerprint: String
    let encryptionAlgorithm: X509Certificate.EncryptionAlgorithm
    let commonName: String
    let notValidBeforeDate: Date
    let notValidAfterDate: Date

    var writtenURL: URL?

    static var onDiskURLsChecked: [URL] = []
    static var onDiskURLChecked: URL? { onDiskURLsChecked.first }

    static var certificatesToCreate: [(fileName: URL, fingerprint: String)] = []

    init(fingerprint: String,
         notValidBeforeDate: Date,
         notValidAfterDate: Date,
         encryptionAlgorithm: X509Certificate.EncryptionAlgorithm = .sha256)
    {
        self.fingerprint = fingerprint
        self.encryptionAlgorithm = encryptionAlgorithm
        self.commonName = "unused for client certificate validation"
        self.notValidBeforeDate = notValidBeforeDate
        self.notValidAfterDate = notValidAfterDate
    }

    func write(to url: URL) {
        writtenURL = url
    }

    static func create(from url: URL) -> TestingClientCertificate? {
        onDiskURLsChecked.append(url)

        guard let fingerprint = certificatesToCreate.first(where: { $0.fileName == url })?.fingerprint else {
            return nil
        }

        return .init(fingerprint: fingerprint, notValidBeforeDate: Date(), notValidAfterDate: Date())
    }

    static func reset() {
        onDiskURLsChecked = []
        certificatesToCreate = []
    }
}

private extension Response.Status.ClientCertificate {
    func isEqualForTestingPurposes(to other: Self) -> Bool {
        switch (self, other) {
        case (.required, .required),
             (.notAuthorized, .notAuthorized),
             (.notValid, .notValid):
            return true
        default:
            return false
        }
    }
}

private class TestFileManager: FileManager {
    var shouldFileExist: Bool?
    var files: [String] = []

    var checkedPaths: [String] = []
    var checkedPath: String? {
        XCTAssert(checkedPaths.count <= 1, "The `checkedPath` property is present for backwards compatibility with previously-written unit tests. If you see this message, migrate the failing test to use `checkedPaths` instead.")
        return checkedPaths.first
    }

    override func fileExists(atPath path: String) -> Bool {
        checkedPaths.append(path)

        if shouldFileExist ?? false {
            return true
        } else if files.contains(path) {
            return true
        } else {
            return super.fileExists(atPath: path)
        }
    }

    override func contentsOfDirectory(atPath path: String) throws -> [String] {
        guard !files.isEmpty else {
            // system file manager throws error in this case, so want to
            // ensure we get system behavior
            return try super.contentsOfDirectory(atPath: path)
        }

        return files
    }
}

private extension ClientCertificateValidator {
    init(request: Request,
         x509Certificate: ClientCertificateProtocol?,
         authentication: LocalConfiguration.Authentication?,
         fileManager: FileManager = .default,
         logger: Logging.Type = Log.self)
    {
        self.init(
            request: request,
            x509Certificate: x509Certificate,
            authentication: authentication,
            clientHostname: "",
            fileManager: fileManager,
            logger: logger
        )
    }
}

/// Tests the client certificate functionality
///
/// The intention here is to test for all possible server configurations
/// at the layer of the configuration *struct*. As such, some possible
/// *json* configurations that cause the same struct are intentionally omitted.
///
/// For example, a config file without an "authentication" key will result in an
/// identical authentication configuration struct as would a config file with an
/// "authentication" key and an explicitly null value.
///
/// To illustrate:
///
/// {
///     "someKey": "value"
/// }
///
/// and
///
/// {
///     "authentication": null
/// }
///
/// will result in identical authentication configuration structs (nil), and so
/// this file considers them the same. These are not parser tests.
class ClientCertificateValidatorTests: XCTestCase {
    // MARK: - Setup

    override func tearDown() {
        super.tearDown()
        TestLogger.reset()
        TestingClientCertificate.reset()
    }

    // MARK: - Helper

    private enum AuthenticationType {
        case file, directory(hasExcludedFiles: Bool)

        func authentication(for fileName: String, at whitelistedPath: String) -> LocalConfiguration.Authentication {
            let files: [LocalConfiguration.Authentication.File]?
            let directory: LocalConfiguration.Authentication.Directory?

            switch self {
            case .file:
                files = [
                    .init(name: fileName, whitelistedPath: whitelistedPath)
                ]
                directory = nil
            case let .directory(hasExcludedFiles) where hasExcludedFiles:
                let excludedFiles = [
                    "openaccess.html"
                ]

                files = nil
                directory = .init(excludedFiles: excludedFiles, whitelistedPath: whitelistedPath)
            case .directory:
                files = nil
                directory = .init(excludedFiles: nil, whitelistedPath: whitelistedPath)
            }

            return .init(files: files, directory: directory)
        }
    }

    // MARK: - Helper functions

    private func testMatch(
        attempting fingerprint: String,
        via authenticationType: AuthenticationType,
        againstWhitelist whitelistedCertificateMetadata: (fileName: String, fingerprint: String)...
    ) throws {
        let request = try Request("gemini://example.com/blog/somefile.gmi\r\n", from: "", expectations: ("example.com", nil))

        let whitelistedPath = "glog/.whitelisted-authentications/"

        let authentication = authenticationType.authentication(for: "somefile.gmi", at: whitelistedPath)
        let testFileManager = TestFileManager()
        let customFileNames = whitelistedCertificateMetadata.map { GlobalConfiguration.configDirectory + whitelistedPath + $0.fileName }
        TestingClientCertificate.certificatesToCreate = customFileNames.enumerated().map { (.init(fileURLWithPath: $0.element), whitelistedCertificateMetadata[$0.offset].fingerprint) }
        testFileManager.files = customFileNames

        let certificate = TestingClientCertificate(fingerprint: fingerprint, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: certificate,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(certificate.writtenURL)

        let expectedMatchingCertificateFilename = TestingClientCertificate.certificatesToCreate.first { $0.fingerprint == fingerprint }?.fileName
        // after it finds a match, no more should be tried, hence the `.last`
        let actualMatchingCertificateFilename = TestingClientCertificate.onDiskURLsChecked.last
        XCTAssertEqual(expectedMatchingCertificateFilename, actualMatchingCertificateFilename)

        XCTAssertFalse(TestingClientCertificate.onDiskURLsChecked.isEmpty)

        let customFileURLs = TestingClientCertificate
                                        .certificatesToCreate
                                        .map { $0.fileName }
                                        .map { $0.absoluteString }
                                        .sorted()
        let clientCertificateURLs = TestingClientCertificate
                                       .onDiskURLsChecked
                                       .map { $0.absoluteString }
                                       .sorted()
        XCTAssertTrue(customFileURLs.contains(clientCertificateURLs))
    }

    private func testNoMatch(
        attempting fingerprint: String,
        via authenticationType: AuthenticationType,
        againstWhitelist whitelistedCertificateMetadata: (fileName: String, fingerprint: String)...
    ) throws {
        let request = try Request("gemini://xmpl.site/subdir/hello.gmi\r\n", from: "", expectations: ("xmpl.site", nil))

        let whitelistedPath = "my/whitelisted-authentications/"
        let authentication = authenticationType.authentication(for: "hello.gmi", at: whitelistedPath)
        let testFileManager = TestFileManager()
        let customFileNames = whitelistedCertificateMetadata.map { GlobalConfiguration.configDirectory + whitelistedPath + $0.fileName }
        TestingClientCertificate.certificatesToCreate = customFileNames.enumerated().map { (.init(fileURLWithPath: $0.element), whitelistedCertificateMetadata[$0.offset].fingerprint) }
        testFileManager.files = customFileNames
        let certificate = TestingClientCertificate(fingerprint: fingerprint, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: certificate,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(certificate.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
            case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notAuthorized(reason: nil)))
        }
        XCTAssertNil(certificate.writtenURL)

        // If we have no match, it better have checked every possibility, so this should not be empty by the time it finishes.
        XCTAssertFalse(TestingClientCertificate.onDiskURLsChecked.isEmpty)
        XCTAssertEqual(Set(TestingClientCertificate.onDiskURLsChecked), Set(TestingClientCertificate.certificatesToCreate.map { $0.fileName }))
    }

    // MARK: - Expected; given - Empty files, empty directory

    func testExpectedGivenNotYetValidEmptyFilesEmptyDirectoryInConfig() throws {
        let request = try Request("gemini://localhost/glog/hello.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantFuture, notValidAfterDate: Date.distantFuture)
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let files: [LocalConfiguration.Authentication.File] = []
        let authentication = LocalConfiguration.Authentication(files: files, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenExpiredEmptyFilesEmptyDirectoryInConfig() throws {
        let request = try Request("gemini://localhost/glog/hello.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantPast)
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let files: [LocalConfiguration.Authentication.File] = []
        let authentication = LocalConfiguration.Authentication(files: files, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenNeverBeforeSeenCertEmptyFilesEmptyDirectoryInConfig() throws {
        let request = try Request("gemini://localhost/starlog/stardate1.gemini\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -3600), notValidAfterDate: Date(timeIntervalSinceNow: 3600))
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let files: [LocalConfiguration.Authentication.File] = []
        let authentication = LocalConfiguration.Authentication(files: files, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "starlog/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
    }

    func testExpectedGivenBeforeSeenCertEmptyFilesEmptyDirectoryInConfig() throws {
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -100), notValidAfterDate: Date(timeIntervalSinceNow: 100))
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let files: [LocalConfiguration.Authentication.File] = []
        let authentication = LocalConfiguration.Authentication(files: files, directory: directory)

        let request = try Request("gemini://localhost/mypath/code.zip\r\n", from: "", expectations: ("localhost", nil))

        let testFileManager = TestFileManager()
        testFileManager.shouldFileExist = false
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "mypath/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
        cert.writtenURL = nil

        // same certificate and path as before, so we've seen it before, so it should not be written to disk again
        testFileManager.shouldFileExist = true

        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)

        let request2 = try Request("gemini://localhost/differentpath/differentfilE.py\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: testFileManager)

        // same certificate as before but different path, so it should be written to disk again
        testFileManager.shouldFileExist = false

        XCTAssertNoThrow(try validator2.validate())
        let expectedURL2 = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "differentpath/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL2, cert.writtenURL)
    }

    // MARK: - Expected; given - Only non-empty files with whitelist

    func testExpectedGivenNotYetValidNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/adios.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantFuture, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "glog/.whitelisted-authentications/"
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "hola.gmi", whitelistedPath: whitelistedPath),
            .init(name: "adios.gmi", whitelistedPath: whitelistedPath)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenExpiredNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/adios.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantPast)

        let whitelistedPath = "glog/.whitelisted-authentications/"
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "hola.gmi", whitelistedPath: whitelistedPath),
            .init(name: "adios.gmi", whitelistedPath: whitelistedPath)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenMatchesWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/adios.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "glog/.whitelisted-authentications/"
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "hola.gmi", whitelistedPath: whitelistedPath),
            .init(name: "adios.gmi", whitelistedPath: whitelistedPath)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let testFileManager = TestFileManager()
        let customFileName = GlobalConfiguration.configDirectory + whitelistedPath + cert.fingerprint + ".pem"
        TestingClientCertificate.certificatesToCreate = [(.init(fileURLWithPath: customFileName), cert.fingerprint)]
        testFileManager.files = [customFileName]
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, testFileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    func testExpectedGivenMatchesOneCustomFileNameWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let fingerprint = "matching-fingerprint!@#"
        try testMatch(
            attempting: fingerprint,
            via: .file,
            againstWhitelist: (fileName: "friend_bob" + ".pem", fingerprint: fingerprint)
        )
    }

    func testExpectedGivenMatchesTwoCustomFileNamesWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let fingerprint = "FiNgErPrInT:That:Match_es*@&@*$(!!2478124"
        try testMatch(
            attempting: fingerprint,
            via: .file,
            againstWhitelist:
            (
                fileName: "known_person" + ".pem",
                fingerprint: "no match"
            ),
            (
                fileName: "myfriend" + ".pem",
                fingerprint: fingerprint
            )
        )
    }

    func testExpectedGivenMatchesOneCustomFileNameOneFingerprintBasedWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let fingerprint = "MA:TC:IN:gf:in:ger:pR:1n:7"
        try testMatch(
            attempting: fingerprint,
            via: .file,
            againstWhitelist:
            (
                fileName: fingerprint + ".pem",
                fingerprint: fingerprint
            ),
            (
                fileName: "nonmatch-certificate" + ".pem",
                fingerprint: "this one does not match"
            )
        )
    }

    func testExpectedGivenNoMatchesWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let request = try Request("gemini://localhost/a/cool/sub/directory/myFile.gmi\r\n", from: "", expectations: ("localhost", nil))
        let clientCert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "allowed/users/"
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "hola.gmi", whitelistedPath: whitelistedPath),
            .init(name: "myFile.gmi", whitelistedPath: whitelistedPath)
        ]

        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let testFileManager = TestFileManager()
        let serversideCert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        XCTAssertNotEqual(clientCert.fingerprint, serversideCert.fingerprint)

        testFileManager.files = [GlobalConfiguration.configDirectory + whitelistedPath + serversideCert.fingerprint + ".pem"]
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: clientCert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(clientCert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notAuthorized(reason: nil)))
        }
        XCTAssertNil(clientCert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, testFileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    func testExpectedGivenNoMatchesOneCustomFileNameWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let fingerprint = "^MATCH!----#"
        try testNoMatch(
            attempting: fingerprint + fingerprint,
            via: .file,
            againstWhitelist: (fileName: "sue" + ".pem", fingerprint: fingerprint)
        )
    }

    func testExpectedGivenNoMatchesTwoCustomFileNamesWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let fingerprint = "this one matches"
        try testNoMatch(
            attempting: fingerprint,
            via: .file,
            againstWhitelist:
            (
                fileName: "billybob" + ".pem",
                fingerprint: "negative"
            ),
            (
                fileName: "billyjoe" + ".pem",
                fingerprint: fingerprint + fingerprint
            )
        )
    }

    func testExpectedGivenNoMatchesOneCustomFileNameOneFingerprintBasedWhitelistNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        let fingerprint = "got 'em"
        try testNoMatch(
            attempting: fingerprint,
            via: .file,
            againstWhitelist:
            (
                fileName: "billysue" + ".pem",
                fingerprint: "nooope"
            ),
            (
                fileName: "112" + fingerprint + "28282" + ".pem",
                fingerprint: "fingerprint"
            )
        )
    }

    // MARK: - Expected; given - Only non-empty files without whitelist

    func testExpectedGivenNotYetValidNonEmptyFilesOnlyWhitelistNotPresentInConfig() throws {
        // match
        let request = try Request("gemini://localhost/secure/supersecretdoc.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantFuture, notValidAfterDate: Date.distantFuture)
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "supersecretdoc.gmi", whitelistedPath: nil)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }

        XCTAssertNil(cert.writtenURL)

        // no match
        let request2 = try Request("gemini://localhost/secure/supersecretdoc2.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: nil,
                                                    authentication: authentication)

        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenExpiredNonEmptyFilesOnlyWhitelistNotPresentInConfig() throws {
        // match
        let request = try Request("gemini://localhost/secure/supersecretdoc.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantPast)
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "supersecretdoc.gmi", whitelistedPath: nil)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)

        // no match
        let request2 = try Request("gemini://localhost/secure/supersecretdoc2.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: nil,
                                                    authentication: authentication)

        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenNeverBeforeSeenCertNonEmptyFilesOnlyWhitelistNotPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/auth/needed/bye.gemini\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -3600), notValidAfterDate: Date(timeIntervalSinceNow: 3600))
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "hello.gmi", whitelistedPath: nil),
            .init(name: "bye.gemini", whitelistedPath: nil)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)

        let testFileManager = TestFileManager()
        testFileManager.shouldFileExist = false
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "auth/needed/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
    }

    func testExpectedGivenBeforeSeenCertNonEmptyFilesOnlyWhitelistNotPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/au/th/needed/bye.gemini\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -3600), notValidAfterDate: Date(timeIntervalSinceNow: 3600))
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "hello.gmi", whitelistedPath: nil),
            .init(name: "bye.gemini", whitelistedPath: nil),
            .init(name: "snake.gmi", whitelistedPath: nil)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)

        let testFileManager = TestFileManager()
        testFileManager.shouldFileExist = false
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "au/th/needed/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
        cert.writtenURL = nil

        // same certificate and path as before, so we've seen it before, so it should not be written to disk again
        testFileManager.shouldFileExist = true

        // match 2
        let request2 = try Request("gemini://localhost/au/th/needed/hello.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)

        // match 3
        let request3 = try Request("gemini://localhost/au/th/also/needed/snake.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: testFileManager)

        // same certificate as before but different path, so it should be written to disk again
        testFileManager.shouldFileExist = false

        XCTAssertNoThrow(try validator3.validate())
        let expectedURL2 = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "au/th/also/needed/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL2, cert.writtenURL)
        cert.writtenURL = nil

        // no match
        let request4 = try Request("gemini://localhost/au/th/also/needed/sneke.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator4 = ClientCertificateValidator(request: request4,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: testFileManager)

        // same certificate as before but file is not authenticated, so it should not be written to disk again
        testFileManager.shouldFileExist = false

        XCTAssertNoThrow(try validator4.validate())
        XCTAssertNil(cert.writtenURL)
    }

    // MARK: - Expected; given - Only non-empty directories with whitelist and excludedFiles

    func testExpectedGivenNotYetValidNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/dog.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantFuture, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "glog/" + ".whitelisted-authentications/"
        let excludedFiles = [
            "cat.gmi"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenExpiredNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/dog.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantPast)

        let whitelistedPath = "glog/" + ".whitelisted-authentications/"
        let excludedFiles = [
            "cat.gmi"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenMatchesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let request = try Request("gemini://localhost/gemlog/lizard.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "glog/.whitelisted-authentications/"
        let excludedFiles = [
            "snek.gmi"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let testFileManager = TestFileManager()
        let customFileName = GlobalConfiguration.configDirectory + whitelistedPath + cert.fingerprint + ".pem"
        TestingClientCertificate.certificatesToCreate = [(.init(fileURLWithPath: customFileName), cert.fingerprint)]
        testFileManager.files = [customFileName]
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, testFileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    func testExpectedGivenMatchesOneCustomFileNameWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let fingerprint = "the print of the finger is a match"
        try testMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: true),
            againstWhitelist: (fileName: "pwnd" + ".pem", fingerprint: fingerprint)
        )
    }

    func testExpectedGivenMatchesTwoCustomFileNamesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let fingerprint = "'tis a match"
        try testMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: true),
            againstWhitelist:
            (
                fileName: "'ello" + ".pem",
                fingerprint: fingerprint
            ),
            (
                fileName: "sparrow" + ".pem",
                fingerprint: "fingerprint nay matches"
            )
        )
    }

    func testExpectedGivenMatchesOneCustomFileNameOneFingerprintBasedWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let fingerprint = "matching, on the double!"
        try testMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: true),
            againstWhitelist:
            (
                fileName: fingerprint + ".pem",
                fingerprint: fingerprint
            ),
            (
                fileName: "he got away" + ".pem",
                fingerprint: "no can do"
            )
        )
    }

    func testExpectedGivenNoMatchesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let request = try Request("gemini://localhost/a/cool/sub/directory/myFile.gmi\r\n", from: "", expectations: ("localhost", nil))
        let clientCert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "a/cool/sub/directory/allowed/users/"
        let excludedFiles = [
            "something.zip"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: whitelistedPath)

        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let testFileManager = TestFileManager()
        let serversideCert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        XCTAssertNotEqual(clientCert.fingerprint, serversideCert.fingerprint)

        testFileManager.files = [GlobalConfiguration.configDirectory + whitelistedPath + "/" + serversideCert.fingerprint + ".pem"]
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: clientCert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(clientCert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notAuthorized(reason: nil)))
        }
        XCTAssertNil(clientCert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, testFileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    func testExpectedGivenNoMatchesOneCustomFileNameWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let fingerprint = "the print of the finger is a match"
        try testNoMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: true),
            againstWhitelist: (fileName: "pwnd" + ".pem", fingerprint: "match, does not")
        )
    }

    func testExpectedGivenNoMatchesTwoCustomFileNamesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let fingerprint = "'tis a match"
        try testNoMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: true),
            againstWhitelist:
            (
                fileName: "'ello" + ".pem",
                fingerprint: fingerprint + "nope"
            ),
            (
                fileName: "sparrow" + ".pem",
                fingerprint: "fingerprint nay matches"
            )
        )
    }

    func testExpectedGivenNoMatchesOneCustomFileNameOneFingerprintBasedWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        let fingerprint = "matching, on the double!"
        try testNoMatch(
            attempting: "uh no " + fingerprint,
            via: .directory(hasExcludedFiles: true),
            againstWhitelist:
            (
                fileName: fingerprint + ".pem",
                fingerprint: fingerprint
            ),
            (
                fileName: "he got away" + ".pem",
                fingerprint: "no can do"
            )
        )
    }

    // MARK: - Expected; given - Only non-empty directories with whitelist, no excludedFiles

    func testExpectedGivenNotYetValidNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/dog.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantFuture, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "glog/" + ".whitelisted-authentications/"
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenExpiredNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/dog.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantPast)

        let whitelistedPath = "glog/" + ".whitelisted-authentications/"
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenMatchesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let request = try Request("gemini://localhost/gemlog/lizard.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "glog/.whitelisted-authentications/"
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let testFileManager = TestFileManager()
        let customFileName = GlobalConfiguration.configDirectory + whitelistedPath + cert.fingerprint + ".pem"
        TestingClientCertificate.certificatesToCreate = [(.init(fileURLWithPath: customFileName), cert.fingerprint)]
        testFileManager.files = [customFileName]
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, testFileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    func testExpectedGivenMatchesOneCustomFileNameWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let fingerprint = "the print of the finger is a match"
        try testMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: false),
            againstWhitelist: (fileName: "pwnd" + ".pem", fingerprint: fingerprint)
        )
    }

    func testExpectedGivenMatchesTwoCustomFileNamesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let fingerprint = "'tis a match"
        try testMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: false),
            againstWhitelist:
            (
                fileName: "'ello" + ".pem",
                fingerprint: fingerprint
            ),
            (
                fileName: "sparrow" + ".pem",
                fingerprint: "fingerprint nay matches"
            )
        )
    }

    func testExpectedGivenMatchesOneCustomFileNameOneFingerprintBasedWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let fingerprint = "matching, on the double!"
        try testMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: false),
            againstWhitelist:
            (
                fileName: "he got away" + ".pem",
                fingerprint: "no can do"
            ),
            (
                fileName: fingerprint + ".pem",
                fingerprint: fingerprint
            )
        )
    }

    func testExpectedGivenNoMatchesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let request = try Request("gemini://localhost/a/cool/sub/directory/something.tar\r\n", from: "", expectations: ("localhost", nil))
        let clientCert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        let whitelistedPath = "a/cool/sub/directory/allowed/users/"
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: whitelistedPath)

        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let testFileManager = TestFileManager()
        let serversideCert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        XCTAssertNotEqual(clientCert.fingerprint, serversideCert.fingerprint)

        testFileManager.files = [GlobalConfiguration.configDirectory + whitelistedPath + "/" + serversideCert.fingerprint + ".pem"]
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: clientCert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(clientCert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notAuthorized(reason: nil)))
        }
        XCTAssertNil(clientCert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, testFileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    func testExpectedGivenNoMatchesOneCustomFileNameWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let fingerprint = "the print of the finger is a match"
        try testNoMatch(
            attempting: "a" + fingerprint + "b",
            via: .directory(hasExcludedFiles: false),
            againstWhitelist: (fileName: "pwnd" + ".pem", fingerprint: fingerprint)
        )
    }

    func testExpectedGivenNoMatchesTwoCustomFileNamesWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let fingerprint = "'tis a match"
        try testNoMatch(
            attempting: fingerprint,
            via: .directory(hasExcludedFiles: false),
            againstWhitelist:
            (
                fileName: "'ello" + ".pem",
                fingerprint: "fake data here, no match"
            ),
            (
                fileName: "sparrow" + ".pem",
                fingerprint: "fingerprint nay matches"
            )
        )
    }

    func testExpectedGivenNoMatchesOneCustomFileNameOneFingerprintBasedWhitelistNonEmptyDirectoriesOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        let fingerprint = "matching, on the double!"
        try testNoMatch(
            attempting: fingerprint + fingerprint + fingerprint,
            via: .directory(hasExcludedFiles: false),
            againstWhitelist:
            (
                fileName: fingerprint + ".pem",
                fingerprint: fingerprint + fingerprint
            ),
            (
                fileName: "he got away" + ".pem",
                fingerprint: "no can do"
            )
        )
    }

    // MARK: - Expected; given - Only non-empty directories without whitelist, no excludedFiles

    func testExpectedGivenNotYetValidNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesNotPresentInConfig() throws {
        let request = try Request("gemini://localhost/glog/hello.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantFuture, notValidAfterDate: Date.distantFuture)
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenExpiredNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesNotPresentInConfig() throws {
        let request = try Request("gemini://localhost/blog/hello.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantPast)
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenNeverBeforeSeenCertNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesNotPresentInConfig() throws {
        let request = try Request("gemini://localhost/index.gemini\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -3600), notValidAfterDate: Date(timeIntervalSinceNow: 3600))
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
    }

    func testExpectedGivenBeforeSeenCertNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesNotPresentInConfig() throws {
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -100), notValidAfterDate: Date(timeIntervalSinceNow: 100))
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)

        let request = try Request("gemini://localhost/mypath/code.zip\r\n", from: "", expectations: ("localhost", nil))

        let testFileManager = TestFileManager()
        testFileManager.shouldFileExist = false
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "mypath/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
        cert.writtenURL = nil

        // same certificate and path as before, so we've seen it before, so it should not be written to disk again
        testFileManager.shouldFileExist = true

        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)

        let request2 = try Request("gemini://localhost/differentpath/differentfilE.py\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: testFileManager)

        // same certificate as before but different path, so it should be written to disk again
        testFileManager.shouldFileExist = false

        XCTAssertNoThrow(try validator2.validate())
        let expectedURL2 = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "differentpath/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL2, cert.writtenURL)
    }

    // MARK: - Expected; given - Only non-empty directories without whitelist, with excludedFiles

    func testExpectedGivenNotYetValidNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/dirr/notafile.go\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantFuture, notValidAfterDate: Date.distantFuture)
        let excludedFiles = [
            "file.vim",
            "notafile.go"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)

        // match 2
        let request2 = try Request("gemini://localhost/dirr/file.vim\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)

        // no match
        let request3 = try Request("gemini://localhost/dirr/file.vimm\r\n", from: "", expectations: ("localhost", nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                    x509Certificate: cert,
                                                    authentication: authentication)
        XCTAssertThrowsError(try validator3.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenExpiredNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/aoeu\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantPast)
        let excludedFiles = [
            "aoeu",
            "idhtn"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)

        // match 2
        let request2 = try Request("gemini://localhost/idhtn\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)

        // no match
        let request3 = try Request("gemini://localhost/dirr/file.vimm\r\n", from: "", expectations: ("localhost", nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                    x509Certificate: cert,
                                                    authentication: authentication)
        XCTAssertThrowsError(try validator3.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notValid(reason: nil)))
        }
        XCTAssertNil(cert.writtenURL)
    }

    func testExpectedGivenNeverBeforeSeenNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/ohteu\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -3600), notValidAfterDate: Date(timeIntervalSinceNow: 3600))
        let excludedFiles = [
            "ohteu",
            "982.nqjmbtk"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)

        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)

        // match 2
        let request2 = try Request("gemini://localhost/982.nqjmbtk\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)

        // no match
        let request3 = try Request("gemini://localhost/dirr/file.vimm\r\n", from: "", expectations: ("localhost", nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                    x509Certificate: cert,
                                                    authentication: authentication)
        XCTAssertNoThrow(try validator3.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "dirr/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
    }

    func testExpectedGivenBeforeSeenNonEmptyDirectoriesOnlyWhitelistNotPresentExcludedFilesPresentInConfig() throws {
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date(timeIntervalSinceNow: -100), notValidAfterDate: Date(timeIntervalSinceNow: 100))
        let excludedFiles = [
            "hcunaotu",
            "nqsmjbk.zip"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)

        // no match

        let request = try Request("gemini://localhost/mypath/code.zip\r\n", from: "", expectations: ("localhost", nil))

        let testFileManager = TestFileManager()
        testFileManager.shouldFileExist = false
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertNoThrow(try validator.validate())
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "mypath/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL, cert.writtenURL)
        cert.writtenURL = nil

        // same certificate and path as before, so we've seen it before, so it should not be written to disk again
        testFileManager.shouldFileExist = true

        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)

        let request2 = try Request("gemini://localhost/dfrnt/path/differentfilE.py\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: testFileManager)

        // same certificate as before but different path, so it should be written to disk again
        testFileManager.shouldFileExist = false

        XCTAssertNoThrow(try validator2.validate())
        let expectedURL2 = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "dfrnt/path/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        XCTAssertEqual(expectedURL2, cert.writtenURL)
    }

    // MARK: - Expected; given - Both non-empty files and directories

    func testExpectedGivenNonEmptyFilesNonEmptyDirectoryInConfig() throws {
        let request = try Request("gemini://localhost/aoeu/123\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)

        let file = LocalConfiguration.Authentication.File(name: "optedinfile.csv", whitelistedPath: nil)
        let excludedFiles = [
            "hello.world",
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: [file], directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   logger: TestLogger.self)
        try? validator.validate() // don't care if this throws or not, only need to see if it causes a log to happen
        XCTAssertTrue(TestLogger.didLogWarning)
    }

    // MARK: - Expected; not given - Both non-empty files and directories

    func testExpectedNotGivenNonEmptyFilesNonEmptyDirectoryInConfig() throws {
        let request = try Request("gemini://localhost/aoeu/123\r\n", from: "", expectations: ("localhost", nil))

        let file = LocalConfiguration.Authentication.File(name: "optedinfile.csv", whitelistedPath: nil)
        let excludedFiles = [
            "world.hello",
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: [file], directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: nil,
                                                   authentication: authentication,
                                                   logger: TestLogger.self)
        try? validator.validate() // don't care if this throws or not, only need to see if it causes a log to happen
        XCTAssertTrue(TestLogger.didLogWarning)
    }

    // MARK: - Expected; not given

    func testExpectedNotGivenEmptyFilesEmptyDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: [], directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }
    }

    func testExpectedNotGivenNilFilesEmptyDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }
    }

    func testExpectedNotGivenNonEmptyFilesOnlyWhitelistPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/gemlog/hello.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let whitelistedPath = request.path
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "hello.gmi", whitelistedPath: whitelistedPath),
            .init(name: "bye.gmi", whitelistedPath: nil),
            .init(name: "test.gmi", whitelistedPath: whitelistedPath)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }
        // match 2
        let request2 = try Request("gemini://localhost/gemlog/test.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator2.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }

        // match 3
        let request3 = try Request("gemini://localhost/gemlog/bye.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator3.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }

        // no match
        let request4 = try Request("gemini://localhost/gemlog/bye\r\n", from: "", expectations: ("localhost", nil))
        let validator4 = ClientCertificateValidator(request: request4,
                                                    x509Certificate: nil,
                                                    authentication: authentication)

        XCTAssertNoThrow(try validator4.validate())
    }

    func testExpectedNotGivenNonEmptyFilesOnlyWhitelistNotPresentInConfig() throws {
        // match
        let request = try Request("gemini://localhost/secure/supersecretdoc.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "supersecretdoc.gmi", whitelistedPath: nil)
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)

        XCTAssertThrowsError(try validator.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }

        // no match
        let request2 = try Request("gemini://localhost/secure/supersecretdoc2.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: nil,
                                                    authentication: authentication)

        XCTAssertNoThrow(try validator2.validate())
    }

    func testExpectedNotGivenNonEmptyDirectoryOnlyWhitelistPresentExcludedFilesPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/somedir/smolver.txt\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let whitelistedPath = request.path
        let excludedFiles = [
            "publicFile.gmi",
            "unauthenticatedScript.sh",
            "smolver.txt"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())

        // match 2
        let request2 = try Request("gemini://localhost/somedir/unauthenticatedScript.sh\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator2.validate())

        // match 3
        let request3 = try Request("gemini://localhost/somedir/publicFile.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator3.validate())

        // no match
        let request4 = try Request("gemini://localhost/somedir/coolAuthenticatedFile.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator4 = ClientCertificateValidator(request: request4,
                                                    x509Certificate: nil,
                                                    authentication: authentication)
        XCTAssertThrowsError(try validator4.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }
    }

    func testExpectedNotGivenNonEmptyDirectoryOnlyWhitelistPresentExcludedFilesNotPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/abc/file.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let whitelistedPath = request.path
        let excludedFiles: [String] = []
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }

        // match 2
        let request2 = try Request("gemini://localhost/abc/\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator2.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }
    }

    func testExpectedNotGivenNonEmptyDirectoryOnlyWhitelistNotPresentExcludedFilesNotPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/stories/rough-draft.gmi\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }

        // match 2
        let request2 = try Request("gemini://localhost/stories/outline.gmi\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertThrowsError(try validator2.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }
    }

    func testExpectedNotGivenNonEmptyDirectoryOnlyWhitelistNotPresentExcludedFilesPresentInConfig() throws {
        // match 1
        let request = try Request("gemini://localhost/mainCode/main.swift\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let excludedFiles = [
            "main.swift",
            "main.c",
            "main.python"
        ]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: excludedFiles, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())

        // match 2
        let request2 = try Request("gemini://localhost/mainCode/main.c\r\n", from: "", expectations: ("localhost", nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator2.validate())

        // match 3
        let request3 = try Request("gemini://localhost/mainCode/main.python\r\n", from: "", expectations: ("localhost", nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator3.validate())

        // no match
        let request4 = try Request("gemini://localhost/mainCode/main.sh\r\n", from: "", expectations: ("localhost", nil))
        let validator4 = ClientCertificateValidator(request: request4,
                                                    x509Certificate: nil,
                                                    authentication: authentication)
        XCTAssertThrowsError(try validator4.validate()) { error in
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .required(reason: nil)))
        }
    }

    // MARK: - Not expected; given

    func testNotExpectedGivenNoAuthenticationInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: "aoeuidhtns",
                                            notValidBeforeDate: Date(),
                                            notValidAfterDate: Date())
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: nil)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
    }

    func testNotExpectedGivenNotYetValidEmptyFilesNilDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: "9283doa;bvW2 3'239 r,m.tnoemku",
                                            notValidBeforeDate: Date.distantFuture,
                                            notValidAfterDate: Date.distantFuture)
        let authentication = LocalConfiguration.Authentication(files: [], directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication:authentication)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
    }

    func testNotExpectedGivenExpiredEmptyFilesNilDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: "9283doa;bvW2 3'239 r,m.tnoemku",
                                            notValidBeforeDate: Date.distantPast,
                                            notValidAfterDate: Date.distantPast)
        let authentication = LocalConfiguration.Authentication(files: [], directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication:authentication)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
    }

    func testNotExpectedGivenEmptyFilesNilDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: "9283doa;bvW2 3'239 r,m.tnoemku",
                                            notValidBeforeDate: Date.distantPast,
                                            notValidAfterDate: Date.distantFuture)
        let authentication = LocalConfiguration.Authentication(files: [], directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication:authentication)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
    }

    func testNotExpectedGivenNilFilesNilDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert = TestingClientCertificate(fingerprint: "v;qmjbk9283c",
                                            notValidBeforeDate: Date(),
                                            notValidAfterDate: Date())
        let authentication = LocalConfiguration.Authentication(files: nil, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
        XCTAssertNil(cert.writtenURL)
    }

    // MARK: - Not expected; not given

    func testNotExpectedNotGivenNoAuthenticationInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: nil)
        XCTAssertNoThrow(try validator.validate())
    }

    func testNotExpectedNotGivenEmptyFilesNilDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let authentication = LocalConfiguration.Authentication(files: [], directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
    }

    func testNotExpectedNotGivenNilFilesNilDirectoryInConfig() throws {
        let request = try Request("gemini://localhost\r\n", from: "", expectations: ("localhost", nil))
        let cert: ClientCertificateProtocol? = nil
        let authentication = LocalConfiguration.Authentication(files: nil, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication)
        XCTAssertNoThrow(try validator.validate())
    }

    // MARK: -

    /// For the purposes of these tests, the exact `.smol.json` configuration is
    /// irrelevant. All that has been tested elsewhere in this file. These simply
    /// test that, per the spec:
    ///
    /// Let's say we have a cert on disk at ~/.smolver/.whitelist/allowed_user.pem.
    /// That whitelist directory is in smolver's config. What if we tell OpenSSL to
    /// read that file and give us a certificate pointer and that fails? For example,
    /// what if the file is empty or just contains the string "hello world"?

    // MARK: - Expected; given - "Cert" on disk is not actually a real cert

    func testExpectedGivenNotRealCertificateFileOnDiskOnlyFileWhitelistPresentInConfig() throws {
        let request = try Request("gemini://localhost/gemlog/welcome.gmi\r\n", from: "", expectations: ("localhost", nil))

        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)
        let whitelistedPath = ".whitelist/"
        let fileManager = TestFileManager()
        fileManager.files = [GlobalConfiguration.configDirectory + whitelistedPath + cert.fingerprint + ".pem"]

        let files: [LocalConfiguration.Authentication.File] = [
            .init(name: "welcome.gmi", whitelistedPath: whitelistedPath),
        ]
        let authentication = LocalConfiguration.Authentication(files: files, directory: nil)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: fileManager)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notAuthorized(reason: nil)), "clientCertificateResponse was \(clientCertificateResponse), expected .notAuthorized")
        }

        XCTAssertNil(cert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    func testExpectedGivenNotRealCertificateFileOnDiskOnlyDirectoryWhitelistPresentInConfig() throws {
        let request = try Request("gemini://localhost/gemlog/welcome.gmi\r\n", from: "", expectations: ("localhost", nil))

        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)
        let whitelistedPath = ".whitelist/"
        let fileManager = TestFileManager()
        fileManager.files = [GlobalConfiguration.configDirectory + whitelistedPath + cert.fingerprint + ".pem"]

        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: cert,
                                                   authentication: authentication,
                                                   fileManager: fileManager)
        XCTAssertThrowsError(try validator.validate()) { error in
            XCTAssertNil(cert.writtenURL)
            guard let error = error as? ClientCertificateValidator.Error,
                case .clientCertificate(let clientCertificateResponse) = error else {
                return XCTFail("Wrong error type")
            }

            XCTAssertTrue(clientCertificateResponse.isEqualForTestingPurposes(to: .notAuthorized(reason: nil)), "clientCertificateResponse was \(clientCertificateResponse), expected .notAuthorized")
        }

        XCTAssertNil(cert.writtenURL)
        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
    }

    // MARK: -

    /// For the purposes of these tests, the exact `.smol.json` configuration is
    /// irrelevant. All that has been tested elsewhere in this file. These simply
    /// test that, per the spec:
    ///
    /// Client certificates are supposed to have their scope bound to the same hostname
    /// as the request URL and to all paths below the path of the request URL path.

    // MARK: - Expected; given - Subdirectory cert acceptance

    func testExpectedGivenAuthenticationAcceptedInSubdirectoriesWithWhitelist() throws {
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)
        let whitelistedPath = ".whitelisted-authentications"
        let fileManager = TestFileManager()
        let customFileName = GlobalConfiguration.configDirectory + whitelistedPath + cert.fingerprint + ".pem"
        TestingClientCertificate.certificatesToCreate = [(.init(fileURLWithPath: customFileName), cert.fingerprint)]
        fileManager.files = [customFileName]
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: whitelistedPath)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)

        let request1 = try Request("gemini://local.host.site/gem/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator1 = ClientCertificateValidator(request: request1,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator1.validate())

        XCTAssertNil(cert.writtenURL)

        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)

        TestingClientCertificate.onDiskURLsChecked = []

        let request2 = try Request("gemini://local.host.site/gem/sub/\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)

        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
        TestingClientCertificate.onDiskURLsChecked = []

        let request3 = try Request("gemini://local.host.site/gem/sub/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator3.validate())
        XCTAssertNil(cert.writtenURL)

        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
        TestingClientCertificate.onDiskURLsChecked = []

        let request4 = try Request("gemini://local.host.site/gem/sub/another/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator4 = ClientCertificateValidator(request: request4,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator4.validate())
        XCTAssertNil(cert.writtenURL)

        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
        TestingClientCertificate.onDiskURLsChecked = []

        let request5 = try Request("gemini://local.host.site/gem/sub/another/and/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator5 = ClientCertificateValidator(request: request5,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator5.validate())
        XCTAssertNil(cert.writtenURL)

        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
        TestingClientCertificate.onDiskURLsChecked = []

        let request6 = try Request("gemini://local.host.site/gem/sub/another/and/another/\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator6 = ClientCertificateValidator(request: request6,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator6.validate())
        XCTAssertNil(cert.writtenURL)

        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
        TestingClientCertificate.onDiskURLsChecked = []

        let request7 = try Request("gemini://local.host.site/gem/sub/another/and/another/again/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator7 = ClientCertificateValidator(request: request7,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator7.validate())
        XCTAssertNil(cert.writtenURL)

        XCTAssertEqual(TestingClientCertificate.onDiskURLChecked?.path, fileManager.files.first)
        XCTAssertNotNil(TestingClientCertificate.onDiskURLChecked)
        TestingClientCertificate.onDiskURLsChecked = []
    }

    func testExpectedGivenAuthenticationAcceptedInSubdirectoriesWithoutWhitelist() throws {
        let cert = TestingClientCertificate(fingerprint: UUID().uuidString, notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)
        let expectedURL = URL(fileURLWithPath: GlobalConfiguration.configDirectory + "gem/" + ".anonymous-authentications/" + cert.fingerprint + ".pem")
        let fileManager = TestFileManager()
        fileManager.shouldFileExist = false
        let directory = LocalConfiguration.Authentication.Directory(excludedFiles: nil, whitelistedPath: nil)
        let authentication = LocalConfiguration.Authentication(files: nil, directory: directory)

        let request1 = try Request("gemini://local.host.site/gem/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator1 = ClientCertificateValidator(request: request1,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator1.validate())
        XCTAssertEqual(expectedURL, cert.writtenURL)
        fileManager.checkedPaths.forEach { checkedPath in
            XCTAssertTrue(checkedPath.contains(GlobalConfiguration.smolverSubdirectory), "Checked path \"\(checkedPath)\" does not contain \"\(GlobalConfiguration.smolverSubdirectory)\"")
            XCTAssertTrue(checkedPath.contains(cert.fingerprint), "Checked path \"\(checkedPath)\" does not contain \"\(cert.fingerprint)\"")
        }
        fileManager.checkedPaths = []
        cert.writtenURL = nil
        fileManager.shouldFileExist = nil
        fileManager.files = [expectedURL.path]

        let request2 = try Request("gemini://local.host.site/gem/sub/\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator2 = ClientCertificateValidator(request: request2,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator2.validate())
        XCTAssertNil(cert.writtenURL)
        fileManager.checkedPaths.forEach { checkedPath in
            XCTAssertTrue(checkedPath.contains(GlobalConfiguration.smolverSubdirectory), "Checked path \"\(checkedPath)\" does not contain \"\(GlobalConfiguration.smolverSubdirectory)\"")
            XCTAssertTrue(checkedPath.contains(cert.fingerprint), "Checked path \"\(checkedPath)\" does not contain \"\(cert.fingerprint)\"")
        }
        fileManager.checkedPaths = []

        let request3 = try Request("gemini://local.host.site/gem/sub/another/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator3 = ClientCertificateValidator(request: request3,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator3.validate())
        XCTAssertNil(cert.writtenURL)
        fileManager.checkedPaths.forEach { checkedPath in
            XCTAssertTrue(checkedPath.contains(GlobalConfiguration.smolverSubdirectory), "Checked path \"\(checkedPath)\" does not contain \"\(GlobalConfiguration.smolverSubdirectory)\"")
            XCTAssertTrue(checkedPath.contains(cert.fingerprint), "Checked path \"\(checkedPath)\" does not contain \"\(cert.fingerprint)\"")
        }
        fileManager.checkedPaths = []

        let request4 = try Request("gemini://local.host.site/gem/sub/another/and/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator4 = ClientCertificateValidator(request: request4,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator4.validate())
        XCTAssertNil(cert.writtenURL)
        fileManager.checkedPaths.forEach { checkedPath in
            XCTAssertTrue(checkedPath.contains(GlobalConfiguration.smolverSubdirectory), "Checked path \"\(checkedPath)\" does not contain \"\(GlobalConfiguration.smolverSubdirectory)\"")
            XCTAssertTrue(checkedPath.contains(cert.fingerprint), "Checked path \"\(checkedPath)\" does not contain \"\(cert.fingerprint)\"")
        }
        fileManager.checkedPaths = []

        let request5 = try Request("gemini://local.host.site/gem/sub/another/and/another/\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator5 = ClientCertificateValidator(request: request5,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator5.validate())
        XCTAssertNil(cert.writtenURL)
        fileManager.checkedPaths.forEach { checkedPath in
            XCTAssertTrue(checkedPath.contains(GlobalConfiguration.smolverSubdirectory), "Checked path \"\(checkedPath)\" does not contain \"\(GlobalConfiguration.smolverSubdirectory)\"")
            XCTAssertTrue(checkedPath.contains(cert.fingerprint), "Checked path \"\(checkedPath)\" does not contain \"\(cert.fingerprint)\"")
        }
        fileManager.checkedPaths = []

        let request6 = try Request("gemini://local.host.site/gem/sub/another/and/another/again/index.gmi\r\n", from: "", expectations: (host: "local.host.site", ip: nil))
        let validator6 = ClientCertificateValidator(request: request6,
                                                    x509Certificate: cert,
                                                    authentication: authentication,
                                                    fileManager: fileManager)
        XCTAssertNoThrow(try validator6.validate())
        XCTAssertNil(cert.writtenURL)
        fileManager.checkedPaths.forEach { checkedPath in
            XCTAssertTrue(checkedPath.contains(GlobalConfiguration.smolverSubdirectory), "Checked path \"\(checkedPath)\" does not contain \"\(GlobalConfiguration.smolverSubdirectory)\"")
            XCTAssertTrue(checkedPath.contains(cert.fingerprint), "Checked path \"\(checkedPath)\" does not contain \"\(cert.fingerprint)\"")
        }
        fileManager.checkedPaths = []
    }

    // MARK: -

    /// For the purposes of these tests, the exact `.smol.json` configuration is
    /// irrelevant. All that has been tested elsewhere in this file. These simply
    /// test that there are no whitelisted certificates on disk despite the config
    /// saying it requires it.
    ///

    // MARK: - Expected; given - Whitelist directed empty

    func testExpectedGivenConfiguredWhitelistDirectoryEmpty() throws {
        let request = try Request("gemini://xmpl.site/subdir/aoeuhtns.gmi\r\n", from: "", expectations: ("xmpl.site", nil))

        let whitelistedPath = "whitelisted"
        let authentication = AuthenticationType.file.authentication(for: "aoeuhtns.gmi", at: whitelistedPath)

        class ThrowingTestFileManager: TestFileManager {
            override func contentsOfDirectory(atPath path: String) throws -> [String] {
                // As of time of writing, the system file manager throws an error from this
                // method when the directory at path is empty. In the event that that changes
                // without me noticing, I want to ensure that that throwing scenario is
                // tested.
                enum Error: Swift.Error {
                    case some
                }
                throw Error.some
            }
        }
        let testFileManager = ThrowingTestFileManager()
        let certificate = TestingClientCertificate(fingerprint: "fingerprint", notValidBeforeDate: Date.distantPast, notValidAfterDate: Date.distantFuture)
        let validator = ClientCertificateValidator(request: request,
                                                   x509Certificate: certificate,
                                                   authentication: authentication,
                                                   fileManager: testFileManager)
        XCTAssertThrowsError(try validator.validate())
        // no crashing
        XCTAssertNil(certificate.writtenURL)
        XCTAssertTrue(TestingClientCertificate.onDiskURLsChecked.isEmpty)
    }

}
