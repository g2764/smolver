//
//  EnvironmentVariablesTests.swift
//  smolver
//
//  Created by Justin Marshall on 2/21/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import SocketServer
import XCTest

class EnvironmentVariablesTests: XCTestCase {

    // For the dictionary (subscript) tests, the script's path is intentionally != the url's path
    // because relative to the CGI script, the path is just what is between the script
    // name and the query parameters, exclusive of both. However, relative to the entire
    // url, the path would include the name of the script. Therefore, them being relative
    // to different contexts means their values are necessarily different in cases where
    // the script name is explicitly in the URL.

    // MARK: - Keys

    func testDictionaryKeyStringValues() {
        XCTAssertEqual(EnvironmentVariables.Key.Authenticated.type.stringValue, "AUTH_TYPE")
        XCTAssertEqual(EnvironmentVariables.Key.Authenticated.certificate(.fingerprint).stringValue, "X_CERTIFICATE_FINGERPRINT")
        XCTAssertEqual(EnvironmentVariables.Key.Authenticated.certificate(.notAfter).stringValue, "X_CERTIFICATE_NOT_AFTER")
        XCTAssertEqual(EnvironmentVariables.Key.Authenticated.certificate(.notBefore).stringValue, "X_CERTIFICATE_NOT_BEFORE")
        XCTAssertEqual(EnvironmentVariables.Key.Authenticated.certificate(.username).stringValue, "X_CERTIFICATE_USERNAME")

        XCTAssertEqual(EnvironmentVariables.Key.Network.gatewayInterface.stringValue, "GATEWAY_INTERFACE")
        XCTAssertEqual(EnvironmentVariables.Key.Network.url.stringValue, "GEMINI_URL")
        XCTAssertEqual(EnvironmentVariables.Key.Network.pathInfo.stringValue, "PATH_INFO")
        XCTAssertEqual(EnvironmentVariables.Key.Network.queryString.stringValue, "QUERY_STRING")
        XCTAssertEqual(EnvironmentVariables.Key.Network.remoteHost.stringValue, "REMOTE_HOST")

        XCTAssertEqual(EnvironmentVariables.Key.Script.name.stringValue, "SCRIPT_NAME")

        XCTAssertEqual(EnvironmentVariables.Key.Server.name.stringValue, "SERVER_NAME")
        XCTAssertEqual(EnvironmentVariables.Key.Server.port.stringValue, "SERVER_PORT")
        XCTAssertEqual(EnvironmentVariables.Key.Server.protocol.stringValue, "SERVER_PROTOCOL")
        XCTAssertEqual(EnvironmentVariables.Key.Server.software.stringValue, "SERVER_SOFTWARE")
    }

    func testDictionaryUnauthKeyCounts() {
        XCTAssertEqual(EnvironmentVariables.Key.unauthenticatedCount, 10)
    }

    func testDictionaryAuthKeyCounts() {
        XCTAssertEqual(EnvironmentVariables.Key.authenticatedCount, 15)
    }

    // MARK: - Unauth

    @discardableResult
    private func testEnvironmentVariables(request: RequestProtocol,
                                          executableFileName: String,
                                          hostname: String,
                                          expectedPathInfo: String)
    -> EnvironmentVariables
    {
        let environmentVariables = EnvironmentVariables(
            request: request,
            executableFileName: executableFileName,
            certificate: nil,
            hostname: hostname
        )

        XCTAssertEqual(environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)

        XCTAssertEqual(environmentVariables[.network(.gatewayInterface)], "CGI/1.1")
        XCTAssertEqual(environmentVariables[.network(.url)], request.url.absoluteString)
        XCTAssertEqual(environmentVariables[.network(.pathInfo)], expectedPathInfo)
        XCTAssertEqual(environmentVariables[.network(.queryString)], request.query ?? "")
        XCTAssertEqual(environmentVariables[.network(.remoteHost)], hostname)

        XCTAssertEqual(
            environmentVariables[.script(.name)],
            executableFileName == "/" ? executableFileName : ("/" + executableFileName)
        )

        XCTAssertEqual(environmentVariables[.server(.name)], request.url.host ?? "")
        XCTAssertEqual(environmentVariables[.server(.port)], "1965")
        XCTAssertEqual(environmentVariables[.server(.protocol)], "GEMINI")
        XCTAssertEqual(environmentVariables[.server(.software)], "smolver/\(SmolverCommand().version.replacingOccurrences(of: "v", with: ""))")

        return environmentVariables
    }

    func testDictionaryUnauthRequestNoHost() {
        let request = TestRequest(url: URL(string: "nohost")!,
                                  path: "",
                                  query: nil)
        testEnvironmentVariables(
            request: request,
            executableFileName: "file",
            hostname: "192.111.111.2",
            expectedPathInfo: "/"
        )
    }

    func testDictionaryUnauthPathRootNotTheExecutable() {
        let request = TestRequest(url: URL(string: "gemini://testi.ng/users/bob?fetchPic=true")!,
                                  path: "/users/bob",
                                  query: "fetchPic=true")
        testEnvironmentVariables(
            request: request,
            executableFileName: "bob",
            hostname: "10.2.189.9",
            expectedPathInfo: "/"
        )
    }

    func testDictionaryUnauthPathNotPresentQueryNotPresent() {
        let request = TestRequest(url: URL(string: "gemini://localhost/")!,
                                  path: "/",
                                  query: nil)
        testEnvironmentVariables(
            request: request,
            executableFileName: "/",
            hostname: "127.0.0.1",
            expectedPathInfo: "/"
        )
    }

    func testDictionaryUnauthPathPresentQueryNotPresent() {
        let request = TestRequest(url: URL(string: "gemini://example.com/search/repositories/smolver/")!,
                                  path: "/search/repositories/smolver/",
                                  query: nil)
        testEnvironmentVariables(
            request: request,
            executableFileName: "search",
            hostname: "192.168.10.21",
            expectedPathInfo: "/repositories/smolver"
        )
    }

    func testDictionaryUnauthPathPresentQueryPresent() {
        let request = TestRequest(url: URL(string: "gemini://testi.ng/users/bob?fetchPic=true")!,
                                  path: "/users/bob",
                                  query: "fetchPic=true")
        testEnvironmentVariables(
            request: request,
            executableFileName: "users",
            hostname: "10.2.189.9",
            expectedPathInfo: "/bob"
        )
    }

    func testDictionaryUnauthPathNotPresentQueryPresent() {
        let request = TestRequest(url: URL(string: "gemini://hello.world?country=United%20States&timeZone=MT")!,
                                  path: "/",
                                  query: "country=United%20States&timeZone=MT")
        testEnvironmentVariables(
            request: request,
            executableFileName: "/",
            hostname: "99.98.97.96",
            expectedPathInfo: "/"
        )
    }

    // MARK: - Auth

    private func test(environmentVariables: EnvironmentVariables,
                      certificate: TestingClientCertificateMetadata)
    {
        let environmentVariables = environmentVariables
                            .addingCertificate(certificate)

        XCTAssertEqual(environmentVariables[.auth(.type)], "CERTIFICATE")
        XCTAssertEqual(environmentVariables[.auth(.certificate(.fingerprint))], "SHA256:\(certificate.fingerprint)")
        XCTAssertEqual(environmentVariables[.auth(.certificate(.notBefore))], certificate.formattedNotValidBeforeDate)
        XCTAssertEqual(environmentVariables[.auth(.certificate(.notAfter))], certificate.formattedNotValidAfterDate)
        XCTAssertEqual(environmentVariables[.auth(.certificate(.username))], certificate.commonName)
    }

    func testDictionaryAuthPathRootNotTheExecutable() {
        let request = TestRequest(url: URL(string: "gemini://testi.ng/users/bob?fetchPic=true")!,
                                  path: "/users/bob",
                                  query: "fetchPic=true")
        // For testing purposes, any string will do. Testing exact format
        // would belong in SocketServer tests, not here.
        let certificate = TestingClientCertificateMetadata(fingerprint: "abc123",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "fancy coMMON NAme",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let environmentVariables = testEnvironmentVariables(
            request: request,
            executableFileName: "bob",
            hostname: "10.2.189.9",
            expectedPathInfo: "/"
        )

        test(
            environmentVariables: environmentVariables,
            certificate: certificate
        )
    }

    func testDictionaryAuthPathNotPresentQueryNotPresent() {
        let request = TestRequest(url: URL(string: "gemini://test.ing")!,
                                  path: "/",
                                  query: nil)
        // For testing purposes, any string will do. Testing exact format
        // would belong in SocketServer tests, not here.
        let certificate = TestingClientCertificateMetadata(fingerprint: "abc123",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "username",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let environmentVariables = testEnvironmentVariables(
            request: request,
            executableFileName: "/",
            hostname: "192.169.5.0",
            expectedPathInfo: "/"
        )

        test(
            environmentVariables: environmentVariables,
            certificate: certificate
        )
    }

    func testDictionaryAuthPathPresentQueryNotPresent() {
        let request = TestRequest(url: URL(string: "gemini://te.st/cgiscriptname/path1/path2/path3/")!,
                                  path: "/cgiscriptname/path1/path2/path3/",
                                  query: nil)
        // For testing purposes, any string will do. Testing exact format
        // would belong in SocketServer tests, not here.
        let certificate = TestingClientCertificateMetadata(fingerprint: "fancy fingerprint",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "qwerty",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let environmentVariables = testEnvironmentVariables(
            request: request,
            executableFileName: "cgiscriptname",
            hostname: "99.99.99.99",
            expectedPathInfo: "/path1/path2/path3"
        )

        test(
            environmentVariables: environmentVariables,
            certificate: certificate
        )
    }

    func testDictionaryAuthPathPresentQueryPresent() {
        let request = TestRequest(url: URL(string: "gemini://example.org/products/phones?manufacturer=purism")!,
                                  path: "/products/phones",
                                  query: "manufacturer=purism")
        // For testing purposes, any string will do. Testing exact format
        // would belong in SocketServer tests, not here.
        let certificate = TestingClientCertificateMetadata(fingerprint: "ac:9b:1a:7z",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "dude",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let environmentVariables = testEnvironmentVariables(
            request: request,
            executableFileName: "products",
            hostname: "1.1.1.1",
            expectedPathInfo: "/phones"
        )
        test(
            environmentVariables: environmentVariables,
            certificate: certificate
        )
    }

    func testDictionaryAuthPathNotPresentQueryPresent() {
        let request = TestRequest(url: URL(string: "gemini://abc.def/zoo?animal=cat")!,
                                  path: "/zoo",
                                  query: "animal=cat")
        // For testing purposes, any string will do. Testing exact format
        // would belong in SocketServer tests, not here.
        let certificate = TestingClientCertificateMetadata(fingerprint: "8h:1b:24:za:hb",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "King Bob",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let environmentVariables = testEnvironmentVariables(
            request: request,
            executableFileName: "zoo",
            hostname: "0.0.0.0",
            expectedPathInfo: "/"
        )

        test(
            environmentVariables: environmentVariables,
            certificate: certificate
        )
    }
}

// MARK: - Extensions

private extension EnvironmentVariables {
    func addingCertificate(_ certificate: ClientCertificateMetadata) -> Self {
        var mutableSelf = self
        mutableSelf[.auth(.certificate(.fingerprint))] = "\(String(describing: certificate.encryptionAlgorithm).uppercased):\(certificate.fingerprint)"
        mutableSelf[.auth(.certificate(.notBefore))] = Formatters.time.string(from: certificate.notValidBeforeDate)
        mutableSelf[.auth(.certificate(.notAfter))] = Formatters.time.string(from: certificate.notValidAfterDate)
        mutableSelf[.auth(.certificate(.username))] = certificate.commonName
        mutableSelf[.auth(.type)] = "CERTIFICATE"

        return .init(mutableSelf)
    }

    private init(_ other: Self) {
        self = other
    }
}
