//
//  CGIExecutableTests.swift
//  smolver
//
//  Created by Justin Marshall on 2/21/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import XCTest

class CGIExecutableTests: XCTestCase {

    /*

    For this entire file:

    The phrase 'CGI metadata' means:
      * Is CGI dir configured?
      * Does CGI file exist?
      * Is CGI script executable?

    */

    /*
        These are are indirectly testing that CGIExecutable calls the
        `EnvironmentVariables` initializer. The actual contents of the
        environment variables are beyond the scope of this test.

        Technically, not testing any of the contents is a testing gap,
        but I'm ok with that in this case because it is just passing
        a few parameters into an initializer for something that itself
        has adequate unit tests.
    */

    private var relativeBasePath: String {
        "/" + #file.split(separator: "/").dropLast().joined(separator: "/") + "/"
    }

    private var relativeIntegrationScriptsPath: String  { relativeBasePath + integrationScriptsSubdir + "/" }

    private let integrationScriptsSubdir = "IntegrationScripts"

    // MARK: - CGI metadata not ok

    func testCGIFileNotExists1() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni.org/\(integrationScriptsSubdir)/weather?zip=00000")!,
                                  path: "/\(integrationScriptsSubdir)/weather",
                                  query: "zip=00000")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/weather")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)

        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "weather", executable?.fullScriptPath)
        XCTAssertThrowsError(try executable?.execute()) { error in
            let error = error as NSError
            XCTAssertEqual(NSCocoaErrorDomain, error.domain)
            XCTAssertEqual(CocoaError.fileReadNoSuchFile.rawValue, error.code)
        }
    }

    func testCGIFileNotExists2() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni.org/\(integrationScriptsSubdir)/weather/us?zip=00000")!,
                                  path: "/\(integrationScriptsSubdir)/weather/us",
                                  query: "zip=00000")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "\(integrationScriptsSubdir)/news")!,
                                              URL(string: "\(integrationScriptsSubdir)/apps")!,
                                              URL(string: "\(integrationScriptsSubdir)/weather")!,
                                              URL(string: "\(integrationScriptsSubdir)/stocks")!
                                          ])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)

        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "weather", executable?.fullScriptPath)
        XCTAssertThrowsError(try executable?.execute()) { error in
            let error = error as NSError
            XCTAssertEqual(NSCocoaErrorDomain, error.domain)
            XCTAssertEqual(CocoaError.fileReadNoSuchFile.rawValue, error.code)
        }
    }

    func testCGIScriptNotExecutable() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni.gemini/\(integrationScriptsSubdir)/posts_nonExecutable.sh/today")!,
                                  path: "/\(integrationScriptsSubdir)/posts_nonExecutable.sh/today/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/posts_nonExecutable.sh")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)

        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "posts_nonExecutable.sh", executable?.fullScriptPath)
        XCTAssertThrowsError(try executable?.execute()) { error in
            let error = error as NSError
            XCTAssertEqual(NSCocoaErrorDomain, error.domain)
            XCTAssertEqual(CocoaError.fileReadNoPermission.rawValue, error.code)
        }
    }

    func testCGIDirNotConfiguredNoFilesInConfig() {
        let request = TestRequest(url: URL(string: "gemini://test.com/\(integrationScriptsSubdir)/cgi/nothing")!,
                                  path: "/\(integrationScriptsSubdir)/cgi/nothing",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredSingleFileSingleSubdirInConfig() {
        let request = TestRequest(url: URL(string: "gemini://ge.mi.ni/\(integrationScriptsSubdir)/feeds/a/")!,
                                  path: "/\(integrationScriptsSubdir)/feeds/a/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)notAMatchingFile")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredSingleFileMultipleSubdirInConfig() {
        let request = TestRequest(url: URL(string: "gemini://ge.mi.ni/\(integrationScriptsSubdir)/feeds/a/")!,
                                  path: "/\(integrationScriptsSubdir)/feeds/a/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/feeds/a/anothersubdir/myexecutablefile")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredMultipleFilesSingleSubdirInConfig() {
        let request = TestRequest(url: URL(string: "gemini://ge.mi.ni/\(integrationScriptsSubdir)/scripts/helloworld")!,
                                  path: "/\(integrationScriptsSubdir)/scripts/helloworld",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "\(integrationScriptsSubdir)abc")!,
                                              URL(string: "\(integrationScriptsSubdir)zxa")!,
                                              URL(string: "\(integrationScriptsSubdir)cgi-bin/")!
                                          ])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredMultipleFilesMultipleSubdirInConfig1() {
        let request = TestRequest(url: URL(string: "gemini://ge.mi.ni/\(integrationScriptsSubdir)/abc/def")!,
                                  path: "/\(integrationScriptsSubdir)/abc/def/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "\(integrationScriptsSubdir)/abc/de")!,
                                              URL(string: "\(integrationScriptsSubdir)/zxa/lmno")!,
                                              URL(string: "\(integrationScriptsSubdir)/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz")!
                                          ])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredMultipleFilesMultipleSubdirInConfig2() {
        let request = TestRequest(url: URL(string: "gemini://ge.mi.ni/\(integrationScriptsSubdir)/abc/def")!,
                                  path: "/\(integrationScriptsSubdir)/abc/def/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "\(integrationScriptsSubdir)/zxa/lmno")!,
                                              URL(string: "\(integrationScriptsSubdir)/abc/deff")!,
                                              URL(string: "\(integrationScriptsSubdir)/abc/def/ghi/jkl/mno/pqr/stu/vwx/yz")!
                                          ])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredCGIFileNotExists1() {
        let request = TestRequest(url: URL(string: "gemini://gemi.nii/\(integrationScriptsSubdir)/people/bob?sort=alphabetical")!,
                                  path: "/\(integrationScriptsSubdir)/people/bob/",
                                  query: "sort=alphabetical")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/software")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredCGIFileNotExists2() {
        let request = TestRequest(url: URL(string: "gemini://ge.mini/\(integrationScriptsSubdir)/repositories/gemini/smolver")!,
                                  path: "/\(integrationScriptsSubdir)/repositories/gemini/smolver/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/kittens")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredCGIFileNotExists3() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/cgi-bin.sh")!,
                                  path: "/cgi-bin.sh",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "cgi-bin")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)

        XCTAssertNil(executable)
    }

    func testCGIDirNotConfiguredCGIFileNotExists4() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/cgi-")!,
                                  path: "/cgi-",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "cgi")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)

        XCTAssertNil(executable)
    }

    // MARK: - CGI metadata ok

    // MARK: Unauth

    func testUnauthEnvironmentVariablesSingleFileSingleSubdirInConfigExactRequestPath() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/cgi-bin.swift")!,
                                  path: "/cgi-bin.swift",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "cgi-bin.swift")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeBasePath + "cgi-bin.swift", executable?.fullScriptPath)
    }

    func testUnauthEnvironmentVariablesSingleFileSingleSubdirInConfigRequestPathUnderConfig() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/\(integrationScriptsSubdir)cgibin/apps/weather")!,
                                  path: "/\(integrationScriptsSubdir)cgibin/apps/weather/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)cgibin")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath.dropLast() + "cgibin", executable?.fullScriptPath)
    }

    func testUnauthEnvironmentVariablesSingleFileMultipleSubdirInConfigExactRequestPath() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/\(integrationScriptsSubdir)/cgibin/apps/weather")!,
                                  path: "/\(integrationScriptsSubdir)/cgibin/apps/weather/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather/")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather", executable?.fullScriptPath)
    }

    func testUnauthEnvironmentVariablesSingleFileMultipleSubdirInConfigRequestPathUnderConfig() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/\(integrationScriptsSubdir)/cgibin/apps/weather/city/london")!,
                                  path: "\(integrationScriptsSubdir)/cgibin/apps/weather/city/london",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather", executable?.fullScriptPath)
    }

    func testUnauthEnvironmentVariablesMultipleFilesMultipleSubdirInConfigExactRequestPath() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/\(integrationScriptsSubdir)/cgibin/apps/weather/")!,
                                  path: "/\(integrationScriptsSubdir)/cgibin/apps/weather",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather/")!,
                                              URL(string: "news/us/")!,
                                              URL(string: "stocks/us/nyse")!,
                                              URL(string: "apps/linux/smolver/")!
                                          ])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather", executable?.fullScriptPath)
    }

    func testUnauthEnvironmentVariablesMultipleFilesMultipleSubdirInConfigRequestPathUnderConfig() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/cgibin/\(integrationScriptsSubdir)/apps/weather/countries/us/states/ny/cities/ny?sub=manhattan")!,
                                  path: "/\(integrationScriptsSubdir)/cgibin/apps/weather/countries/us/states/ny/cities/ny/",
                                  query: "sub=manhattan")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "news/us/")!,
                                              URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather/countries/us")!,
                                              URL(string: "stocks/us/nyse")!,
                                              URL(string: "apps/linux/smolver")!
                                          ])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.unauthenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather/countries/us", executable?.fullScriptPath)
    }

    // MARK: Auth

    func testAuthEnvironmentVariablesSingleFileSingleSubdirInConfigExactRequestPath() {
        let request = TestRequest(url: URL(string: "gemini://gemini.gopher.smol/\(integrationScriptsSubdir)dynamic/some/path?sort=random")!,
                                  path: "/\(integrationScriptsSubdir)dynamic/some/path",
                                  query: "sort=random")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)dynamic")!])
        let certificate = TestingClientCertificateMetadata(fingerprint: "unique print of a finger",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.authenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath.dropLast() + "dynamic", executable?.fullScriptPath)
    }

    func testAuthEnvironmentVariablesSingleFileSingleSubdirInConfigRequestPathUnderConfig() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/\(integrationScriptsSubdir)cgibin/apps/weather")!,
                                  path: "/\(integrationScriptsSubdir)cgibin/apps/weather/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)cgibin")!])
        let certificate = TestingClientCertificateMetadata(fingerprint: "unique print of a finger",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.authenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath.dropLast() + "cgibin", executable?.fullScriptPath)
    }

    func testAuthEnvironmentVariablesSingleFileMultipleSubdirInConfigExactRequestPath() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/\(integrationScriptsSubdir)/cgibin/apps/weather")!,
                                  path: "/\(integrationScriptsSubdir)/cgibin/apps/weather/",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather/")!])
        let certificate = TestingClientCertificateMetadata(fingerprint: "unique print of a finger",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.authenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather", executable?.fullScriptPath)
    }

    func testAuthEnvironmentVariablesSingleFileMultipleSubdirInConfigRequestPathUnderConfig() {
        let request = TestRequest(url: URL(string: "gemini://gem.ni/\(integrationScriptsSubdir)/cgibin/apps/weather/city?name=london")!,
                                  path: "/\(integrationScriptsSubdir)/cgibin/apps/weather/city",
                                  query: "name=london")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather/city")!])
        let certificate = TestingClientCertificateMetadata(fingerprint: "unique print of a finger",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.authenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather/city", executable?.fullScriptPath)
    }

    func testAuthEnvironmentVariablesMultipleFilesMultipleSubdirInConfigExactRequestPath() {
        let request = TestRequest(url: URL(string: "gemini://\(integrationScriptsSubdir)/gemi.ni/cgibin/apps/weather/")!,
                                  path: "/\(integrationScriptsSubdir)/cgibin/apps/weather",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "\(integrationScriptsSubdir)/news/us/")!,
                                              URL(string: "\(integrationScriptsSubdir)/stocks/us/nyse")!,
                                              URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather/")!,
                                              URL(string: "\(integrationScriptsSubdir)/apps/linux/smolver/")!
                                          ])
        let certificate = TestingClientCertificateMetadata(fingerprint: "unique print of a finger",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.authenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather", executable?.fullScriptPath)
    }

    func testAuthEnvironmentVariablesMultipleFilesMultipleSubdirInConfigRequestPathUnderConfig() {
        let request = TestRequest(url: URL(string: "gemini://gemi.ni/\(integrationScriptsSubdir)/cgibin/apps/weather/countries/us/states/ny/cities/ny?sub=manhattan")!,
                                  path: "/\(integrationScriptsSubdir)/cgibin/apps/weather/countries/us/states/ny/cities/ny/",
                                  query: "sub=manhattan")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [
                                              URL(string: "\(integrationScriptsSubdir)/news/us/")!,
                                              URL(string: "\(integrationScriptsSubdir)/stocks/us/nyse")!,
                                              URL(string: "\(integrationScriptsSubdir)/apps/linux/smolver")!,
                                              URL(string: "\(integrationScriptsSubdir)/cgibin/apps/weather/countries/us")!
                                          ])
        let certificate = TestingClientCertificateMetadata(fingerprint: "unique print of a finger",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: Date.distantPast,
                                                           notValidAfterDate: Date.distantFuture)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "",
                                       scriptBasePath: relativeBasePath)
        XCTAssertEqual(executable?.environmentVariables.count, EnvironmentVariables.Key.authenticatedCount)
        XCTAssertEqual(relativeIntegrationScriptsPath + "cgibin/apps/weather/countries/us", executable?.fullScriptPath)
    }

    // MARK: - CGI metadata ok, integration tests

    // For testing the actual calling of a real script, integration tests are
    // better than attempting to mock out all the system APIs for doing so.

    func testIntegrationApplicationXML() throws {
        let expectedOutputBodyString = """
        <?xml version="1.0" encoding="UTF-8"?>
        <server>
            <name>smolver</name>
            <protocol>Gemini</protocol>
            <language>Swift</language>
        </server>

        """
        let expectedOutputHeaderString = "20 application/xml\r\n"
        let expectedOutput = (expectedOutputHeaderString + expectedOutputBodyString).data(using: .utf8)

        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgiApplicationXML.sh")!,
                                  path: "/\(integrationScriptsSubdir)/cgiApplicationXML.sh",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgiApplicationXML.sh")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let actualOutput = try executable?.execute()

        XCTAssertNotNil(expectedOutput)
        XCTAssertNotNil(actualOutput)
        XCTAssertEqual(expectedOutput, actualOutput)
    }

    func testIntegrationImagePNG() throws {
        // image used per terms of license
        // source: https://www.gnu.org/graphics/amihud-4-freedoms.html
        // license: https://creativecommons.org/licenses/by-sa/4.0/
        // changes: converted to jpg, downsized

        let expectedOutputBodyData = try! Data(
            contentsOf: URL(
                    fileURLWithPath: relativeBasePath + integrationScriptsSubdir + "/4-freedoms.jpg"
                )
            )
        let expectedOutputHeaderString = "20 image/jpeg\r\n"
        var expectedOutput = expectedOutputHeaderString.data(using: .utf8)
        expectedOutput?.append(expectedOutputBodyData)

        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgiImagePNG.sh")!,
                                  path: "/\(integrationScriptsSubdir)/cgiImagePNG.sh",
                                  query: nil)
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgiImagePNG.sh")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let actualOutput = try executable?.execute()

        XCTAssertNotNil(expectedOutput)
        XCTAssertNotNil(actualOutput)
        XCTAssertEqual(expectedOutput, actualOutput)
    }

    private func unauthOutput(from languageName: String, extension: String?) -> Data {
        """
        Hello from \(languageName)!
        GATEWAY_INTERFACE = "CGI/1.1"
        GEMINI_URL = "gemini://some.thing/\(integrationScriptsSubdir)/cgi\(`extension` != nil ? ".\(`extension`!)" : "")/pathtest/pathtest2?query=parameter"
        PATH_INFO = "/pathtest/pathtest2"
        QUERY_STRING = "query=parameter"
        REMOTE_HOST = "192.168.1.1"
        SCRIPT_NAME = "/cgi\(`extension` != nil ? ".\(`extension`!)" : "")"
        SERVER_NAME = "some.thing"
        SERVER_PORT = "1965"
        SERVER_PROTOCOL = "GEMINI"
        SERVER_SOFTWARE = "smolver/\(SmolverCommand().version.replacingOccurrences(of: "v", with: ""))"

        """
            .data(using: .utf8)!
    }

    func testIntegrationShellScriptUnauth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi.sh/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi.sh/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi.sh/")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let output = try executable?.execute()
        XCTAssertEqual(unauthOutput(from: "bash", extension: "sh"), output)
    }

    func testIntegrationPythonScriptUnauth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi.py/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi.py/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi.py")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let output = try executable?.execute()
        XCTAssertEqual(unauthOutput(from: "python", extension: "py"), output)
    }

    func testIntegrationSwiftBinaryUnauth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let output = try executable?.execute()
        XCTAssertEqual(unauthOutput(from: "swift", extension: nil), output)
    }

    func testIntegrationSwiftScriptUnauth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi.swift/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi.swift/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi.swift/")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let output = try executable?.execute()
        XCTAssertEqual(unauthOutput(from: "swift", extension: "swift"), output)
    }

    private func authOutput(on date: String) -> Data {
        """
        AUTH_TYPE = "CERTIFICATE"
        X_CERTIFICATE_FINGERPRINT = "SHA256:CO:LO:NS:EP:AR:AT:ED:FI:NG:ER:PR:IN:T"
        X_CERTIFICATE_NOT_AFTER = "\(date)"
        X_CERTIFICATE_NOT_BEFORE = "\(date)"
        X_CERTIFICATE_USERNAME = "Name that IS commoN"

        """
            .data(using: .utf8)!
    }

    func testIntegrationShellScriptAuth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi.sh/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi.sh/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi.sh")!])

        let date = Date()
        let certificate = TestingClientCertificateMetadata(fingerprint: "CO:LO:NS:EP:AR:AT:ED:FI:NG:ER:PR:IN:T",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: date,
                                                           notValidAfterDate: date)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let actualOutput = try executable?.execute()
        let formattedDate = Formatters.time.string(from: date)
        let expectedOutput = unauthOutput(from: "bash", extension: "sh") + authOutput(on: formattedDate)
        XCTAssertEqual(expectedOutput, actualOutput)
    }

    func testIntegrationPythonScriptAuth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi.py/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi.py/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi.py")!])

        let date = Date()
        let certificate = TestingClientCertificateMetadata(fingerprint: "CO:LO:NS:EP:AR:AT:ED:FI:NG:ER:PR:IN:T",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: date,
                                                           notValidAfterDate: date)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let actualOutput = try executable?.execute()
        let formattedDate = Formatters.time.string(from: date)
        let expectedOutput = unauthOutput(from: "python", extension: "py") + authOutput(on: formattedDate)
        XCTAssertEqual(expectedOutput, actualOutput)
    }

    func testIntegrationSwiftBinaryAuth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi/")!])

        let date = Date()
        let certificate = TestingClientCertificateMetadata(fingerprint: "CO:LO:NS:EP:AR:AT:ED:FI:NG:ER:PR:IN:T",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: date,
                                                           notValidAfterDate: date)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let actualOutput = try executable?.execute()
        let formattedDate = Formatters.time.string(from: date)
        let expectedOutput = unauthOutput(from: "swift", extension: nil) + authOutput(on: formattedDate)
        XCTAssertEqual(expectedOutput, actualOutput)
    }

    func testIntegrationSwiftScriptAuth() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/cgi.swift/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/cgi.swift/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/cgi.swift")!])

        let date = Date()
        let certificate = TestingClientCertificateMetadata(fingerprint: "CO:LO:NS:EP:AR:AT:ED:FI:NG:ER:PR:IN:T",
                                                           encryptionAlgorithm: .sha256,
                                                           commonName: "Name that IS commoN",
                                                           notValidBeforeDate: date,
                                                           notValidAfterDate: date)
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: certificate,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)
        let actualOutput = try executable?.execute()
        let formattedDate = Formatters.time.string(from: date)
        let expectedOutput = unauthOutput(from: "swift", extension: "swift") + authOutput(on: formattedDate)
        XCTAssertEqual(expectedOutput, actualOutput)
    }

    func testFailCGIOutputToStandardErrorOnly1() {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/stderrOnly.sh/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/stderrOnly.sh/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/stderrOnly.sh//")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)

        XCTAssertThrowsError(try executable?.execute()) { error in
            XCTAssertEqual("hello from stderr\n", error.localizedDescription)
        }
    }

    func testFailCGIOutputToStandardErrorOnly2() {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/stderrOnly.sh/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/stderrOnly.sh/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/stderrOnly.sh/")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)

        XCTAssertThrowsError(try executable?.execute()) { error in
            XCTAssertEqual("hello from stderr\n", error.localizedDescription)
        }
    }

    func testFailCGIOutputToStandardOutAndStandardError() {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/stderrAndStdout.sh/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/stderrAndStdout.sh/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/stderrAndStdout.sh")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)

        XCTAssertThrowsError(try executable?.execute()) { error in
            XCTAssertEqual("hello from stderr\n", error.localizedDescription)
        }
    }

    func testFailCGICrash() throws {
        let request = TestRequest(url: URL(string: "gemini://some.thing/\(integrationScriptsSubdir)/crash.swift/pathtest/pathtest2?query=parameter")!,
                                  path: "/\(integrationScriptsSubdir)/crash.swift/pathtest/pathtest2",
                                  query: "query=parameter")
        let config = CGIExecutable.Config(whitelistedScriptFiles: [URL(string: "\(integrationScriptsSubdir)/crash.swift")!])
        let executable = CGIExecutable(request: request,
                                       config: config,
                                       certificate: nil,
                                       hostname: "192.168.1.1",
                                       scriptBasePath: relativeBasePath)

        XCTAssertThrowsError(try executable?.execute()) { error in
            // This expected string is specific to the Swift language in which the crash.swift
            // script is written. Other languages will necessarily have different output strings.
            XCTAssertTrue(error.localizedDescription.contains("Fatal error: intentional crash"),
                          "Expected to contain \"Fatal error: intentional crash\", got:\n\(error.localizedDescription)")
        }

        // a crashing CGI script should not cause smolver to crash, which is proven by being able to call `execute()` without crashing
    }
}
