#!/bin/env bash
#
#  cgiImagePNG.sh
#  smolver
#
#  Created by Justin Marshall on 8/11/23.
#  Copyright © 2023 Justin Marshall. All rights reserved.
#
#  This file is part of smolver.
#
#  smolver is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, version 3 of the License only.
#
#  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#  details.
#
#  You should have received a copy of the GNU Affero General Public License along
#  with smolver. If not, see <https://www.gnu.org/licenses/>.
#

# I don't like hardcoding this relative path, but good enough for now.
# When this inevitably breaks, find something more future-proof.
echo -e "20 image/jpeg\\r"
cat $(pwd)/Tests/smolverTests/Spec/CGI/IntegrationScripts/4-freedoms.jpg
