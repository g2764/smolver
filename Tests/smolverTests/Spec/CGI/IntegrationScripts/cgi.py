#!/bin/env python3
#
#  cgi.py
#  smolver
#
#  Created by Justin Marshall on 3/3/23.
#  Copyright © 2023 Justin Marshall. All rights reserved.
#
#  This file is part of smolver.
#
#  smolver is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, version 3 of the License only.
#
#  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#  details.
#
#  You should have received a copy of the GNU Affero General Public License along
#  with smolver. If not, see <https://www.gnu.org/licenses/>.
#

import os

print('Hello from python!')
print('GATEWAY_INTERFACE = "{0}"'.format(os.getenv('GATEWAY_INTERFACE')))
print('GEMINI_URL = "{0}"'.format(os.getenv('GEMINI_URL')))
print('PATH_INFO = "{0}"'.format(os.getenv('PATH_INFO')))
print('QUERY_STRING = "{0}"'.format(os.getenv('QUERY_STRING')))
print('REMOTE_HOST = "{0}"'.format(os.getenv('REMOTE_HOST')))
print('SCRIPT_NAME = "{0}"'.format(os.getenv('SCRIPT_NAME')))
print('SERVER_NAME = "{0}"'.format(os.getenv('SERVER_NAME')))
print('SERVER_PORT = "{0}"'.format(os.getenv('SERVER_PORT')))
print('SERVER_PROTOCOL = "{0}"'.format(os.getenv('SERVER_PROTOCOL')))
print('SERVER_SOFTWARE = "{0}"'.format(os.getenv('SERVER_SOFTWARE')))

if os.getenv('AUTH_TYPE'):
    print('AUTH_TYPE = "{0}"'.format(os.getenv('AUTH_TYPE')))

if os.getenv('X_CERTIFICATE_FINGERPRINT'):
    print('X_CERTIFICATE_FINGERPRINT = "{0}"'.format(os.getenv('X_CERTIFICATE_FINGERPRINT')))

if os.getenv('X_CERTIFICATE_NOT_AFTER'):
    print('X_CERTIFICATE_NOT_AFTER = "{0}"'.format(os.getenv('X_CERTIFICATE_NOT_AFTER')))

if os.getenv('X_CERTIFICATE_NOT_BEFORE'):
    print('X_CERTIFICATE_NOT_BEFORE = "{0}"'.format(os.getenv('X_CERTIFICATE_NOT_BEFORE')))

if os.getenv('X_CERTIFICATE_USERNAME'):
    print('X_CERTIFICATE_USERNAME = "{0}"'.format(os.getenv('X_CERTIFICATE_USERNAME')))
