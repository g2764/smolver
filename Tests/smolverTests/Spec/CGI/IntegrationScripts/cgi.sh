#!/bin/env bash
#
#  cgi.sh
#  smolver
#
#  Created by Justin Marshall on 3/2/23.
#  Copyright © 2023 Justin Marshall. All rights reserved.
#
#  This file is part of smolver.
#
#  smolver is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, version 3 of the License only.
#
#  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#  details.
#
#  You should have received a copy of the GNU Affero General Public License along
#  with smolver. If not, see <https://www.gnu.org/licenses/>.
#

echo Hello from bash!
echo GATEWAY_INTERFACE = \"$GATEWAY_INTERFACE\"
echo GEMINI_URL = \"$GEMINI_URL\"
echo PATH_INFO = \"$PATH_INFO\"
echo QUERY_STRING = \"$QUERY_STRING\"
echo REMOTE_HOST = \"$REMOTE_HOST\"
echo SCRIPT_NAME = \"$SCRIPT_NAME\"
echo SERVER_NAME = \"$SERVER_NAME\"
echo SERVER_PORT = \"$SERVER_PORT\"
echo SERVER_PROTOCOL = \"$SERVER_PROTOCOL\"
echo SERVER_SOFTWARE = \"$SERVER_SOFTWARE\"

if [[ -n $AUTH_TYPE ]]; then
    echo AUTH_TYPE = \"$AUTH_TYPE\"
fi

if [[ -n $X_CERTIFICATE_FINGERPRINT ]]; then
    echo X_CERTIFICATE_FINGERPRINT = \"$X_CERTIFICATE_FINGERPRINT\"
fi

if [[ -n $X_CERTIFICATE_NOT_AFTER ]]; then
    echo X_CERTIFICATE_NOT_AFTER = \"$X_CERTIFICATE_NOT_AFTER\"
fi

if [[ -n $X_CERTIFICATE_NOT_BEFORE ]]; then
    echo X_CERTIFICATE_NOT_BEFORE = \"$X_CERTIFICATE_NOT_BEFORE\"
fi

if [[ -n $X_CERTIFICATE_USERNAME ]]; then
    echo X_CERTIFICATE_USERNAME = \"$X_CERTIFICATE_USERNAME\"
fi
