#!/usr/local/bin/swift
//
//  cgi.swift
//  smolver
//
//  Created by Justin Marshall on 3/3/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

print("Hello from swift!")

let env = ProcessInfo.processInfo.environment

print("GATEWAY_INTERFACE = \"" + env["GATEWAY_INTERFACE"]! + "\"")
print("GEMINI_URL = \"" + env["GEMINI_URL"]! + "\"")
print("PATH_INFO = \"" + env["PATH_INFO"]! + "\"")
print("QUERY_STRING = \"" + env["QUERY_STRING"]! + "\"")
print("REMOTE_HOST = \"" + env["REMOTE_HOST"]! + "\"")
print("SCRIPT_NAME = \"" + env["SCRIPT_NAME"]! + "\"")
print("SERVER_NAME = \"" + env["SERVER_NAME"]! + "\"")
print("SERVER_PORT = \"" + env["SERVER_PORT"]! + "\"")
print("SERVER_PROTOCOL = \"" + env["SERVER_PROTOCOL"]! + "\"")
print("SERVER_SOFTWARE = \"" + env["SERVER_SOFTWARE"]! + "\"")


if let authType = env["AUTH_TYPE"] {
    print("AUTH_TYPE = \"\(authType)\"")
}

if let fingerprint = env["X_CERTIFICATE_FINGERPRINT"] {
    print("X_CERTIFICATE_FINGERPRINT = \"\(fingerprint)\"")
}

if let notAfter = env["X_CERTIFICATE_NOT_AFTER"] {
    print("X_CERTIFICATE_NOT_AFTER = \"\(notAfter)\"")
}

if let notBefore = env["X_CERTIFICATE_NOT_BEFORE"] {
    print("X_CERTIFICATE_NOT_BEFORE = \"\(notBefore)\"")
}

if let username = env["X_CERTIFICATE_USERNAME"] {
    print("X_CERTIFICATE_USERNAME = \"\(username)\"")
}
