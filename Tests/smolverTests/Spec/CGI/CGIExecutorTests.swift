//
//  CGIExecutorTests.swift
//  smolver
//
//  Created by Justin Marshall on 2/18/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import XCTest

private struct TestingExecutable: Executable {
    enum Output {
        case data(Data)
        case string(String?)
        case `throw`(Error)
    }

    let output: Output
    var environmentVariables: EnvironmentVariables { fatalError("uneeded for this test") }

    func execute() throws -> Data {
        switch output {
        case let .data(data):
            return data
        case let .string(string):
            return (string ?? "").data(using: .utf8)!
        case let .throw(error):
            throw error
        }
    }
}

class CGIExecutorTests: XCTestCase {
    /*

    For this entire file, the following rules apply:

    1. The phrase 'CGI metadata' means:
        * Is CGI dir configured?
        * Does CGI file exist?
        * Is CGI script executable?

    2. Redirects are ignored - outside the scope of the file under test
    3. Client certificate scenarios - (mostly) outside the scope of the file under test

    */

    // MARK: - Setup

    override func tearDown() {
        super.tearDown()
        TestLogger.reset()
    }

    // MARK: - CGI failures

    private func testCGIFailure(with error: Error) {
        let executable = TestingExecutable(output: .throw(error))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()

        guard case .failure(.temporary(.cgiError(let message))) = actualResponse.status else {
            return XCTFail("Expected CGI error response, got: \(actualResponse.status)")
        }

        XCTAssertEqual(CGIExecutor.Error.failure.localizedDescription, message)

        // Others might be logged as needed for admin convenience, but this is the only one that I want to be a hard requirement
        XCTAssertTrue(TestLogger.loggedErrors.contains(error.localizedDescription))
        XCTAssertFalse(TestLogger.didLogConfig)
        XCTAssertFalse(TestLogger.didLogWarning)
        XCTAssertFalse(TestLogger.didLogInfo)
        XCTAssertFalse(TestLogger.didLogDebug)
    }

    func testFailCGIReturnsNil() {
        let executable = TestingExecutable(output: .string(nil))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()

        guard case .failure(.temporary(.cgiError(let message))) = actualResponse.status else {
            return XCTFail("Expected CGI error response, got: \(actualResponse.status)")
        }

        XCTAssertEqual(CGIExecutor.Error.failure.localizedDescription, message)

        // Others might be logged as needed for admin convenience, but this is the only one that I want to be a hard requirement
        XCTAssertTrue(TestLogger.loggedErrors.contains(CGIExecutor.Error.undecodableOutput.localizedDescription))
        XCTAssertFalse(TestLogger.didLogConfig)
        XCTAssertFalse(TestLogger.didLogWarning)
        XCTAssertFalse(TestLogger.didLogInfo)
        XCTAssertTrue(TestLogger.didLogError)
        XCTAssertTrue(TestLogger.didLogDebug)
    }

    func testFailCGIThrowNoSuchFileError() {
        testCGIFailure(with: NSError(domain: NSCocoaErrorDomain, code: CocoaError.fileReadNoSuchFile.rawValue))
    }

    func testFailCGIThrowNoPermissionError() {
        testCGIFailure(with: NSError(domain: NSCocoaErrorDomain, code: CocoaError.fileReadNoPermission.rawValue))
    }

    func testFailCGIThrowUnknownError() {
        enum ExecutionError: LocalizedError {
            case somethingWentWrong

            var errorDescription: String? { "Something went wrong." }
        }

        testCGIFailure(with: ExecutionError.somethingWentWrong)
    }

    // MARK: - CGI success - mime non- text/gemini (i.e., arbitrary data)

    func testSuccessReturnsApplicationXMLSuccessResponse() {
        let header = "20 application/xml\r\n".data(using: .utf8)!
        let body = """
        <?xml version="1.0" encoding="UTF-8"?>
        <server>
            <name>smolver</name>
            <protocol>Gemini</protocol>
            <language>Swift</language>
        </server>
        """
            .data(using: .utf8)!

        let expectedResponse = Response(rawData: header + body)

        let executable = TestingExecutable(output: .data(header + body))
        let executor = CGIExecutor(executable: executable, hostname: "")
        let actualResponse = executor.execute()
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessReturnsImagePNGSuccessResponse() {
        let relativeBasePath = "/"
            + #file.split(separator: "/").dropLast().joined(separator: "/")
            + "/"

        let integrationScriptsSubdir = "IntegrationScripts"

        let header = "20 image/png\r\n".data(using: .utf8)!

        let body = try! Data(
            contentsOf: URL(
                    fileURLWithPath: relativeBasePath + integrationScriptsSubdir + "/4-freedoms.jpg"
                )
            )
        let expectedResponse = Response(rawData: header + body)

        let executable = TestingExecutable(output: .data(header + body))
        let executor = CGIExecutor(executable: executable, hostname: "")
        let actualResponse = executor.execute()
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    // MARK: - CGI success - text/gemini, check *some* response types

    func testSuccessReturnsGemtextSuccessResponse() {
        let expectedOutput = "nicely formatted script output"
        let expectedResponse = Response(status: .success(.success(mimeType: .text(.gemini(encoding: .utf8,
                                                                                  language: "en-US"),
                                                                                  expectedOutput.data(using: .utf8)!))))

        let executable = TestingExecutable(output: .string(expectedResponse.header + expectedOutput))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessReturnsPlaintextNonGeminiResponse() {
        let expectedOutput = "plaintext prose goes here"
        let expectedResponse = Response(status: .success(.success(mimeType: .text(.nonGemini(.textPlain,
                                                                                             encoding: .utf8),
                                                                                  expectedOutput.data(using: .utf8)!))))

        let executable = TestingExecutable(output: .string(expectedResponse.header + expectedOutput))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()
        XCTAssertEqual(actualResponse, expectedResponse)
    }

    func testSuccessReturnsNonGemtextSuccessResponse() {
        let expectedOutput = "- [ ] Markdown checklist"
        let expectedResponse = Response(status: .success(.success(mimeType: .text(.nonGemini(.textMarkdown,
                                                                                             encoding: .utf8),
                                                                                  expectedOutput.data(using: .utf8)!))))

        let executable = TestingExecutable(output: .string(expectedResponse.header + expectedOutput))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessReturnsTemporaryRedirectResponse() {
        let newURLPath = "newRelativeLocation/files/index.gmi"
        let expectedRespone = Response(status: .redirect(.temporary(newURL: URL(string: newURLPath)!)))

        let executable = TestingExecutable(output: .string(expectedRespone.header))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()
        XCTAssertEqual(expectedRespone, actualResponse)
    }

    func testSuccessReturnsPermanentRedirectResponse() {
        let newURLPath = "gemini://example.gem.ni/gemlog/index.gemini"
        let expectedResponse = Response(status: .redirect(.permanent(newURL: URL(string: newURLPath)!)))

        let executable = TestingExecutable(output: .string(expectedResponse.header))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()
        XCTAssertEqual(expectedResponse, actualResponse)
    }

    func testSuccessReturnsClientCertificateNotValidResponse() {
        let meta = "invalid cert!"
        let expectedResponse = Response(status: .clientCertificate(.notValid(reason: meta)))

        let executable = TestingExecutable(output: .string(expectedResponse.header))
        let executor = CGIExecutor(executable: executable, hostname: "", logger: TestLogger.self)
        let actualResponse = executor.execute()
        XCTAssertEqual(expectedResponse, actualResponse)
    }
}
