//
//  TestingClientCertificateMetadata.swift
//  smolver
//
//  Created by Justin Marshall on 4/22/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import Foundation
import SocketServer

struct TestingClientCertificateMetadata: ClientCertificateMetadata {
    var formattedNotValidBeforeDate: String {
        Formatters.time.string(from: notValidBeforeDate)
    }

    var formattedNotValidAfterDate: String {
        Formatters.time.string(from: notValidAfterDate)
    }

    let fingerprint: String
    let encryptionAlgorithm: X509Certificate.EncryptionAlgorithm
    let commonName: String
    let notValidBeforeDate: Date
    let notValidAfterDate: Date
}
