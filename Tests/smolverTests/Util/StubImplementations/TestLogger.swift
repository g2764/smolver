//
//  TestLogger.swift
//  smolver
//
//  Created by Justin Marshall on 4/22/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver

class TestLogger: Logging {
    static var loggedConfigs: [String] = []
    static var loggedErrors: [String] = []
    static var loggedWarnings: [String] = []
    static var loggedInfos: [String] = []
    static var loggedDebugs: [String] = []

    static var didLogConfig: Bool { !loggedConfigs.isEmpty }
    static var didLogError: Bool { !loggedErrors.isEmpty }
    static var didLogWarning: Bool { !loggedWarnings.isEmpty }
    static var didLogInfo: Bool { !loggedInfos.isEmpty }
    static var didLogDebug: Bool { !loggedDebugs.isEmpty }

    static func reset() {
        loggedConfigs.removeAll()
        loggedErrors.removeAll()
        loggedWarnings.removeAll()
        loggedInfos.removeAll()
        loggedDebugs.removeAll()
    }

    static func config(_ message: String) {
        loggedConfigs.append(message)
    }

    static func error(_ message: String, from source: Source) {
        loggedErrors.append(message)
    }

    static func warn(_ message: String, from source: Source) {
        loggedWarnings.append(message)
    }

    static func info(_ message: String, from source: Source) {
        loggedInfos.append(message)
    }

    static func debug(_ message: String, from source: Source) {
        loggedDebugs.append(message)
    }

    private init() { /* intentionally left blank */ }
}
