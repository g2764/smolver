//
//  URLMimeType+TestHelpers.swift
//  smolver
//
//  Created by Justin Marshall on 6/22/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
@testable import smolver

extension URL.MimeType {
    static let textHTML = Self(extension: "html")!
    static let textMarkdown = Self(extension: "md")!
    static let textPlain = Self(extension: "txt")!
    static let textPython = Self(extension: "py")!
    static let textEnglish = Self(extension: "english")!
    static let textCacheManifest = Self(extension: "appcache")!

    static let applicationPDF = Self(extension: "pdf")!
    static let applicationXML = Self(extension: "xml")!

    static let videoMP4 = Self(extension: "mp4")!

    static let imagePNG = Self(extension: "png")!
    static let imageJPEG = Self(extension: "jpeg")!

    static let textCSV = Self(type: "text/csv", extensions: ["csv"])
    static let textCPlusPlus = Self(type: "text/x-c++src", extensions: ["c++", "cpp", "cxx", "cc"])
}
