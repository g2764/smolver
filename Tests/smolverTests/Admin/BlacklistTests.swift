//
//  BlacklistTests.swift
//  smolver
//
//  Created by Justin Marshall on 5/22/23.
//  Copyright © 2023 Justin Marshall. All rights reserved.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

@testable import smolver
import XCTest

private extension Blacklist {
    static func shouldBlock(_ request: RequestProtocol, logger: Logging.Type = Log.self) -> Bool {
        shouldBlock(request, from: "", logger: logger)
    }
}

class BlacklistTests: XCTestCase {
    // MARK: - Setup

    override func tearDown() {
        super.tearDown()
        TestLogger.reset()
    }

    // MARK: - Not blacklisted

    private func assertLogsForNotBlacklisted() {
        XCTAssertFalse(TestLogger.didLogConfig,          "No config message should have been logged.")
        XCTAssertFalse(TestLogger.didLogError,           "No error message should have been logged.")
        XCTAssertFalse(TestLogger.didLogWarning,         "No warning message should have been logged.")
        XCTAssertFalse(TestLogger.didLogInfo,            "No info message should have been logged.")

        XCTAssertEqual(TestLogger.loggedDebugs.count, 2, "2 debug messages should have been logged.")
    }

    func testNotBlacklistedRequest1() {
        let request = TestRequest(url: URL(string: "gemini://somethi.ng/index.gmi")!,
                                  path: "/index.gmi",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    func testNotBlacklistedRequest2() {
        let request = TestRequest(url: URL(string: "gemini://some.thi/updates/\(LocalConfiguration.fileName)/posts/index")!,
                                  path: "/updates/\(LocalConfiguration.fileName)/posts/index",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    func testNotBlacklistedRequest3() {
        let request = TestRequest(url: URL(string: "gemini://some.thi/updates/\(GlobalConfiguration.configFileName)/posts/index")!,
                                  path: "/updates/\(GlobalConfiguration.configFileName)/posts/index",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    func testNotBlacklistedRequest4() {
        let request = TestRequest(url: URL(string: "gemini://some.thi/abc/config")!,
                                  path: "/abc/config",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    func testNotBlacklistedRequest5() {
        let request = TestRequest(url: URL(string: "gemini://some.thi/updates/\(GlobalConfiguration.configFileName)")!,
                                  path: "/updates/\(GlobalConfiguration.configFileName)",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    func testNotBlacklistedRequest6() {
        let request = TestRequest(url: URL(string: "gemini://some.thi/updates/config\(ClientCertificateValidator.anonymousAuthenticationsDirectory)")!,
                                  path: "/updates/config\(ClientCertificateValidator.anonymousAuthenticationsDirectory)",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    func testNotBlacklistedRequest7() {
        let request = TestRequest(url: URL(string: "gemini://subdomain\(ClientCertificateValidator.anonymousAuthenticationsDirectory)")!,
                                  path: "/",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    func testNotBlacklistedRequest9() {
        let request = TestRequest(url: URL(string: "gemini://sub.domain/test/anonymous")!,
                                  path: "/test/anonymous",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertFalse(isBlocked)
        assertLogsForNotBlacklisted()
    }

    // MARK: - Blacklisted

    private func assertLogsForBlacklisted() {
        XCTAssertFalse(TestLogger.didLogConfig,  "No config message should have been logged.")
        XCTAssertFalse(TestLogger.didLogError,   "No error message should have been logged.")
        XCTAssertFalse(TestLogger.didLogInfo,    "No info message should have been logged.")

        XCTAssertTrue(TestLogger.didLogWarning,  "A warning message should have been logged.")
        XCTAssertTrue(TestLogger.didLogDebug,    "A debug message should have been logged.")
    }

    func testRequestingLocalConfigurationFileWithoutQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://test.example/gemlog/\(LocalConfiguration.fileName)")!,
                                  path: "/gemlog/\(LocalConfiguration.fileName)",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }

    func testRequestingLocalConfigurationFileWithQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://example.org/posts/\(LocalConfiguration.fileName)?some=query&another=parameter")!,
                                  path: "/posts/\(LocalConfiguration.fileName)",
                                  query: "some=query&another=parameter")
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }

    func testRequestingGlobalConfigurationFileWithoutQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://test.example/\(GlobalConfiguration.configFileName)")!,
                                  path: "/\(GlobalConfiguration.configFileName)",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }

    func testRequestingGlobalConfigurationFileWithQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://test.example/\(GlobalConfiguration.configFileName)?bot=true")!,
                                  path: "/\(GlobalConfiguration.configFileName)",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }

    func testRequestingWithinAnonymousAuthenticationDirectoryWithoutQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://com.company.domain/gemlog/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)cert.pem")!,
                                  path: "/gemlog/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)cert.pem",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }

    func testRequestingWithinAnonymousAuthenticationDirectoryWithQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://com.company/blog/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)?secrets=1&returnThem=true")!,
                                  path: "/blog/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)",
                                  query: "secrets=1&returnThem=true")
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }

    func testRequestingInSubdirOfAnonymousAuthenticationDirectoryWithoutQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://gmi.gemini/admin/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)hidden/index.gmi")!,
                                  path: "/admin/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)hidden",
                                  query: nil)
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }

    func testRequestingInSubdirOfAnonymousAuthenticationDirectoryWithQueryParameters() {
        let request = TestRequest(url: URL(string: "gemini://gmi.gemini/admin/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)hidden?secrets=1&returnThem=true")!,
                                  path: "/admin/\(ClientCertificateValidator.anonymousAuthenticationsDirectory)hidden",
                                  query: "secrets=1&returnThem=true")
        let isBlocked = Blacklist.shouldBlock(request, logger: TestLogger.self)
        XCTAssertTrue(isBlocked)
        assertLogsForBlacklisted()
    }
}
