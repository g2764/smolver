// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.
//
//  This file is part of smolver.
//
//  smolver is free software: you can redistribute it and/or modify it under the
//  terms of the GNU Affero General Public License as published by the Free
//  Software Foundation, version 3 of the License only.
//
//  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//  details.
//
//  You should have received a copy of the GNU Affero General Public License along
//  with smolver. If not, see <https://www.gnu.org/licenses/>.
//

import PackageDescription

let package = Package(
    name: "smolver",
    products: [
        .executable(
            name: "smolver",
            targets: ["smolver"]
        )
    ],
    dependencies: [
        .package(
            name: "SocketServer",
            // path: "../../epikt/SocketServer"
            url: "git@gitlab.com:marshall-software-games/epikt/game/socketserver.git",
            // from: Version(0, 0, 21)
            branch: "master"
        )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .executableTarget(
            name: "smolver",
            dependencies: [
                "SocketServer"
            ]
        ),
        .testTarget(
            name: "smolverTests",
            dependencies: ["smolver"],
            resources: [.copy("Spec/CGI/IntegrationScripts")]
        )
    ]
)
