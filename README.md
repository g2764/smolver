---
title: smolver
section: 1
header: smolver Administrator Manual
footer: smolver v1.5.4
date: 2024-07-02
---

NAME
====

smolver - a small Gemini server

SYNOPSIS
========

`smolver [--version|-v] [--help|-h]`

`smolver run`

`smolver generateGlobalConfig`

`smolver generateLocalConfig`

`smolver validate`

DESCRIPTION
===========

This is a small [Gemini](//geminiprotocol.net) server, written in Swift. The name comes from the general self-reference
of Geminispace as the "smol [sic] internet" or "smolnet". This is a server for the smolnet, hence "smolver".

OPTIONS
=======

`-v, --version`

:   Prints the smolver version number.

`-h, --help`

:   Prints the synopsis and a list of all available subcommands.

EXAMPLES
=======

## Table of Contents
* [Installation](#installation)
* [Usage](#usage)
  * [Overview](#overview)
  * [Global Configuration](#global-configuration)
  * [Redirects](#redirects)
    * [Caveat Emptor](#caveat-emptor)
    * [How to Configure](#how-to-configure)
  * [Gone Status](#gone-status)
  * [CGI](#cgi)
    * [Caveat Emptor](#caveat-emptor-1)
    * [Variables](#variables)
  * [Authentication and Authorization](#authentication-and-authorization)
    * [How to Configure](#how-to-configure-1)
  * [Overriding Response Language](#overriding-response-language)
  * [Overriding Response Mime Type](#overriding-response-mime-type)
* [Note on Status Codes](#note-on-status-codes)
* [Security Considerations](#security-considerations)
* [Bugs](#bugs)
* [Roadmap](#roadmap)
* [Support](#support)
* [Contributing](#contributing)
* [Authors](#authors)
* [Copyright](#copyright)

### Installation
1. Install the [Swift](https://swift.org/download/#installation) language for your platform, if not already installed
2. Run the following:
```zsh
$ git clone git@gitlab.com:g2764/smolver.git
$ ./install.sh
```

This will:

* Download dependencies via the [Swift Package Manager](https://github.com/apple/swift-package-manager)
* Build dependencies and server
* Install the binary in `~/.swiftpm/bin`
* Create a `~/.smolver/` directory, with all the necessary default configurable keys (see [`Usage`](#usage) for customization / required tweaks)

If you want to make this run on boot, use your choice of `cron`, a [`systemd` service](smolver.service), etc.

### Usage

#### Overview
The specifics of each type of file will be covered below, but in a nutshell, there are 2 types of configuration file that `smolver` uses.

* `config.json`
  * Global settings that `smolver` needs to start serving any requests
  * Changes to this require a restart of `smolver` to take effect
  * For example: which directories should be allowed for serving files? where are SSL encryption keys? how much logging, if any, should be output for each request / response? etc.
* `.smol.json`
  * Localized settings for individual subdirectories that need to be considered before responding to a request for a specific file therein
  * Changes to this are effective immediately, without restarting, and will be read before returning any file in its directory
  * For example: redirects, authentication / authorization, overriding response language and / or mime types, `GONE` status response

On the surface, it seems like some overlap, but think of it like this:

* If `smolver` must have the information to be able to serve requests of any kind, it belongs in `config.json`. The one exception here is the property for the `robots.txt`, simply because that is required to live in the capsule's root (explained more below).
* If you as the administrator decide that you want to customize the rules for individual files or directories, it belongs in `.smol.json`.

Symbolic links are not officially supported in any configuration. ***See [security considerations](#security-considerations) for a brief explanation***

#### Global Configuration
1. Tweak the contents of your new `~/.smolver/config.json` file that was created upon installation. If you need to recreate this for any reason, you can do so with the following command:
```zsh
$ smolver generateGlobalConfig > ~/.smolver/config.json
```

* ***Make sure you understand the [security considerations](#security-considerations) of your configuration***
* Alternate locations for this file are not supported
* All paths within config file are relative to `~/.smolver/`, alternate locations not supported
* All properties are required unless otherwise noted below
* The path for the `robots.txt` is not configurable, merely whether or not it should be served. This is due a requirement in the protocol that this file should live at the root.
* `smolver` reads this file once at launch and caches it in RAM. Consequently, if you change this file then `smolver` needs restarted to see the changes.
* Optional properties
  * `maintenanceMessage`: Only needed if you want to halt serving of content, instead responding to most requests with a `41` (server unavailable), populating this value into the response header's meta field. I say "most" because `smolver` will still perform rate limiting if configured to do so, as described below.
  * `requestInterval`: integer representing number of seconds that should pass between requests from the same IP; if `null` or not present, rate limiting will be disabled
  * `language`: If not provided, defaults to `"en"`. This can be overridden on a file-by-file or directory basis. See [here](#overriding-response-language) for more information.
  * `encryption.certificateAuthorityPath`: unless you are using a custom Certificate Authority
  * `logs.logPath`: optional only if log level is `none`; if still provided it is ignored; required in all other cases
  * `logs.ipAddresses`: optional only if log level is `none`; if still provided it is ignored; required in all other cases
  * `ip`: optional, but recommended if you have a ***static*** IP, unless you don't want your server accessible by direct IP. If you choose to use this with a dynamic IP, you will need to change the config file every time your IP changes if you want to continue being accessible by direct IP.
   * `cgi`: optional, unless you want to enable CGI support. Its `whitelistedScriptFiles` key is required. For more information about CGI support, see [here](#cgi).

Log levels are listed in order of increasing verbosity, and higher values encompass the lower one(s); i.e.:
`none` < `error` < `warn` < `info` < `verbose`. `verbose` logging includes debug trace messages.

```json
{
    "maintenanceMessage": "Define maintenance reason here.",
    "host": "your.dns.host.name",
    "ip": "a.b.c.d",
    "requestInterval": 3,
    "language": "en",
    "shouldServeRobotsFile": true,
    "staticContent": {
        "allowedFileDownloadPaths": [
            "gemlog/",
            "tags/",
            "anySubdirectory/"
        ],
    },
    "cgi": {
        "whitelistedScriptFiles": [
            "scriptDir1/myscriptFile",
            "script2/test.sh",
            "script3",
            "script4.sh"
        ]
    },
    "encryption": {
        "certificateAuthorityPath": "location/of/certificateAuthority.pem",
        "certificateFile": "location/of/certificate.pem",
        "privateKeyFile": "location/of/private.pem"
    },
    "logs": {
        "ipAddresses": false,
        "level": "[none|error|warn|info|verbose]",
        "logPath": "logs/"
    }
}
```

2. Start the server:
```zsh
$ smolver run
```

Any misconfigurations in `config.json` will cause the server not to `run` and will output appropriate human-readable error messages.

3. (optional) Generate new local configuration files as needed:
```zsh
$ smolver generateLocalConfig > ~/.smolver/gemlog/.smol.json
```

4. (optional) Validate all config files:
```zsh
$ smolver validate
```

Content, [`.smol.json`](#overview) files (if configured), and `robots.txt` (if configured) are read and served from disk on every request and consequently can be changed at any time without restarting.

#### Redirects

##### Caveat Emptor
In Geminispace, redirects are:

* Considered a necessary evil
* Intended for restructuring sites or migrating sites between servers without breaking existing links. `smolver` currently does not support cross-domain redirects via configuration. You can, however, accomplish this via a CGI script. If you need configurable support for this, please send me a patch and a use case.
* Handled ***entirely*** by the client; it could be rejected, only allowed x number of times in succession, delegated to the human using the client, etc.
* Must be same-protocol *only*; i.e., do not attempt to redirect from `gemini://` to `gopher://`, `https://`, `ftp://` or any other protocol. While this behavior
  *is* technically within the limits outlined in the official protocol spec, Gemini strongly discourages it. As far as `smolver` is concerned, it is not supported
  and I have no intention of *ever* supporting it. It is hardcoded to redirect to another `gemini://` URL. Anything else is against the spirit of Gemini.

##### How to Configure
If you need to configure redirect(s), add or append to a `.smol.json` file in the *old* directory of the file(s). Only the `isTemporary` property is optional; if not provided, defaults to `false`.

```json
{
    "redirects": [
        {
            "oldFileName": "name-of-an-old-file-in-this-directory.gmi",
            "newFileName": "new-file-name-1.gmi",
            "newFilePath": "new/path/relative/to/config",
            "isTemporary": true
        },
        {
            "oldFileName": "name-of-another-old-file-in-this-directory.gmi",
            "newFileName": "new-file-name-2.gmi",
            "newFilePath": "new/path/relative/to/config",
            "isTemporary": false
        }
    ]
}
```

#### Gone Status

##### How to Configure
If you need to stop serving a file and have it return a `52` (gone) instead of a `51` (not found), add or append to a `.smol.json` file in the directory where the file(s) used to be.

```json
{
    "deletions": [
        {
            "fileName": "deleted-file-name-1.gmi",
        },
        {
            "fileName": "deleted-file-name-2.gmi",
        }
    ]
}
```

#### CGI

`smolver` has limited support for CGI scripts. It does not attempt to follow the official CGI RFC exactly, just the parts that make sense for Gemini and `smolver`.

CGI scripts are fully opt-in via configuration, similar to opting-in for static content. See [Global Configuration](#global-configuration) for example configuration.

Each CGI script file must be individually whitelisted via its path relative to `~/.smolver` and will be validated on server startup. If an opted-in file file is requested, `smolver` will attempt to invoke the script as documented in [environment variables](#variables) and return its `stdout` to the client. The output should be a full, valid Gemini response string, including the header.

There are no limits on mime types that can be returned, with one exception: binary files (images, PDF documents, etc.) can cause smolver to hang when trying to return them via a CGI script. In my testing on one machine, this limit seems to be at around the 64k size mark. I am not sure if this is due to a hardware limitation on my end, a `Swift.Process` limitation, or just a `smolver` bug. Text files do not seem to be affected.

If any of the following occur:

* The whitelisted script is not executable
* `stderr` has a non-empty value encoded as UTF8
* `stdout` is not encoded as UTF8
* `stdout` is not a valid Gemini response
* any other errors not explicitly mentioned here

... then `smolver` will return a `42` (CGI error) response to the client and the entirety of `stderr` will be logged as an error (provided that your logging setting in `config.json` allows errors). This will include the stack trace, assuming your script's language places stack traces into `stderr`.

`smolver` does not place any limits on CGI script execution time.

##### Caveat Emptor
If you choose to have a script itself locate and return static content, your script will be responsible for protecting against file system traversal attacks, which is already handled in `smolver`'s static content request processing. You have been warned.

The nature of CGI is that it is dynamic content, meaning `smolver` cannot possibly detect all malicious requests to arbitrary CGI scripts; thus, it is each CGI script's responsibility to perform input validation, particularly with query parameters. Use at your own risk. The safest configuration is one that does not enable CGI support.

##### Variables
The variables below will be passed to each script as environment variables.

| Environment variable | Example |
| --- | --- |
| `GATEWAY_INTERFACE` | `"CGI/1.1"` |
| `GEMINI_URL` | `"gemini://example.com/my-cgi-script/optional/path?queryParameters=percent%20encoded"` |
| `PATH_INFO`* | `"/"` or `"/optional/path"` |
| `QUERY_STRING` | `""` or `"queryParameters=percent%20encoded"` |
| `REMOTE_HOST` | `"client.fully.qualified.domain.name.or.ipv4.address"` |
| `SCRIPT_NAME` | `"/my-cgi-script"` |
| `SERVER_NAME` | `"example.com"` |
| `SERVER_PORT` | `"1965"` |
| `SERVER_PROTOCOL` | `"GEMINI"` |
| `SERVER_SOFTWARE` | `"smolver/x.y.z"` |

All environment variables will always be present, though may contain empty values.

\*There is a known issue where CGI requests which contain a URL path *after* the executable name will not resolve, and a `51` (not found) will be returned. Until that is fixed, this variable will always be empty. As a workaround, stick to query parameters for now. The CGI subsystem is capable of detecting script requests with paths, but the file routing logic that sits on top of it will bail out before ever even attempting the CGI code.

Before executing the CGI script, `smolver` will look for a client certificate on the request and an [`authentication` configuration](#authentication-and-authorization). If it passes authentication and authorization, the following extra environment variables will be passed. If authentication or authorization fails, `smolver` will not attempt to execute the script.

| Environment variable | Example |
| --- | --- |
| `AUTH_TYPE` | `"CERTIFICATE"` |
| `X_CERTIFICATE_FINGERPRINT` | `"SHA256:CO:LO:NS:EP:AR:AT:ED:FI:NG:ER:PR:IN:T"` |
| `X_CERTIFICATE_NOT_AFTER` | `"2023-02-18T12:00:00.000-0000"` |
| `X_CERTIFICATE_NOT_BEFORE` | `"2023-02-18T12:00:00.000-0000"` |
| `X_CERTIFICATE_USERNAME` | `"example_user from the cert's subject CN"` |

The time zone offset for `X_CERTIFICATE_NOT_BEFORE` & `X_CERTIFICATE_NOT_AFTER` will be in your machine's local time.

#### Authentication and Authorization

##### How to Configure
If you need to configure authentication, add or append to a `.smol.json` file in the directory of the file(s).

Provide either the `files` or `directory` object. Having both is a misconfiguration and a warning will be logged (provided that your logging setting in `config.json` allows warnings).

If you use whitelists:

* It is your responsibility as the server administrator to manually place the `.pem` files to which you want to grant access in the same place that you specify in the configuration.
* ***See [security considerations](#security-considerations)***

For anonymous access (i.e., if you are not using whitelists), `smolver` will systematically manage these for you. It is not intended for admins to touch these.

* `files`
  * `name` is required
  * `whitelistedCertificatePath` is optional
    * If provided, then only client certificates with the same signature as one of the `.pem` files in that path, relative to `smolver`'s root (`~/.smolver`) will be authorized.
    * If not provided, then all files in the `.smol.json` directory will be authorized for anyone who provides a client certificate.

* `directory`
  * `excludedFiles` is optional
    * Any files listed will be excluded from requiring a client certificate.
  * `whitelistedCertificatePath` is optional
    * If provided, then only client certificates with the same signature as one of the `.pem` files in that path, relative to `smolver`'s root (`~/.smolver`) will be authorized.
    * If not provided, then all files in the directory will be authorized for anyone who provides a client certificate.
  * The Gemini spec expects client certificates bound to one path also to be bound to paths below it; i.e., if a client certificate is authorized for `localhost/gemlog/`, it should also be authorized for `localhost/gemlog/tech/`, `localhost/gemlog/any/subdirectory/like/so/`, etc. For `smolver` to behave like so, `gemlog/`, `gemlog/tech/`, and `gemlog/any/subdirectory/like/so/` will need configured with a `.smol.json` file and this `directory` key. I realize this is not ideal, and plan to enhance it at some point so that `smolver` will detect this scenario for you and you won't have to duplicate your configuration(s).

```json
{
    "authentication": {
        "files": [
            {
                "name": "authenticated-file1.gmi",
                "whitelistedCertificatePath": "whitelisted/path/relative/to/root/dir/containing/pem/certificate/files/"
            },
            {
                "name": "authenticated-file2.gmi",
                "whitelistedCertificatePath": "whitelisted/path/relative/to/root/dir/containing/pem/certificate/files/"
            }
        ],
        "directory": {
            "excludedFiles": [
                "unauthenticatedFile1InAuthenticatedPath1.ext",
                "unauthenticatedFile2InAuthenticatedPath2.ext"
            ],
            "whitelistedCertificatePath": "whitelisted/path/relative/to/root/dir/containing/pem/certificate/files/"
        }
    }
}
```

#### Overriding Response Language
The following does not apply to CGI output, only static files. CGI language metadata cannot be overridden.

##### How to Configure
If you need to override the response language for particular gemtext file(s) or for an entire directory, add or append to a `.smol.json` file in the directory that needs the override. Per the spec, language metadata is only allowed for gemtext files.

If you use `languages.directory`, it applies to that directory only, not subdirectories. I realize this is not ideal, and plan to enhance it at some point so that `smolver` will detect this scenario for you and you won't have to duplicate your configuration(s).

`smolver` will make no attempt to validate your language string(s); it'll be returned verbatim in the response header.

```json
{
    "languages": {
        "files": [
            {
                "fileName": "de-que-es-this-capsule.gmi",
                "language": "en-US,es"
            }
            {
                "fileName": "hola-amigos.gmi",
                "language": "es-MX"
            },
            {
                "fileName": "au-revoir.gmi",
                "language": "fr-FR"
            }
        ],
        "directory": {
            "language": "de-DE"
        }
    }
}
```

#### Overriding Response Mime Type
The following does not apply to CGI output, only static files. CGI mime types cannot be overridden.

##### How to Configure
`smolver` will attempt to determine the correct mime type for success responses based on the file extension for the requested file, cross referenced with your system's `/etc/mime.types` file. If for some reason this file is unavailable, it will fallback to a default mime type of `application/octet-stream` for everything and a warning will be logged. If a file's mime type cannot be systematically determined, it will default to `application/octet-stream` and a warning will be logged. Logged warnings are dependent upon your logging setting in `config.json` allowing warnings.

If you need to override the response mime type (or the type you need simply isn't known) for particular file(s) or for an entire directory, add or append to a `.smol.json` file in the directory that needs the override.

If you use `mimeTypes.directory`, it applies to that directory only, not subdirectories. I realize this is not ideal, and plan to enhance it at some point so that `smolver` will detect this scenario for you and you won't have to duplicate your configuration(s).

`smolver` will make no attempt to validate your mime type string(s); it'll be returned verbatim in the response header.

```json
{
    "mimeTypes": {
        "files": [
            {
                "fileName": "index.pdf",
                "mimeType": "application/octet-stream"
            },
            {
                "fileName": "index.md",
                "mimeType": "text/plain"
            },
            {
                "fileName": "au-revoir.gmi",
                "mimeType": "application/json"
            }
        ],
        "directory": {
            "mimeType": "text/markdown"
        }
    }
}
```


### Note on Status Codes
`smolver` can directly return, either out of the box, or via configuration, every status code in the Gemini spec except for:

* `10` (input)
* `11` (sensitive input)

Reason: these only apply for dynamic queries, which is only supported via CGI.

* `43` (proxy error)

Reason: `smolver` does not support proxying directly, unless you choose to do it via CGI.

If you need to return any of these, return it from a CGI script as needed.

### Security Considerations
* Symbolic links are not supported for whitelisted client certificates. Supporting this would not only require extra code (the relevant code goes through `C`'s `fopen` function because the `OpenSSL` APIs require that, while all other filesystem access points go through Swift's `FileManger`), but would broaden `smolver`'s attack surface.
* With a few exceptions, anything in a directory or subdirectory of anything in `staticContent.allowedFileDownloadPaths` will be servable to clients.
* That means that if your configuration of whitelisted authentication directories and whitelisted static file directories overlaps, then the whitelisted authentication directories and all of their contents (`.pem` files) will be served if directly requested.
* Do not put anything in `staticContent.allowedFileDownloadPaths` that you do not want accessible; logs, SSL certificates, whitelisted client certificates, etc.

BUGS
====

See the [known issues](https://gitlab.com/g2764/smolver/-/blob/master/backlog/known-issues.md) for issues which have no planned patch timeframe (patches welcome!)

ROADMAP
=======

[See](https://gitlab.com/g2764/smolver/-/blob/master/backlog/v1-x/releases) [future development plans](https://gitlab.com/g2764/smolver/-/blob/master/backlog/v2-x)

SUPPORT
=======

Open an [issue](https://gitlab.com/g2764/smolver/-/issues/new)

CONTRIBUTING
============

* Be ok with the terms of the AGPL.
* Comply with the terms of the AGPL.
* If you are adding a new feature that is *not* in the [backlog](backlog), please reach out before starting. In the interest of keeping `smolver` small, simple and easy to maintain, new user-facing features will be rare. That said, I am more than ok with adding more admin-facing features (new configuration options, for example), so as to make running and administering `smolver` as easy as possible.
* Don't introduce any new warnings.
* Any new source files must be fully unit tested, with exceptions on a case-by-case basis.
* Any modifications to existing source files already covered by unit tests must have full accompanying unit test coverage for the modifications.
* Any new source files must have copyright notices matching the existing pattern. If comfortable, please include your name in the copyright notice so that you retain your copyright.
* If comfortable, feel free to add your name to the authors section of the README.
* Have fun!

AUTHORS
======

Code, documentation, and configurations written by Justin Marshall.

COPYRIGHT
=========

Copyright © 2021-2024 Justin Marshall. License AGPLv3: GNU AGPL version 3 ONLY <https://gnu.org/licenses/agpl-3.0.html>.

This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.
