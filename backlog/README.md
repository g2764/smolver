# What is this directory?

Trying a new approach to project planning here. Instead of GitLab issues and other agile features therein, I'm gonna try something new: a simple directory structure containing TODO lists.

Items will be prioritized based on numeric prefixes, like so:

```
1-implement-me-first.md
2-then-me.md
3-me-last.md
```

File internals will then be prioritized from top to bottom. Note that not all of the checklist items in each file will actually require any code. Some are just informational and have simply been copied from the spec for easy reference. As I encounter those, I'll remove the checkbox from them.

This will also make release notes trivial to create, as they will simply match the `.md` file for that release cycle.
