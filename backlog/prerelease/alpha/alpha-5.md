# 10 INPUT

- [x] Requested resource accepts a line of textual user input
- [x] <META> is a prompt which should be displayed to the user
- Same resource should then be requested again with the user's input included as a query component
- [x] Queries are included in requests as per usual generic URL definition in RFC3986
  - [x] Separated from the path by a `?`
- Reserved characters used in the user's input must be percent-encoded as per RFC3986
- Space characters should also be percent-encoded

# 11 SENSITIVE INPUT

- [x] As per status code 10, but for sensitive input such as passwords
- [x] Clients should present the prompt as per status code 10, but should not echo input to the screen, for privacy

# 20 SUCCESS

- [x] Request was handled successfully
- [x] Response body will follow the response header
- [x] <META> is a MIME media type which applies to the response body

# 30 REDIRECT - TEMPORARY

- [x] Server is redirecting client to a new location for the requested resource
- [x] No response body
- [x] <META> is a new URL for the requested resource
- [x] URL may be absolute or relative
  - [x] If relative, it should be resolved against the URL used it the original request
- If URL used in the original request contained a query string, the client MUST NOT apply this string to the redirect URL, instead using the redirect URL exactly as is
- Redirect considered temporary and clients should continue to request the resource at the original address and should not perform convenience actions like automatically updating bookmarks

# 31 REDIRECT - PERMANENT

- Requested resource should be consistently requested from the new URL provided in the future
- Search engine indexers, content aggregators, etc., should update their configurations to avoid requesting the old URL
- End-user clients may automatically update bookmarks, etc.
- Clients that only pay attention to the first digit will treat this as a temporary redirect (small performance penalty)

# 40 TEMPORARY FAILURE

- Request has failed
- [x] No response body
- Nature of the failure is temporary (identical request MAY succeed in the future)
- [x] <META> may provide additional information on the failure and should be displayed to human users

# 41 SERVER UNAVAILABLE

- Server unavailable due to overload or maintenance (cf. HTTP 503)

# 42 CGI ERROR

- CGI process, or similar system for generating dynamic content, died unexpectedly or timed out

# 43 PROXY ERROR

- Proxy request failed because the server was unable to successfully complete a transaction with the remote host (cf. HTTP 502, 504)

# 44 SLOW DOWN

- Rate limiting is in effect
- [x] <META> is an integer number of seconds which the client must wait before another request is made to this server (cf. HTTP 429)

# 50 PERMANENT FAILURE

- Request has failed
- [x] No response body
- Failure is premanent (identical future requests will reliably fail for the same reason)
- [x] <META> may provide additional information on the failure and should be displayed to human users
- Automatic clients such as aggregators, crawlers, etc. should not repeat this request

# 51 NOT FOUND

- Requested resource could not be found but may be available in the future (cf. HTTP 404)

# 52 GONE

- Requested resource is no longer available and will not be available again
- Search engines and similar tools should remove this resource from their indices
- Content aggregators should stop requesting the resource and convey to the human that the subscribed resource is gone (cf. HTTP 410)

# 53 PROXY REQUEST REFUSED

- Request was for a resource at a domain not served by the server and the server does not accept proxy requests

# 59 BAD REQUEST

- Server unable to parse the client's request, presumably due to a malformed request (cf. HTTP 400)

# 60 CLIENT CERTIFICATE REQUIRED

- Requested resource requires client certificate to access
- If the request was made without a certificate, it should be repeated with one
- If the request was made with a certificate, the server did not accept it and the request should be repeated with a different certificate
- [x] <META> (and/or the specific 6x code) may provide additional information on certificate requirements or the reason a certificate was rejected

# 61 CERTIFICATE NOT AUTHORIZED

- Supplied client certificate not authorized for accessing the particular requested resource
- The problem is not with the certificate itself, which may be authorized for other resources

# 62 CERTIFICATE NOT VALID

- Supplied client certificate not accepted because it is not valid
- Indicates a problem with the certificate itself, with no consideration of the particular requested resource
- Most likely cause is that the certificate's validity start date is in the future or its expiry date has passed
- May also indicate an invalid signature or a violation of X509 standard requirements
- [x] <META> should provide more information about the exact error

# Reminder

- [x] Double check response header format. Is meta being an empty string acceptable for responses without a meta?
- [x] Double check your implementation
- [x] Test via hardcoding a temporary response of every single type
