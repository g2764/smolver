- [x] Configurable redirects
- [x] Update README.md

- [x] Implement the following
- [x] Update README.md

## Redirects

Add a `.smol.json` file in the *old* directory of the file(s)
```json
{
    "redirects": [
        {
            "oldFileName": "name-of-an-old-file-in-this-directory.gmi",
            "newFileName": "new-file-name-1.gmi",
            "newFilePath": "new/path/relative/to/config"
        },
        {
            "oldFileName": "name-of-another-old-file-in-this-directory.gmi",
            "newFileName": "new-file-name-2.gmi",
            "newFilePath": "new/path/relative/to/config"
        }
    ]
}
```

## Installation

1. Install the [Swift](https://swift.org/download/#installation) language for your platform, if not already installed
2. Run the following:

```zsh
$ git clone git@gitlab.com:g2764/smolver.git
$ ./install.sh
```

## Usage

This will:
- Download dependencies via the [Swift Package Manager](https://github.com/apple/swift-package-manager)
- Build dependencies and server
- Install the binary in `/usr/local/bin`
- Create a `~/.smolver/` directory, with all the necessary default configurable keys (see [`Usage`](#usage) for customization / required tweaks)

If you want to make this run on boot, use your choice of `cron`, a `systemd` service, etc.

2. Start the server
```zsh
$ smolver run # any misconfigurations in `config.json` will cause the server not to `run` and will output appropriate human-readable error messages.
```
