- [x] Redesign the `ini` example below in `json`; `ini` would require implementing a custom parser
- [x] Implement config file parsing
- [x] Implement config file validation
- [x] Visiting a directory without a trailing / causes any relative links in that directory to fail
- [x] Implement usage of rules within config file
- [x] Refactor to support any arbitrary number of directories instead of just a single "gemlogPath"
- [x] Update README.md

## Usage

1. Customize your configuration via editing the `~/.smolver/config.json` file that was created upon installation.
   Alternate locations for this file are not supported. All properties are required unless otherwise noted below.

* Optional properties
   * `static.requestInterval`: integer number of seconds that should pass between requests from the same IP; if `null` or not present, rate limiting will be disabled
   * `encryption.certificateAuthorityPath`: unless you are using a custom Certificate Authority
   * `logs.logPath`: optional only if IP logging is off AND log level is `none`; if still provided it is ignored; required in all other cases

Log levels are listed in order of increasing verbosity, and higher values encompass the lower one(s); i.e.:
`none`<`error`<`warn`<`info`<`verbose`. `verbose` logging includes debug trace messages.

```json
{
    "static": {
        "allowedFileDownloadPaths: [
            "gemlog/",
            "tags/",
            "anySubdirectory/"
        ],
        "requestInterval": 3
    },
    "encryption": {
        "certificateAuthorityPath": "location/of/certificateAuthority.pem",
        "certificateFile": "location/of/certificate.pem",
        "privateKeyFile": "location/of/private.pem"
    },
    "logs": {
        "ipAddresses": false,
        "level": "[none|error|warn|info|verbose]",
        "logPath": "logs/"
    }
}
```
