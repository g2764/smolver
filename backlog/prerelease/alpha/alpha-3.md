# Redirects

## General stuff

- [x] Considered a necessary evil in Gemini

- Intended for restructuring sites or migrating sites between servers without breaking existing links
- Don't do this for any other intention than to avoid breaking old links

## Redirect limits

- Up to the client, but generally no more than 5

## Cross-protocol redirects

- [x] Are possible
- [x] Heavily discouraged

## Response body handling

- Response handling by clients should be informed by the provided MIME type information
- [x] Gemini defines one MIME type of its own (text/gemini) whose handling is discussed below in section 5
- In all other cases, clients should do something sensible based on the MIME type
- Minimalistic clients might adopt a strategy of printing all other "text/" responses to the screen without formatting and saving all non-text responses to the disk
- Clients for unix systems may consult /etc/mailcap to find installed programs for handling non-text types

# TLS

## Overview

- [x] TLS is mandatory
- [x] Servers MUST send a TLS `close_notify` prior to closing the connection after sending complete response (per RFCs 5246 and 8446). Disambiguates completed responses from responses closed prematurely due to network error or attack.

## Version requirements

- [x] Servers SHOULD use TLS v1.3 or higher
- Clients MAY refuse to connect to servers with TLS v1.2 or lower, as this support will hopefully be dropped in the future

# TLS

- [x] Support 1.3
