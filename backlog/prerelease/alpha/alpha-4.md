# Filenames

- [x] Inform client of MIME type. Usually done by filename, also cf. `/etc/mime.types`
- [x] MIME type for Gemini files is "text/gemini"
- [x] If receiving a request for a directory, and that directory contains an `index.gmi` or `index.gemini`, serve that instead

# File size

- No way to inform client of file size
- No support for compression or checksums
- Rule of thumb, 100MiB are better served elsewhere (still technically supported by Gemini)

# Text encoding

- [x] Should assume content is just UTF-8 and return it formatted as such to client (other encodings are not guaranteed)

# General

- [x] Test arbitrary file downloads
- [x] These changes required some refactoring, go back through all the previously-implemented requirements and double check everything
