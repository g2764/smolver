Bug fixes:

- [x] Too many connections too quickly logs an error and then the server is unresponsive, but doesn't crash - implemented a workaround: shutdown the server in this scenario and let systemd take care of the restarting ... should be ok for smolver, but not for the MUD, so I opened a ticket in SocketServer's repository to track this
- [x] Too many connections too quickly and app crashes, same problem as in MUD
