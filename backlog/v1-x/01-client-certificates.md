## Chores

- [x] Update local to latest Swift version
- [x] Smoke test
- [x] Address any and all warnings
- [x] Fix invalid json in README and example json file
- [x] Update production to latest Swift version
- [x] Rip out static site generation TODOs, move into separate repo
- [x] Make sure all TODOs are addressed
- [x] Update README
- [x] Audit code changes for loggable scenarios, including misconfigurations for new config file updates
- [x] Merge and tag SocketServer

## Client certificates

- [x] Implement

-  Short-lived client certificates are allowed
  - Generated on demand
  - Deleted immediately after use
  - Can be used as a "session identifier" to maintain server-side state for applications
  - Substitute for HTTP cookies, only generated voluntarily by the client, and once client deletes it, server cannot possible resurrect same value later
- Long-lived client certificates can be used for authentication into multi-user applications without passwords
- Self-hosted, single-user applications can be secured in manner familiar with OpenSSH, analogous to the `.authorized_keys` file for SSH
- Gemini requests typically made without client certificate
- Client certs created in response to a server request for one (via response code)
  - [x] Has its scope bound to the same hostname as the request URL and to all paths below the path of the request URL path
  - Interactive clients are strongly recommended to make deletion / temporary deactivation of certificates easy and to give users full control of client certificates
