This is going away from the protocol at some point, so may never do this. TLS library in use may handle this already?
- [x] Servers MUST use SSL v1.2 or higher - According to `nmap` and `openssl s_client`, `smolver` works with SSL v1.2 and v1.3

I need to research how to implement do this with the current SSL library
- [x] Strongly recommended only to support:
  - [x] Ephemeral Diffie-Hellman (DHE) Ephemeral Eliptic Curve Diffie-Hellman (ECDHE) for key agreement, for forward secrecy
  - [x] AES or ChaCha20 as bulk ciphers
  - [x] SHA2 or SHA3 family hash functions for message authentication

smolver's SSL library, BlueSSLService defaults to this cipher suite config: "DEFAULT:!DH". Running some ssl commands, it appears to match the above recommendations (pulled from the spec), with a few extras. Since SSL is outside the realm of my expertise, I am just going to stick with this default even though it appears to allow broader SSL connections from clients. I don't want to cause unintentional security holes. SSL commands used to perform this evaluation:

- openssl s_client -connect localhost:1965 -tls1_3
- nmap -sV --script ssl-enum-ciphers -p 1965 localhost
