- [ ] Research what it would take to refactor out BlueSocket and BlueSSLService. You currently have a fork of BlueSSLService because it appears, in the upstream repository, to be unmaintained. There is also a hacky (but documented) hardcoded error code check in SocketServer, due to some implementation details of its dependencies. These are not things I want to maintain long term.
  - [ ] Is the official Swift socket/TLS package viable?
  - [ ] https://github.com/apple/swift-certificates
  - [ ] These should be able to be swapped out by only changing your SocketServer framework, if your abstractions were done correctly.
  - [ ] Write comprehensive unit/integration tests before attempting any migrations

Current TLS library doesn't appear to support this as af 1/18/22. Implement this when implementing the above refactor.
- [ ] Use of Server Name Indication (SNI) extension to TLS is also mandatory, to facilitate name-based virtual hosting

Current TLS library should? support this as of 7/9/24, but not worth the effort when refactoring it out is so relatively soon.
- [ ] Double check difference between TLS v1.2 and v1.3. I believe it is just that v1.3 encrypts client certificates while v1.2 does not
- [ ] If correct, fail SSL handshake if client certificate is attached and client using v1.2


Current TLS library doesn't support this, to my memory, as of 7/3/24. See if this is possible when implementing the above refactor.
- [ ] Simultaneous IPv4 / IPv6 support

After refactoring BlueSocket & BlueSSLService out:
- [ ] Reverify the information in the ssl enhancements backlog file
- [ ] Regression test your game code that uses SocketServer
- [ ] Bump SocketServer to v0.1.0

Last:
- [ ] Remove `SocketServer.h`
- [ ] Remove `#if ...`s (except in `Package.swift`)
- [ ] Change `platforms` in `Package.swift` to Linux only
- [ ] Refactor `SocketServer`'s `public` interface
  - [ ] Break down into 2 `struct`s with parent `protocol` (names are just examples)
    - [ ] `protocol Socket { ... }`
    - [ ] `struct PersistentSocket: Socket { ... }`
    - [ ] `struct EphemeralSocket: Socket { ... }`
  - [ ] Rename package to just `Socket`
  - [ ] Move repository
- [ ] Modularize all SSL code into separate repository
- [ ] Bump SocketServer to v1.0.0. Arguably should have been done long ago...
