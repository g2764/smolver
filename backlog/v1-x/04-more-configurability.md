- [x] Configuration of maintenance mode (response status code 41 SERVER UNAVAILABLE)
- [x] Configurable support for the 52 gone response code
- [x] Configurable support for both redirect response codes
- [x] Audit for all remaining unreturned response codes, evaluate if it makes sense to allow configuration of them
  - Summary of findings: smolver returns every applicable status code, or allows it to be returned via CGI. No changes needed. Details below.

  - Currently returnable status codes, per smolver codebase:
    - 20

    - 30
    - 31

    - 40
    - 41
    - 42
    - 44

    - 50
    - 51
    - 52
    - 53
    - 59

    - 60
    - 61
    - 62

  - Status codes smolver never returns:
    - 10 INPUT - This only applies for dynamic queries, which is only supported via CGI. CGI scripts should be returning this, not smolver. Won't change.
    - 11 SENSITIVE INPUT - This only applies for dynamic queries, which is only supported via CGI. CGI scripts should be returning this, not smolver. Won't change.

    - 43 PROXY ERROR - smolver does not support proxying directly, unless you do it via CGI. CGI scripts should be returning this, not smolver. Won't change.
- [x] Audit all configurable response codes, evaluate if any need configurability for meta
  - None needed at this time
- [x] Response language(s) configuration
  - [x] Default language(s) in `config.json`
  - [x] File-by-file overrides via `.smol.json'
  - [x] Override entire directory via `.smol.json'
- [x] Configuration of arbitrary mime types paired with file extension. Much easier to do this than to implement and maintain a list of every possible one.
  - [x] More common ones should all work out of the box.
  - [x] File-by-file overrides via `.smol.json'
  - [x] Override entire directory via `.smol.json'
  - [x] Add note to README (similar to language) about duplicating config for subdirectories
  - [x] Make sure this works with CGI output as well, add more CGI unit tests for a few newly supported text/ mime types
- [x] Remove references within code, documentation, about planning to support non-utf8 charsets
- [x] Re-review all changes for this release, make sure logs are adequately placed and of an appropriate level
- [x] Reprioritize backlog so all new features come next, then internal changes
