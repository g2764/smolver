- [x] Fix inability to serve (or execute, in this branch) files unless they have a file extension
- [x] Fix incorrect query parameter detection
- [x] CGI (enhance `config.json` accordingly, add final config to this file)
  - [x] Example config (goes in `config.json`):
  "cgi": {
        "whitelistedScriptFiles": [
            "script1",
            "location/to/script2.py",
            "the/location/of/script.sh"
        ]
    }
  - [x] Query parameters: /weather?zip=x /weather?zip=x/
  - [x] Authentication, all client certificate validation should reuse existing code, not to be done by the CGI script
  - [x] Redirect configurations should work with CGI
