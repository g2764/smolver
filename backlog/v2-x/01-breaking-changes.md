v2.0.0 plans:

- [ ] TLS SNI support (if this doesn't require breaking changes as suspected, move to a v1.x release)
- [ ] Config changes
  - [ ] Rename `.smol.json` to `.meta` or `.meta.json`
  - [ ] One top-level config to declare basics of each
    - [ ] Perhaps just: domains (allow hosting multiple domains from same content root! that way you can have 2 domains that really go to the same place) and config location?
    - [ ] Default config locations to relevant `$XDG` environment variables
      - [ ] Allow override
      - [ ] Otherwise, programmatically pick a sensible hardcoded directory (such as ~/.local/config/smolver/config.json)
  - [ ] For each capsule hosted via SNI
    - [ ] Require separate config files
      - [ ] This will make it easier to keep each's content in separate repositories.
    - [ ] Default config locations to relevant `$XDG` environment variables
      - [ ] Allow override
      - [ ] Otherwise, programmatically pick a sensible hardcoded directory (such as ~/.local/config/smolver/domain/config.json)
    - [ ] Allow full customization of all configured directory/file locations, including but not limited to content root and logs
  - [ ] Make all configuration whitelist properties consistent; some use `allow`, some use `whitelist`. For CGI, simply `"scripts": [ ... ]` might be enough.
  - [ ] Audit all configuration options for cleanup possibilities
  - [ ] Refactor requestInterval into a Double so that ranges 0..<1 can be possible
  - [ ] Consider a new config language
    - [ ] JSON is becoming hard to maintain
    - [ ] JSON5 allows comments
    - [ ] toml allows comments
  - [ ] Read all `.smol.json` (or new equivalent) files on server start
    - [ ] This will allow much easier fixing of bugs (see known issues list) where these config files do not apply recursively to subdirectories
- [ ] Audit known issues list for other items that may be a good fit for a breaking-changes release
- [ ] Remove the backwards compatibility from `install.sh`
- [ ] Refactor `smolver` subcommands
  - [ ] See if there's a Debian standard for command line interfaces?
  - [ ] `run` -> `serve`
  - [ ] `generateGlobalConfig` --> `generate --global `
  - [ ] `generateGlobalConfig` --> `generate --capsule `
  - [ ] `generateLocalConfig` --> `generate --local `
- [ ] Audit `CGI` implementation for CGI spec non-compliance
  - [ ] Breaking changes
    - [ ] Custom environment variables should be prefixed with `X_` (see RFC 3875 4.1 & 4.1.18)
      - [ ] Rename
        - [ ] `GEMINI_URL` --> `X_GEMINI_URL`
        - [ ] `X_CERTIFICATE_FINGERPRINT` --> `X_GEMINI_CERTIFICATE_FINGERPRINT`
        - [ ] `X_CERTIFICATE_NOT_AFTER` --> `X_GEMINI_CERTIFICATE_NOT_AFTER`
        - [ ] `X_CERTIFICATE_NOT_BEFORE` --> `X_GEMINI_CERTIFICATE_NOT_BEFORE`
    - [ ] `X_CERTIFICATE_USERNAME` --> `REMOTE_USER` (see RFC 3875 4.1.11)
  - [ ] Non-breaking changes, but I'm fine with putting them into a v2.0.0
    - [ ] Add `PATH_TRANSLATED` environment varible (see RFC 3875 4.1.6)
    - [ ] The current working directory for the script SHOULD be set to the directory containing the script. (see RFC 3875 7.2)
- [ ] Delete modularized scripts from repository
- [ ] Documentation
  - [ ] Wording changes for each change above
  - [ ] Add `v1.x` to `v2.x` migration section
