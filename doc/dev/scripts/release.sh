#!/bin/bash
#
#  release.sh
#  smolver
#
#  Created by Justin Marshall on 7/28/23.
#  Copyright © 2023 Justin Marshall. All rights reserved.
#
#  This file is part of smolver.
#
#  smolver is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, version 3 of the License only.
#
#  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#  details.
#
#  You should have received a copy of the GNU Affero General Public License along
#  with smolver. If not, see <https://www.gnu.org/licenses/>.
#

echo "What is the new version number?"
read NEW_VERSION_NUMBER

# ensure consistency of inter-release version formatting as well as git tags
if [[ ${NEW_VERSION_NUMBER:0:1} != "v" ]]; then
    echo "Prefix version number with lowercase v."
    exit 1
fi

VERSION_FILE="Command.swift"
PATH_TO_VERSION_FILE="$(pwd)/Sources/smolver/Admin/${VERSION_FILE}"

# look for: `let version = "vX.Y.Z"`, `let version = "vXX.YY.ZZ"`, `let version = "vX.YY.Z"`, etc.
VERSION_DECLARATION_REGEX='let version = "v[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+"'
VERSION_DECLARATION=$(grep -E "${VERSION_DECLARATION_REGEX}" ${PATH_TO_VERSION_FILE})
OLD_VERSION_NUMBER=$(echo "${VERSION_DECLARATION}" | awk '{ print $4 }' | tr -d \")

if [[ $OLD_VERSION_NUMBER == "" ]]; then
    echo "Unable to find old version number. Aborting."
    exit 2
fi

sed -i "s/${OLD_VERSION_NUMBER}/${NEW_VERSION_NUMBER}/" ${PATH_TO_VERSION_FILE}

README_FILE="README.md"
PATH_TO_README_FILE="$(pwd)/${README_FILE}"
sed -i "s/${OLD_VERSION_NUMBER}/${NEW_VERSION_NUMBER}/" ${PATH_TO_README_FILE}

echo "Version number bumped to ${NEW_VERSION_NUMBER}."

DATE_DECLARATION_REGEX='^date: [[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$'
DATE_DECLARATION=$(grep -E "${DATE_DECLARATION_REGEX}" ${PATH_TO_README_FILE})
OLD_DATE=$(echo "${DATE_DECLARATION}" | awk '{ print $2 }')
NEW_DATE=$(date --iso-8601)
#
if [[ $OLD_DATE == "" ]]; then
    echo "Unable to find old date. Aborting."
    exit 3
fi

sed -i "s/${OLD_DATE}/${NEW_DATE}/" ${PATH_TO_README_FILE}
echo "Documentation date set to $NEW_DATE."

copyrightRegex() {
    echo "Copyright $1 [[:digit:]]{4}-[[:digit:]]{4}"
}

updateCopyright() {
    local FILE_PATH="${1:?}"
    local COPYRIGHT_SYMBOL="${2:?}"

    local COPYRIGHT_DECLARATION_REGEX=$( \
        copyrightRegex "${COPYRIGHT_SYMBOL:?}" \
    )

    local OLD_COPYRIGHT_DECLARATION=$( \
        grep -E "${COPYRIGHT_DECLARATION_REGEX:?}" "${FILE_PATH:?}" | \
        awk '{ print $1 " " $2 " " $3} ' \
    )

    # extract most recent copyright date, i.e., YYYY, from XXXX-YYYY
    local OLD_COPYRIGHT_END=$( \
        echo "${OLD_COPYRIGHT_DECLARATION:?}" | \
        awk '{ print $3 }' | \
        awk 'BEGIN { FS = "-" };{ print $2 }' \
    )

    local NEW_COPYRIGHT_END=$(date +%Y)
    local NEW_COPYRIGHT_DECLARATION=$( \
        echo ${OLD_COPYRIGHT_DECLARATION:?} | \
        sed "s/${OLD_COPYRIGHT_END:?}/${NEW_COPYRIGHT_END:?}/" \
    )

    sed -i "s/${OLD_COPYRIGHT_DECLARATION:?}/${NEW_COPYRIGHT_DECLARATION:?}/" "${FILE_PATH}"
    echo "Copyright date in "${FILE_PATH}" changed from "${OLD_COPYRIGHT_END:?}" to "${NEW_COPYRIGHT_END:?}.""
}

updateCopyright "${PATH_TO_README_FILE}" "©"

LICENSE_FILE="LICENSE"
PATH_TO_LICENSE_FILE="$(pwd)/${LICENSE_FILE}"
updateCopyright "${PATH_TO_LICENSE_FILE}" "\(C\)"

echo "Updating manpage"
cd doc/man/scripts && ./makeDocs.sh && cd - > /dev/null
echo "Manpage updated."

METADATA_DIR="doc/dev/reports"

echo "Updating cloc"
cloc --vcs=auto --hide-rate > "${METADATA_DIR:?}/cloc.txt"
echo "cloc updated."

echo Running tests to generate colorized coverage data, this may take a while...
./codeCoverage.sh --color > "${METADATA_DIR:?}/coverage_color.txt"
echo Colorized coverage data generated.

echo Running tests to generate plaintext coverage data, this may take a while...
./codeCoverage.sh --no-color > "${METADATA_DIR:?}/coverage_plain.txt"
echo Plaintext coverage data generated.
