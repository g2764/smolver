#!/bin/bash
#
#  newFile.sh
#  smolver
#
#  Created by Justin Marshall on 6/22/24.
#  Copyright © 2024 Justin Marshall. All rights reserved.
#
#  This file is part of smolver.
#
#  smolver is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, version 3 of the License only.
#
#  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#  details.
#
#  You should have received a copy of the GNU Affero General Public License along
#  with smolver. If not, see <https://www.gnu.org/licenses/>.
#

licenseText() {
    local FILE_TYPE=$1
    local FILE_NAME="${2}"
    local YOUR_NAME="${3}"
    local PROJECT_NAME="${4}"

    echo
    echo "  ${FILE_NAME}"
    echo "  ${PROJECT_NAME}"
    echo
    echo "  Created by ${YOUR_NAME} on $(date +%D)."
    echo "  Copyright © $(date +%Y) ${YOUR_NAME}. All rights reserved."
    echo
    echo "  This file is part of ${PROJECT_NAME}."
    echo
    echo "  ${PROJECT_NAME} is free software: you can redistribute it and/or modify it under the"
    echo '  terms of the GNU Affero General Public License as published by the Free'
    echo '  Software Foundation, version 3 of the License only.'
    echo
    echo "  ${PROJECT_NAME} is distributed in the hope that it will be useful, but WITHOUT ANY"
    echo '  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS'
    echo '  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more'
    echo '  details.'
    echo
    echo '  You should have received a copy of the GNU Affero General Public License along'
    echo "  with ${PROJECT_NAME}. If not, see <https://www.gnu.org/licenses/>."
    echo
}

collectProjectName() {
    local FILE_TYPE=$1
    local FILE_NAME="${2}"
    local YOUR_NAME="${3}"

    echo 'Project name?'
    read PROJECT_NAME

    if [[ "${PROJECT_NAME}" = '' ]]; then
        collectProjectName $FILE_TYPE "${FILE_NAME}" "${YOUR_NAME}"
    else
        LICENSE_TEXT=$(licenseText $FILE_TYPE "${FILE_NAME}" "${YOUR_NAME}" "${PROJECT_NAME}")

        case $FILE_TYPE in
            "shell")
                FILE_CONTENTS=$(echo "${LICENSE_TEXT}" | sed 's/^/#/')
                ;;
            "swift")
                FILE_CONTENTS=$(echo "${LICENSE_TEXT}" | sed 's/^/\/\//')
                ;;
        esac
    fi

    echo "${FILE_CONTENTS:?}" > "${FILE_NAME}"
}

collectYourName() {
    local FILE_TYPE=$1
    local FILE_NAME="${2}"

    echo 'Your name?'
    read YOUR_NAME

    if [[ "${YOUR_NAME}" = '' ]]; then
        collectYourName $FILE_TYPE "${FILE_NAME}"
    else
        collectProjectName $FILE_TYPE "${FILE_NAME}" "${YOUR_NAME}"
    fi
}

collectFileName() {
    local FILE_TYPE=$1

    echo 'File name?'
    read FILE_NAME

    if [[ $FILE_NAME = '' ]]; then
        collectFileName $FILE_TYPE
    else
        case $FILE_TYPE in
            "shell")
                collectYourName $FILE_TYPE "${FILE_NAME:?}.sh"
                ;;
            "swift")
                collectYourName $FILE_TYPE "${FILE_NAME:?}.swift"
                ;;
        esac
    fi
}

collectFileType() {
    echo 'File type? [swift|shell]'
    read FILE_TYPE

    case $FILE_TYPE in
        "shell"|"swift")
            collectFileName $FILE_TYPE
            ;;
        *)
            collectFileType
            ;;
    esac
}

collectFileType
