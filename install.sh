#!/bin/bash
#
#  install.sh
#  smolver
#
#  Created by Justin Marshall on 2/10/22.
#  Copyright © 2022 Justin Marshall. All rights reserved.
#
#  This file is part of smolver.
#
#  smolver is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, version 3 of the License only.
#
#  smolver is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#  details.
#
#  You should have received a copy of the GNU Affero General Public License along
#  with smolver. If not, see <https://www.gnu.org/licenses/>.
#

if ! [[ $(which swift) ]]; then
    echo "The Swift language is required. Swiftly (https://github.com/swiftlang/swiftly) is the simplest way to do so, but can also be done via instructions at: https://swift.org/download/#installation"
    exit 1
fi

echo "Building and installing smolver..."
swift package clean
swift package experimental-uninstall smolver
swift package -c release experimental-install

LEGACY_INSTALL_DIR="/usr/local/bin/smolver"
if [[ -d $LEGACY_INSTALL_DIR ]]; then
    echo "The next step, removing the legacy version of smolver in ${LEGACY_INSTALL_DIR}, must be run as root because of the location. If you are not comfortable with this, manually remove ${LEGACY_INSTALL_DIR}/smolver"
    # This isn't necessary for fresh installs, but is kept in place for backwards
    # compatibility. An old version of this script would create a smolver/
    # directory, and place the binary and other files in there.
    sudo rm -rf $LEGACY_INSTALL_DIR
    echo "Legacy version removed.\n"
fi

SMOLVER_HOME=~/.smolver
if [ ! -d $SMOLVER_HOME ]; then
    echo "No ~/.smolver directory detected. Creating ..."
    mkdir ~/.smolver
    echo "Created"
    echo "Creating template ~/.smolver/config.json file"
    smolver generateGlobalConfig > ~/.smolver/config.json
    echo "Created"
    echo "Tweak the config file's values to match your requirements. For full documentation on this file, see the documentation [README or <man smolver>]."
else
    echo "~/.smolver directory already present. Leaving alone."
fi

echo "Installing manpage..."
cd doc/man/scripts && ./installDocs.sh
echo "Installed"

~/.swiftpm/bin/smolver -h
